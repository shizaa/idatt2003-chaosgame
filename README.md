# IDATT2003 - ChaosGame



## Project Description

This is project is a Chaos Game Desktop Application. 
It allows you generate, visualize and modify fractals on a canvas. Its functionalities include:

- Generate predefined fractals like Sierpinski triangle, Barnsley leaf, Julia sets, and Mandelbrot set.
- Reading fractal from file 
- Save and load fractal configurations from text files.
- Creating empty fractal files. 
- Changing parameters of chosen fractal file. 
- Changing fractal color (This does not work for the mandel brot implementation. That has its own functionalities) 

## Prerequisites

- Java Development Kit (JDK) 8 or higher
- Maven 3.6.0 or higher

## How to run the project

- Clone the project from this Git repository.
- Open the project in an IDE or navigate to it in your terminal. 
- For terminal access: use the command mvn javafx:run.
- For IDE access: run the "ChaosGameApplication" class.

## Tips 
- When changing the parameters of the read file, remember to submit changes on each transformation. This means save the changes before pressing either "next transformation" or "previous transforamtion"

- The K and Y key will only change colors on fractals that are not Mandelbrot. Used on other fractals, it will turn the canvas black. Just load another fractal in this case.
- The M and P key will only change the mandelbrot iterations. 

- When chaning the iterations, you can increase the number of iterations without confirmin the txt file again. 
However, if you want to decrease and print the new fractal, you have to confirm the txt file first. 

- The saved files will (as of now) only save of thei directed path. This is inside resources/textFiles in the directory. 

- Make sure to have all included txt files in your project for all the functionality to work. 

- For the change buttons to make popups, they must be clicked twice. The first time will open them up in another window, but just go back to the main window and click the button again. 

(Project made public after submission)