package edu.ntnu.stud.modelTests.chaosGame;

import edu.ntnu.stud.model.chaosgame.ChaosGameCanvas;
import edu.ntnu.stud.model.math.Vector2D;
import javafx.scene.paint.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ChaosGameCanvasTests {

  private ChaosGameCanvas canvas;
  private Vector2D minCoords;
  private Vector2D maxCoords;

  @BeforeEach
  void setUp() {
    minCoords = new Vector2D(0, 0);
    maxCoords = new Vector2D(10, 10);
    canvas = new ChaosGameCanvas(10, 10, minCoords, maxCoords);
  }

  @Nested
  @DisplayName("Constructor Tests")
  class ConstructorTests {

    @Test
    @DisplayName("Test ChaosGameCanvas constructor with valid arguments")
    void testConstructorValidArguments() {
      assertDoesNotThrow(() -> new ChaosGameCanvas(10, 10, minCoords, maxCoords));
    }

    @Test
    @DisplayName("Test ChaosGameCanvas constructor with negative dimensions")
    void testConstructorInvalidDimensions() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameCanvas(-1, -1, minCoords, maxCoords));
    }

    @Test
    @DisplayName("Test ChaosGameCanvas constructor with null minCoords")
    void testConstructorNullMinCoords() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameCanvas(10, 10, null, maxCoords));
    }

    @Test
    @DisplayName("Test ChaosGameCanvas constructor with null maxCoords")
    void testConstructorNullMaxCoords() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameCanvas(10, 10, minCoords, null));
    }
  }

  @Nested
  @DisplayName("putPixel Tests")
  class PutPixelTests {

    @Test
    @DisplayName("Test putPixel with valid point")
    void testPutPixelValid() {
      assertDoesNotThrow(() -> canvas.putPixel(new Vector2D(5, 5)));
    }

    @Test
    @DisplayName("Test putPixel with negative coordinates")
    void testPutPixelNegativeCoordinates() {
      assertDoesNotThrow(() -> canvas.putPixel(new Vector2D(-1, -1)));
    }

    @Test
    @DisplayName("Test putPixel with out of bounds coordinates")
    void testPutPixelOutOfBoundsCoordinates() {
      assertDoesNotThrow(() -> canvas.putPixel(new Vector2D(11, 11)));
    }

    @Test
    @DisplayName("Test putPixel with null point")
    void testPutPixelNull() {
      assertThrows(IllegalArgumentException.class, () -> canvas.putPixel(null));
    }
  }

  @Nested
  @DisplayName("generateFractalImage Tests")
  class GenerateFractalImageTests {

    @Test
    @DisplayName("Test generateFractalImage returns non-null image")
    void testGenerateFractalImage() {
      assertNotNull(canvas.generateFractalImage());
    }
  }

  @Nested
  @DisplayName("getColor Tests")
  class GetColorTests {

    @Test
    @DisplayName("Test getColor with invalid y coordinate")
    void testGetColorInvalidY() {
      assertThrows(IllegalArgumentException.class, () -> canvas.getColor(1, 1, 5, -1));
    }

    @Test
    @DisplayName("Test getColor with valid parameters")
    void testGetColorValid() {
      assertDoesNotThrow(() -> canvas.getColor(1, 0, 5, 5));
    }

    @Test
    @DisplayName("Test getColor returns correct color for pixel value 1")
    void testGetColorPixelValueOne() {
      Color color = canvas.getColor(1, 1, 5, 5);
      assertNotNull(color);
      assertNotEquals(Color.BLACK, color);
    }

    @Test
    @DisplayName("Test getColor returns correct color for pixel value 0")
    void testGetColorPixelValueZero() {
      Color color = canvas.getColor(0, 1, 5, 5);
      assertEquals(Color.BLACK, color);
    }
  }
}
