package edu.ntnu.stud.modelTests.chaosGame;

import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.chaosgame.ChaosGameFractalFactory;
import edu.ntnu.stud.model.exception.InvalidFormatException;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testfx.framework.junit5.ApplicationTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("ChaosGameFractalFactory Tests")
public class ChaosGameFractalFactoryTest extends ApplicationTest {

  private static ChaosGameFractalFactory factory;
  private ComboBox<String> comboBox;
  private TextField numTransformationsField;
  private HBox nameNumField;

  @BeforeAll
  public static void setUpClass() {
    factory = new ChaosGameFractalFactory();
  }

  @BeforeEach
  public void setUp() {
    comboBox = new ComboBox<>();
    numTransformationsField = new TextField();
    nameNumField = new HBox();
  }

  @Nested
  @DisplayName("Positive Tests")
  class PositiveTests {

    @Test
    @DisplayName("Create Empty Affine Description")
    public void testCreateEmptyAffineDescription() {
      ChaosGameDescription description = factory.createEmptyAffineDescription(3);
      assertNotNull(description);
      assertEquals(3, description.getTransformationsList().size());
      for (Transform2D transform : description.getTransformationsList()) {
        assertTrue(transform instanceof AffineTransform2D);
      }
    }

    @Test
    @DisplayName("Create Empty Julia Description")
    public void testCreateEmptyJuliaDescription() {
      ChaosGameDescription description = factory.createEmptyJuliaDescription();
      assertNotNull(description);
      assertEquals(1, description.getTransformationsList().size());
      assertTrue(description.getTransformationsList().get(0) instanceof JuliaTransform);
    }

    @Test
    @DisplayName("Create Empty Fractal Combo Action - Affine")
    public void testCreateEmptyFractalComboActionAffine() {
      comboBox.getItems().addAll("Affine", "Julia");
      comboBox.setValue("Affine");
      factory.handleFractalComboBoxAction(comboBox, nameNumField, numTransformationsField);
      assertTrue(numTransformationsField.isVisible());
      assertTrue(nameNumField.getChildren().contains(numTransformationsField));
    }

    @Test
    @DisplayName("Create Empty Fractal Combo Action - Julia")
    public void testCreateEmptyFractalComboActionJulia() {
      comboBox.getItems().addAll("Affine", "Julia");
      comboBox.setValue("Julia");
      factory.handleFractalComboBoxAction(comboBox, nameNumField, numTransformationsField);
      assertFalse(numTransformationsField.isVisible());
      assertFalse(nameNumField.getChildren().contains(numTransformationsField));
    }

    @Test
    @DisplayName("Get Files")
    public void testGetFiles() {
      ComboBox<String> fileComboBox = new ComboBox<>();
      factory.populateFileComboBox(fileComboBox);
      assertFalse(fileComboBox.getItems().isEmpty());
    }

    @Test
    @DisplayName("List Files in Directory")
    public void testListFilesInDirectory() {
      List<String> files = factory.listFilesInDirectory("src/main/resources/textFiles", name -> name.endsWith(".txt"));
      assertFalse(files.isEmpty());
    }
  }

  @Nested
  @DisplayName("Negative Tests")
  class NegativeTests {

    @Test
    @DisplayName("Create Empty Affine Description with Zero Transformations")
    public void testCreateEmptyAffineDescriptionNegative() {
      ChaosGameDescription description = factory.createEmptyAffineDescription(0);
      assertNotNull(description);
      assertEquals(0, description.getTransformationsList().size());
    }

    @Test
    @DisplayName("Create Description with Invalid Number Format")
    public void testCreateDescriptionInvalidNumberFormat() {
      comboBox.setValue("Affine");
      numTransformationsField.setText("invalid");
      assertThrows(InvalidFormatException.class, () -> factory.createDescription(comboBox.getValue(), numTransformationsField));
    }

    @Test
    @DisplayName("Create Description with Invalid Fractal Type")
    public void testCreateDescriptionInvalidFractalType() {
      comboBox.setValue("InvalidType");
      assertThrows(InvalidFormatException.class, () -> factory.createDescription(comboBox.getValue(), numTransformationsField));
    }

    @Test
    @DisplayName("List Files in Invalid Directory")
    public void testListFilesInDirectoryInvalidDirectory() {
      List<String> files = factory.listFilesInDirectory("invalid/directory/path", name -> name.endsWith(".txt"));
      assertTrue(files.isEmpty());
    }

    @Test
    @DisplayName("Create Description with Null Values")
    public void testCreateDescriptionWithNullValues() {
      assertThrows(InvalidFormatException.class, () -> factory.createDescription(null, null));
    }

    @Test
    @DisplayName("Get Files with No Txt Files")
    public void testGetFilesWithNoTxtFiles() {
      ComboBox<String> fileComboBox = new ComboBox<>();
      List<String> files = factory.listFilesInDirectory("src/main/resources/emptyDir", name -> name.endsWith(".txt"));
      files.forEach(fileComboBox.getItems()::add);
      assertTrue(fileComboBox.getItems().isEmpty());
    }
  }
}
