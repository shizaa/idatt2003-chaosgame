package edu.ntnu.stud.modelTests.chaosGame;

import edu.ntnu.stud.model.chaosgame.ChaosGame;
import edu.ntnu.stud.model.chaosgame.ChaosGameCanvas;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.interfaces.ChaosGameObserver;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.math.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ChaosGameTests {

  private ChaosGameDescription mockDescription;
  private ChaosGameCanvas mockCanvas;
  private ChaosGame chaosGame;
  private ChaosGameObserver mockObserver;

  @BeforeEach
  void setUp() {
    mockDescription = mock(ChaosGameDescription.class);
    mockCanvas = mock(ChaosGameCanvas.class);
    mockObserver = mock(ChaosGameObserver.class);
    chaosGame = new ChaosGame(mockDescription, mockCanvas);
  }

  @Nested
  @DisplayName("Constructor Tests")
  class ConstructorTests {

    @Test
    @DisplayName("Test ChaosGame constructor with valid arguments")
    void testConstructorValidArguments() {
      assertDoesNotThrow(() -> new ChaosGame(mockDescription, mockCanvas));
    }

    @Test
    @DisplayName("Test ChaosGame constructor with invalid arguments")
    void testConstructorInvalidArguments() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGame(null, null));
    }
  }

  @Nested
  @DisplayName("runSteps Tests")
  class RunStepsTests {

    @Test
    @DisplayName("Test runSteps with valid steps")
    void testRunStepsValid() {
      // Arrange
      List<Transform2D> mockTransforms = new ArrayList<>();
      Transform2D mockTransform = mock(Transform2D.class);
      when(mockDescription.getTransformationsList()).thenReturn(mockTransforms);
      mockTransforms.add(mockTransform);

      Vector2D initialPosition = new Vector2D(0, 0);
      Vector2D transformedPosition = new Vector2D(1, 1);
      when(mockTransform.transform(any(Vector2D.class))).thenReturn(transformedPosition);

      // Act
      chaosGame.runSteps(1);

      // Assert
      verify(mockCanvas, times(1)).putPixel(transformedPosition);
    }

    @Test
    @DisplayName("Test runSteps with invalid steps")
    void testRunStepsInvalid() {
      assertThrows(IllegalArgumentException.class, () -> chaosGame.runSteps(-1));
    }
  }

  @Nested
  @DisplayName("Observer Tests")
  class ObserverTests {

    @Test
    @DisplayName("Test adding a valid observer")
    void testAddObserverValid() {
      assertDoesNotThrow(() -> chaosGame.addObserver(mockObserver));
    }

    @Test
    @DisplayName("Test adding an invalid observer")
    void testAddObserverInvalid() {
      assertThrows(IllegalArgumentException.class, () -> chaosGame.addObserver(null));
    }

    @Test
    @DisplayName("Test adding the same observer twice")
    void testAddObserverTwice() {
      chaosGame.addObserver(mockObserver);

      assertThrows(IllegalArgumentException.class, () -> chaosGame.addObserver(mockObserver));
    }

    @Test
    @DisplayName("Test notifying observers")
    void testNotifyObservers() {
      chaosGame.addObserver(mockObserver);
      chaosGame.notifyObservers("test");
      verify(mockObserver, times(1)).update("test");
    }

    @Test
    @DisplayName("Test notifying observers when there are no observers")
    void testNotifyObserversNoObservers() {
      assertDoesNotThrow(() -> chaosGame.notifyObservers("test"));
    }

    @Test
    @DisplayName("Test getting observers")
    void testGetObservers() {
      List<ChaosGameObserver> observers = chaosGame.getObservers();

      assertNotNull(observers);
      assertEquals(0, observers.size());
    }
  }
}
