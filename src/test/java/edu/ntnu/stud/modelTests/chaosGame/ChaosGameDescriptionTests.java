package edu.ntnu.stud.modelTests.chaosGame;

import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ChaosGameDescriptionTests {

  private ChaosGameDescription description;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private List<Transform2D> transforms;

  @BeforeEach
  void setUp() {
    minCoords = new Vector2D(0, 0);
    maxCoords = new Vector2D(10, 10);
    transforms = new ArrayList<>();
    description = new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  @Nested
  @DisplayName("Constructor Tests")
  class ConstructorTests {

    @Test
    @DisplayName("Test constructor with valid arguments")
    void testConstructorValidArguments() {
      assertDoesNotThrow(() -> new ChaosGameDescription(minCoords, maxCoords, transforms));
    }

    @Test
    @DisplayName("Test constructor with null minCoords")
    void testConstructorNullMinCoords() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameDescription(null, maxCoords, transforms));
    }

    @Test
    @DisplayName("Test constructor with null maxCoords")
    void testConstructorNullMaxCoords() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameDescription(minCoords, null, transforms));
    }

    @Test
    @DisplayName("Test constructor with null transforms")
    void testConstructorNullTransforms() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameDescription(minCoords, maxCoords, null));
    }
  }

  @Nested
  @DisplayName("Getter Tests")
  class GetterTests {

    @Test
    @DisplayName("Test getMinCoords")
    void testGetMinCoords() {
      assertEquals(minCoords, description.getMinCoords());
    }

    @Test
    @DisplayName("Test getMaxCoords")
    void testGetMaxCoords() {
      assertEquals(maxCoords, description.getMaxCoords());
    }

    @Test
    @DisplayName("Test getTransforms")
    void testGetTransforms() {
      assertEquals(transforms, description.getTransformationsList());
    }
  }

  @Nested
  @DisplayName("Setter Tests")
  class SetterTests {

    @Test
    @DisplayName("Test setMinCoords with valid argument")
    void testSetMinCoordsValid() {
      Vector2D newMinCoords = new Vector2D(1, 1);
      assertDoesNotThrow(() -> description.setMinCoords(newMinCoords));
      assertEquals(newMinCoords, description.getMinCoords());
    }

    @Test
    @DisplayName("Test setMinCoords with null argument")
    void testSetMinCoordsNull() {
      assertThrows(IllegalArgumentException.class, () -> description.setMinCoords(null));
    }

    @Test
    @DisplayName("Test setMaxCoords with valid argument")
    void testSetMaxCoordsValid() {
      Vector2D newMaxCoords = new Vector2D(11, 11);
      assertDoesNotThrow(() -> description.setMaxCoords(newMaxCoords));
      assertEquals(newMaxCoords, description.getMaxCoords());
    }

    @Test
    @DisplayName("Test setMaxCoords with null argument")
    void testSetMaxCoordsNull() {
      assertThrows(IllegalArgumentException.class, () -> description.setMaxCoords(null));
    }

    @Test
    @DisplayName("Test setFilePath with valid argument")
    void testSetFilePathValid() {
      String filePath = "path/to/file";
      assertDoesNotThrow(() -> description.setFilePath(filePath));
    }

    @Test
    @DisplayName("Test setFilePath with null argument")
    void testSetFilePathNull() {
      assertThrows(IllegalArgumentException.class, () -> description.setFilePath(null));
    }
  }

  @Nested
  @DisplayName("Matrix and Vector Update Tests")
  class MatrixAndVectorUpdateTests {

    @BeforeEach
    void setUp() {
      transforms.add(mock(AffineTransform2D.class));
    }

    @Test
    @DisplayName("Test updateMatrix with valid arguments")
    void testUpdateMatrixValid() {
      assertDoesNotThrow(() -> description.updateMatrix(0, 1, 2, 3, 4));
    }

    @Test
    @DisplayName("Test updateMatrix with invalid index")
    void testUpdateMatrixInvalidIndex() {
      assertThrows(IndexOutOfBoundsException.class, () -> description.updateMatrix(-1, 1, 2, 3, 4));
      assertThrows(IndexOutOfBoundsException.class, () -> description.updateMatrix(1, 1, 2, 3, 4));
    }

    @Test
    @DisplayName("Test updateVector with valid arguments")
    void testUpdateVectorValid() {
      assertDoesNotThrow(() -> description.updateVector(0, 1, 2));
    }

    @Test
    @DisplayName("Test updateVector with invalid index")
    void testUpdateVectorInvalidIndex() {
      assertThrows(IndexOutOfBoundsException.class, () -> description.updateVector(-1, 1, 2));
      assertThrows(IndexOutOfBoundsException.class, () -> description.updateVector(1, 1, 2));
    }
  }

  @Nested
  @DisplayName("Julia Transform Tests")
  class JuliaTransformTests {

    private JuliaTransform mockJuliaTransform;

    @BeforeEach
    void setUp() {
      mockJuliaTransform = mock(JuliaTransform.class);
      transforms.add(mockJuliaTransform);
    }

    @Test
    @DisplayName("Test updateJuliaConstant")
    void testUpdateJuliaConstant() {
      description.updateJuliaConstant(1.0, 2.0);
      verify(mockJuliaTransform, times(1)).setComplex(argThat(complex ->
              Double.compare(complex.getRealPart(), 1.0) == 0 &&
                      Double.compare(complex.getImaginaryPart(), 2.0) == 0));
    }

    @Test
    @DisplayName("Test getJuliaConstant returns correct value")
    void testGetJuliaConstant() {
      Complex expectedComplex = new Complex(1.0, 2.0);
      when(mockJuliaTransform.getComplex()).thenReturn(expectedComplex);
      assertEquals(expectedComplex, description.getJuliaConstant());
    }

    @Test
    @DisplayName("Test getJuliaConstant returns null if no JuliaTransform present")
    void testGetJuliaConstantNoJuliaTransform() {
      transforms.clear();
      assertNull(description.getJuliaConstant());
    }
  }

  @Nested
  @DisplayName("Matrix Retrieval Tests")
  class MatrixRetrievalTests {

    @BeforeEach
    void setUp() {
      transforms.add(mock(AffineTransform2D.class));
    }

    @Test
    @DisplayName("Test getMatrix returns correct matrices")
    void testGetMatrix() {
      AffineTransform2D mockAffine = mock(AffineTransform2D.class);
      when(mockAffine.getMatrix()).thenReturn(new Matrix2x2(1, 2, 3, 4));
      transforms.set(0, mockAffine);

      List<Matrix2x2> matrices = description.getMatrix();
      assertEquals(1, matrices.size());
      assertEquals(new Matrix2x2(1, 2, 3, 4), matrices.getFirst());
    }
  }

  @Nested
  @DisplayName("Vector Retrieval Tests")
  class VectorRetrievalTests {

    @BeforeEach
    void setUp() {
      transforms.add(mock(AffineTransform2D.class));
    }

    @Test
    @DisplayName("Test getVector with valid index")
    void testGetVectorValid() {
      Vector2D expectedVector = new Vector2D(1, 2);
      AffineTransform2D mockAffine = mock(AffineTransform2D.class);
      when(mockAffine.getVector()).thenReturn(expectedVector);
      transforms.set(0, mockAffine);

      assertEquals(expectedVector, description.getVector(0));
    }

    @Test
    @DisplayName("Test getVector with invalid index")
    void testGetVectorInvalidIndex() {
      assertThrows(IndexOutOfBoundsException.class, () -> description.getVector(-1));
      assertThrows(IndexOutOfBoundsException.class, () -> description.getVector(1));
    }


    @Test
    @DisplayName("Test getVector returns null for non-AffineTransform2D")
    void testGetVectorReturnsNullForNonAffineTransform2D() {
      Transform2D mockTransform = mock(Transform2D.class);
      transforms.add(mockTransform);

      Vector2D result = description.getVector(0);

      assertNull(result);
    }

  }
}
