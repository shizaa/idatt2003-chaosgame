package edu.ntnu.stud.modelTests.math;

import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The class Matrix2x2Test is a test class for the Matrix2x2 class.
 * Responsibility: To test the Matrix2x2 class.
 */
class Matrix2x2Test {

  @Test
  @DisplayName("Test the multiply method")
  void multiplyTest() {
    Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
    Vector2D vector = new Vector2D(5, 6);
    Vector2D result = matrix.multiply(vector);

    assertEquals(17, result.getX0());
    assertEquals(39, result.getX1());
  }

  @Test
  @DisplayName("Test the constructor with NaN")
  void constructorWithNaNShouldThrowException() {
    assertThrows(IllegalArgumentException.class, () -> new Matrix2x2(Double.NaN, 1, 2, 3), "Constructor did not throw for NaN input");
    assertThrows(IllegalArgumentException.class, () -> new Matrix2x2(1, Double.NaN, 2, 3), "Constructor did not throw for NaN input");
    assertThrows(IllegalArgumentException.class, () -> new Matrix2x2(1, 2, Double.NaN, 3), "Constructor did not throw for NaN input");
    assertThrows(IllegalArgumentException.class, () -> new Matrix2x2(1, 2, 3, Double.NaN), "Constructor did not throw for NaN input");
  }

  @Test
  @DisplayName("Test multiply with null vector")
  void multiplyWithNullVectorShouldThrowException() {
    Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
    assertThrows(IllegalArgumentException.class, () -> matrix.multiply(null), "Multiply did not throw for null vector");
  }
}