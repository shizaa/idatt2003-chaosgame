package edu.ntnu.stud.modelTests.math;

import edu.ntnu.stud.model.math.Complex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

/**
 * Test class for the Complex class.
 * Responsibility: To test the Complex class.
 */
public class ComplexTest {
        private static final double DELTA = 1e-6;

        @Nested
        @DisplayName("Tests for the Complex class constructor")

        class ComplexConstructorTest {
            @Test
            @DisplayName("Test Initialization with Zero Values")
            public void testComplexInitializationZero() {
                Complex complex = new Complex(0.0, 0.0);
                Assertions.assertEquals(0.0, complex.getX0(), DELTA, "Real part not initialized correctly with zero value.");
                Assertions.assertEquals(0.0, complex.getX1(), DELTA, "Imaginary part not initialized correctly with zero value.");
            }

            @Test
            @DisplayName("Test Initialization with Positive Values")
            public void testComplexInitializationPositive() {
                Complex complex = new Complex(3.0, 4.0);
                Assertions.assertEquals(3.0, complex.getX0(), DELTA, "Real part not initialized correctly.");
                Assertions.assertEquals(4.0, complex.getX1(), DELTA, "Imaginary part not initialized correctly.");
            }

            @Test
            @DisplayName("Test Initialization with Negative Values")
            public void testComplexInitializationNegative() {
                Complex complex = new Complex(-3.0, -4.0);
                Assertions.assertEquals(-3.0, complex.getX0(), DELTA, "Real part not initialized correctly with negative value.");
                Assertions.assertEquals(-4.0, complex.getX1(), DELTA, "Imaginary part not initialized correctly with negative value.");
            }
        }


        @Nested
        @DisplayName("Tests for the Complex class sqrt method")
        class ComplexSqrtTest {
            @Test
            @DisplayName("Test sqrt with positive real number only")
            public void testSqrtPositiveRealOnly() {
                Complex complex = new Complex(4.0, 0.0);
                Complex result = complex.sqrt();
                Assertions.assertEquals(2.0, result.getX0(), DELTA, "Square root of positive real number is incorrect.");
                Assertions.assertEquals(0.0, result.getX1(), DELTA, "Imaginary part should be zero for positive real square root.");
            }

            @Test
            @DisplayName("Test sqrt with negative real number only")
            public void testSqrtNegativeRealOnly() {
                Complex complex = new Complex(-4.0, 0.0);
                Complex result = complex.sqrt();
                Assertions.assertEquals(0.0, result.getX0(), DELTA, "Real part should be zero for square root of negative real number.");
                Assertions.assertEquals(2.0, result.getX1(), DELTA, "Square root of negative real number is incorrect.");
            }

            @Test
            @DisplayName("Test sqrt with positive imaginary number only")
            public void testSqrtPositiveImaginaryOnly() {
                Complex complex = new Complex(0.0, 4.0);
                Complex result = complex.sqrt();
                Assertions.assertTrue(result.getX0() > 0, "Real part of sqrt of positive imaginary number should be positive.");
                Assertions.assertTrue(result.getX1() > 0, "Imaginary part of sqrt of positive imaginary number should be positive.");
            }

            @Test
            @DisplayName("Test sqrt with negative imaginary number only")
            public void testSqrtComplexNumber() {
                Complex complex = new Complex(1.0, 1.0);
                Complex result = complex.sqrt();
                Assertions.assertTrue(result.getX0() > 0, "Real part of sqrt of complex number should be positive.");
                Assertions.assertTrue(result.getX1() > 0, "Imaginary part of sqrt of complex number should be positive.");
            }
        }
    }

