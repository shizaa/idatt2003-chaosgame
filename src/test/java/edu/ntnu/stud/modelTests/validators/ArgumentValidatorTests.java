package edu.ntnu.stud.modelTests.validators;

import edu.ntnu.stud.model.exception.InvalidFormatException;
import edu.ntnu.stud.model.exception.NullArgumentException;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.exception.InvalidRangeException;
import edu.ntnu.stud.model.validators.ArgumentValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * <p>Test class for the {@code ArgumentValidator} class. The tests are meant to test the
 * validation methods in the {@code ArgumentValidator} class that perhaps has not already been tested by testing other classes.</p>
 *
 * Responsibility: To test the ArgumentValidator class.
 *
 * @see ArgumentValidator
 * @see InvalidFormatException
 * @see NullArgumentException
 * @see InvalidRangeException
 * @author shizaahmad
 */
@DisplayName("ArgumentValidator Tests")
public class ArgumentValidatorTests {

  @Nested
  @DisplayName("Mandelbrot Set Constructor Validator Tests")
  class MandelbrotSetConstructorValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid parameters")
    void testValidParameters() {
      assertDoesNotThrow(() -> ArgumentValidator.mandelbrotSetConstructorValidator(1000, 1000, 500));
    }

    @Test
    @DisplayName("Should throw exception with invalid width")
    void testInvalidWidth() {
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.mandelbrotSetConstructorValidator(999, 1000, 500));
    }

    @Test
    @DisplayName("Should throw exception with invalid height")
    void testInvalidHeight() {
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.mandelbrotSetConstructorValidator(1000, 999, 500));
    }

    @Test
    @DisplayName("Should throw exception with invalid max iterations")
    void testInvalidMaxIterations() {
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.mandelbrotSetConstructorValidator(1000, 1000, 0));
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.mandelbrotSetConstructorValidator(1000, 1000, 1001));
    }
  }

  @Nested
  @DisplayName("Vector2D Constructor Validator Tests")
  class Vector2DConstructorValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid parameters")
    void testValidParameters() {
      assertDoesNotThrow(() -> ArgumentValidator.vector2DConstructorValidator(5, 5));
    }

    @Test
    @DisplayName("Should throw exception with NaN values")
    void testNaNValues() {
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.vector2DConstructorValidator(Double.NaN, 5));
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.vector2DConstructorValidator(5, Double.NaN));
    }

    @Test
    @DisplayName("Should throw exception with out of range values")
    void testOutOfRangeValues() {
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.vector2DConstructorValidator(-1e7, 5));
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.vector2DConstructorValidator(5, -1e7));
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.vector2DConstructorValidator(1e7, 5));
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.vector2DConstructorValidator(5, 1e7));
    }
  }

  @Nested
  @DisplayName("Vector2D Add Validator Tests")
  class Vector2DAddValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid parameter")
    void testValidParameter() {
      assertDoesNotThrow(() -> ArgumentValidator.vector2DAddValidator(new Vector2D(5, 5)));
    }

    @Test
    @DisplayName("Should throw exception with null parameter")
    void testNullParameter() {
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.vector2DAddValidator(null));
    }
  }

  @Nested
  @DisplayName("Coordinate Differences Canvas Validator Tests")
  class CoordinateDifferencesCanvasValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid parameters")
    void testValidParameters() {
      assertDoesNotThrow(() -> ArgumentValidator.validateCoordinateDifferencesCanvas(new Vector2D(5, 5), new Vector2D(6, 6)));
    }

    @Test
    @DisplayName("Should throw exception with invalid parameters")
    void testInvalidParameters() {
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.validateCoordinateDifferencesCanvas(new Vector2D(5, 5), new Vector2D(5, 6)));
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.validateCoordinateDifferencesCanvas(new Vector2D(5, 5), new Vector2D(6, 5)));
    }
  }

  @Nested
  @DisplayName("Non-Null Parameter Controller Validator Tests")
  class NonNullParameterControllerValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid parameters")
    void testValidParameters() {
      assertDoesNotThrow(() -> ArgumentValidator.validateNonNullParameterController(false, "Test message"));
    }

    @Test
    @DisplayName("Should throw exception with invalid parameters")
    void testInvalidParameters() {
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.validateNonNullParameterController(true, "Test message"));
    }
  }

  @Nested
  @DisplayName("Complex Number Not Null Validator Tests")
  class ComplexNumberNotNullValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid parameters")
    void testValidParameters() {
      assertDoesNotThrow(() -> ArgumentValidator.complexNumberNotNullValidator(new Complex(5, 5)));
    }

    @Test
    @DisplayName("Should throw exception with null parameter")
    void testNullParameter() {
      assertThrows(IllegalArgumentException.class, () -> ArgumentValidator.complexNumberNotNullValidator(null));
    }
  }

  @Nested
  @DisplayName("Read Fractal File Validator Tests")
  class ReadFractalFileValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid parameters")
    void testValidParameters() {
      assertDoesNotThrow(() -> ArgumentValidator.readFractalFileValidator("src/main/resources/textFiles/affine.txt", new File("src/main/resources/textFiles/affine.txt")));
    }

    @Test
    @DisplayName("Should throw exception with invalid file path")
    void testInvalidFilePath() {
      assertThrows(FileNotFoundException.class, () -> ArgumentValidator.readFractalFileValidator("invalid/path", new File("invalid/path")));
    }
  }

  @Nested
  @DisplayName("Real Part Value Validator Tests")
  class RealPartValueValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid real part value")
    void testValidRealPartValue() {
      assertDoesNotThrow(() -> ArgumentValidator.validateRealPartValue(5));
    }

    @Test
    @DisplayName("Should throw exception with invalid real part value")
    void testInvalidRealPartValue() {
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.validateRealPartValue(15));
    }
  }

  @Nested
  @DisplayName("Imaginary Part Value Validator Tests")
  class ImaginaryPartValueValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid imaginary part value")
    void testValidImaginaryPartValue() {
      assertDoesNotThrow(() -> ArgumentValidator.validateImaginaryPartValue(5));
    }

    @Test
    @DisplayName("Should throw exception with invalid imaginary part value")
    void testInvalidImaginaryPartValue() {
      assertThrows(InvalidRangeException.class, () -> ArgumentValidator.validateImaginaryPartValue(15));
    }
  }

  @Nested
  @DisplayName("File Path Validator Tests")
  class FilePathValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid file path")
    void testValidFilePath() {
      assertDoesNotThrow(() -> ArgumentValidator.validateFilePath("valid/path"));
    }

    @Test
    @DisplayName("Should throw exception with null file path")
    void testNullFilePath() {
      assertThrows(NullArgumentException.class, () -> ArgumentValidator.validateFilePath(null));
    }

    @Test
    @DisplayName("Should throw exception with empty file path")
    void testEmptyFilePath() {
      assertThrows(NullArgumentException.class, () -> ArgumentValidator.validateFilePath(""));
    }
  }

  @Nested
  @DisplayName("Index Validator Tests")
  class IndexValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid index")
    void testValidIndex() {
      assertDoesNotThrow(() -> ArgumentValidator.validateIndex(5, 10));
    }

    @Test
    @DisplayName("Should throw exception with invalid index")
    void testInvalidIndex() {
      assertThrows(IndexOutOfBoundsException.class, () -> ArgumentValidator.validateIndex(15, 10));
    }
  }

  @Nested
  @DisplayName("Vector Values Validator Tests")
  class VectorValuesValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid vector values")
    void testValidVectorValues() {
      assertDoesNotThrow(() -> ArgumentValidator.validateVectorValues(5, 5));
    }

    @Test
    @DisplayName("Should throw exception with NaN vector values")
    void testNaNVectorValues() {
      assertThrows(InvalidFormatException.class, () -> ArgumentValidator.validateVectorValues(Double.NaN, 5));
      assertThrows(InvalidFormatException.class, () -> ArgumentValidator.validateVectorValues(5, Double.NaN));
    }

    @Test
    @DisplayName("Should throw exception with infinity vector values")
    void testInfinityVectorValues() {
      assertThrows(InvalidFormatException.class, () -> ArgumentValidator.validateVectorValues(Double.POSITIVE_INFINITY, 5));
      assertThrows(InvalidFormatException.class, () -> ArgumentValidator.validateVectorValues(5, Double.POSITIVE_INFINITY));
    }
  }

  @Nested
  @DisplayName("Coordinate Validator Tests")
  class CoordinateValidatorTests {

    @Test
    @DisplayName("Should not throw exception with valid coordinate")
    void testValidCoordinate() {
      assertDoesNotThrow(() -> ArgumentValidator.validateCoordinate(new Vector2D(5, 5), "Test message"));
    }

    @Test
    @DisplayName("Should throw exception with null coordinate")
    void testNullCoordinate() {
      assertThrows(NullArgumentException.class, () -> ArgumentValidator.validateCoordinate(null, "Test message"));
    }
  }
}
