package edu.ntnu.stud.modelTests.transformations;

import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

/**
 * The class AffineTransform2DTest is a test class for the AffineTransform2D class.
 * Responsibility: To test the AffineTransform2D class.
 */
@DisplayName("AffineTransform2D Test Suite")
class AffineTransform2DTest {
  private AffineTransform2D transform;
  private Matrix2x2 matrix;
  private Vector2D vector;

  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
    vector = new Vector2D(5, 6);
    transform = new AffineTransform2D(matrix, vector);
  }

  @Nested
  @DisplayName("Constructor Tests")
  class ConstructorTests {
    @BeforeEach
    void setUpConstructorTests() {
      // Ensure matrix and vector are set up for constructor tests
      matrix = new Matrix2x2(1, 2, 3, 4);
      vector = new Vector2D(5, 6);
    }

    @Test
    @DisplayName("Constructor should initialize with valid arguments")
    void constructorShouldInitializeWithValidArguments() {
      AffineTransform2D transform = new AffineTransform2D(matrix, vector);
      assertNotNull(transform, "AffineTransform2D should be initialized.");
    }

    @Test
    @DisplayName("Constructor should throw exception if matrix is null")
    void constructorShouldThrowExceptionIfMatrixIsNull() {
      Matrix2x2 matrix = null;
      Vector2D vector = new Vector2D(5, 6);
      IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
        new AffineTransform2D(matrix, vector);
      }, "Constructor should throw IllegalArgumentException if matrix is null.");
      assertEquals("Matrix and Vector cannot be null", thrown.getMessage(), "Exception message should match expected message.");
    }

    @Test
    @DisplayName("Constructor should throw exception if vector is null")
    void constructorShouldThrowExceptionIfVectorIsNull() {
      Vector2D vector = null;
      IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
        new AffineTransform2D(matrix, vector);
      }, "Constructor should throw IllegalArgumentException if vector is null.");
      assertEquals("Matrix and Vector cannot be null", thrown.getMessage(), "Exception message should match expected message.");
    }

    @Test
    @DisplayName("Constructor should throw exception if both matrix and vector are null")
    void constructorShouldThrowExceptionIfBothAreNull() {
      Matrix2x2 matrix = null;
      Vector2D vector = null;
      IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
        new AffineTransform2D(matrix, vector);
      }, "Constructor should throw IllegalArgumentException if both matrix and vector are null.");
      assertEquals("Matrix and Vector cannot be null", thrown.getMessage(), "Exception message should match expected message.");
    }
  }

  @Nested
  @DisplayName("Transform Method Tests")
  class TransformMethodTests {
    private AffineTransform2D customTransform;
    private Matrix2x2 customMatrix;
    private Vector2D customVector;

    @BeforeEach
    void setUpTransformTests() {
      double[][] matrixData = {{0.5, 1}, {1, 0.5}};
      customMatrix = new Matrix2x2(matrixData[0][0], matrixData[0][1], matrixData[1][0], matrixData[1][1]);
      customVector = new Vector2D(3, 1);
      customTransform = new AffineTransform2D(customMatrix, customVector);
    }

    @Test
    @DisplayName("Transform should return correct result")
    void transformShouldReturnCorrectResult() {
      Vector2D point = new Vector2D(1, 2);
      Vector2D result = customTransform.transform(point);
      assertEquals(5.5, result.getX0(), "The X coordinate of the result should be 5.5");
      assertEquals(3, result.getX1(), "The Y coordinate of the result should be 3");
    }

    @Test
    @DisplayName("Transform should return incorrect result")
    void transformShouldReturnIncorrectResult() {
      Vector2D point = new Vector2D(1, 2);
      Vector2D result = customTransform.transform(point);
      assertNotEquals(4.5, result.getX0(), "The X coordinate of the result should be 5.5");
      assertNotEquals(2, result.getX1(), "The Y coordinate of the result should be 3");
    }

    @Test
    @DisplayName("Transform point with zero matrix")
    void testTransformPointWithZeroMatrix() {
      Matrix2x2 zeroMatrix = new Matrix2x2(0, 0, 0, 0);
      AffineTransform2D zeroTransform = new AffineTransform2D(zeroMatrix, vector);
      Vector2D point = new Vector2D(1, 2);
      Vector2D result = zeroTransform.transform(point);
      assertEquals(5, result.getX0(), "The x0 component of the resulting point should be 5.");
      assertEquals(6, result.getX1(), "The x1 component of the resulting point should be 6.");
    }

    @Test
    @DisplayName("Set matrix with valid matrix")
    public void testSetMatrix_ValidMatrix() {
      Matrix2x2 newMatrix = new Matrix2x2(2, 3, 4, 5);
      transform.setMatrix(newMatrix);
      assertEquals(newMatrix, transform.getMatrix());
    }

    @Test
    @DisplayName("Set matrix with null matrix")
    public void testSetMatrix_NullMatrix() {
      assertThrows(IllegalArgumentException.class, () -> transform.setMatrix(null));
    }

    @Test
    @DisplayName("Set vector with valid vector")
    public void testSetVector_ValidVector() {
      Vector2D newVector = new Vector2D(7, 8);
      transform.setVector(newVector);
      assertEquals(newVector, transform.getVector());
    }

    @Test
    @DisplayName("Set vector with null vector")
    public void testSetVector_NullVector() {
      assertThrows(IllegalArgumentException.class, () -> transform.setVector(null));
    }
  }
}
