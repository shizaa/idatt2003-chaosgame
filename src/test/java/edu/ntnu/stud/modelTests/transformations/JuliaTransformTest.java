package edu.ntnu.stud.modelTests.transformations;

import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The class JuliaTransformTest is a test class for the JuliaTransform class.
 * Responsibility: To test the JuliaTransform class.
 */
@DisplayName("JuliaTransform Test Suite")
public class JuliaTransformTest {
  private Complex validPoint;
  private int validSign;
  private JuliaTransform transform;

  @BeforeEach
  void setUp() {
    validPoint = new Complex(1.0, 1.0);
    validSign = 1;
    transform = new JuliaTransform(validPoint, validSign);
  }

  @Nested
  @DisplayName("Constructor Tests")
  class ConstructorTests {
    @Test
    @DisplayName("Test that the constructor initializes the instance variables correctly.")
    public void testConstructor_ValidArguments() {
      assertDoesNotThrow(() -> {
        new JuliaTransform(validPoint, validSign);
      });
    }

    @Test
    @DisplayName("Test that the constructor throws IllegalArgumentException when the point is null")
    void testConstructorWithNullPoint() {
      assertThrows(IllegalArgumentException.class, () -> {
        new JuliaTransform(null, 1);
      }, "Complex point cannot be null");
    }

    @Test
    @DisplayName("Test constructor with invalid sign")
    void testConstructorWithInvalidSign() {
      Complex point = new Complex(1, 1);
      assertThrows(IllegalArgumentException.class, () -> {
        new JuliaTransform(point, 0);
      }, "Sign must be either +1 or -1");
    }
  }

  @Nested
  @DisplayName("Transform Method Tests")
  class TransformMethodTests {
    private Complex customPoint;
    private JuliaTransform customTransform;

    @BeforeEach
    void setUpTransformTests() {
      customPoint = new Complex(0, 0);
      customTransform = new JuliaTransform(customPoint, 1);
    }

    @Test
    @DisplayName("Test that the transform method throws IllegalArgumentException when the vector is null")
    void testTransformWithNullVector() {
      assertThrows(IllegalArgumentException.class, () -> {
        transform.transform(null);
      }, "Vector v cannot be null");
    }

    @Test
    @DisplayName("Test that the transform method returns the correct result")
    void testTransform() {
      Vector2D vector = new Vector2D(1, 1);
      Vector2D result = customTransform.transform(vector);

      assertEquals(1.09868411346781, result.getX0(), 1e-10);
      assertEquals(0.45508986056222733, result.getX1(), 1e-10);
    }

    @Test
    @DisplayName("Test that the transform method returns the correct result when the sign is negative")
    public void testTransformWithNegativeSign() {
      Vector2D vector = new Vector2D(1, 1);
      customTransform = new JuliaTransform(customPoint, -1);

      Vector2D result = customTransform.transform(vector);

      assertEquals(-1.09868411346781, result.getX0(), 1e-10);
      assertEquals(-0.45508986056222733, result.getX1(), 1e-10);
    }
  }

  @Nested
  @DisplayName("Get Complex Method Tests")
  class GetComplexMethodTests {
    private Complex newComplex;

    @BeforeEach
    void setUpGetComplexTests() {
      newComplex = new Complex(2.0, 2.0);
    }

    @Test
    @DisplayName("Test that the getComplex method returns the correct complex number")
    public void testSetComplex_ValidComplex() {
      transform.setComplex(newComplex);
      assertEquals(newComplex, transform.getComplex());
    }

    @Test
    @DisplayName("Test that the setComplex method throws IllegalArgumentException when the complex number is null")
    public void testSetComplex_NullComplex() {
      assertThrows(IllegalArgumentException.class, () -> transform.setComplex(null));
    }
  }
}
