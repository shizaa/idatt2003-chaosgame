package edu.ntnu.stud.controllertTests;

import edu.ntnu.stud.controller.MediatorController;
import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.controller.fractalactioncontroller.MandelbrotController;
import edu.ntnu.stud.controller.fractalactioncontroller.UiHandler;
import edu.ntnu.stud.controller.parameteractioncontroller.FileOperations;
import edu.ntnu.stud.controller.parameteractioncontroller.ParameterController;
import edu.ntnu.stud.model.chaosgame.*;
import edu.ntnu.stud.view.panels.PanelCreator;
import edu.ntnu.stud.view.panels.ParameterElements;
import javafx.application.Platform;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mockStatic;

import org.mockito.junit.jupiter.MockitoExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("ActionController Tests")
public class ActionControllerJavaFxTests extends ApplicationTest {

  private ParameterController parameterController;

  @Mock
  private MediatorController mediatorController;

  @Mock
  private ChaosGameFractalFactory fractalFactory;

  @Mock
  private UiHandler uiHandler;

  @Mock
  private MandelbrotController mandelbrotController;

  @Mock
  private ChaosGameFileHandler chaosGameFileHandler;

  @InjectMocks
  private ActionController actionController;

  @Mock
  private FileOperations fileOperations;

  private ParameterElements parameterElements;

  @BeforeEach
  public void setUp() {
    Platform.runLater(() -> {
      parameterElements = new ParameterElements();
      parameterController = new ParameterController(parameterElements, mediatorController, fileOperations, new PanelCreator(new ChaosGameFractalFactory()));
      actionController = new ActionController(parameterController, mediatorController,
              fractalFactory,
              uiHandler,
              mandelbrotController);
      actionController.setChaosGameFileHandler(chaosGameFileHandler);
    });
    WaitForAsyncUtils.waitForFxEvents();
  }


  @Nested
  @DisplayName("Confirm Button Tests")
  class ConfirmButtonTests {

    @Test
    @DisplayName("Display Fractals Combo - File Not Found")
    public void testDisplayFractalsComboFileNotFound() throws FileNotFoundException {
      TextArea feedbackTextArea = new TextArea();
      ImageView imageView = new ImageView();
      ComboBox<String> displayFractalsCombo = new ComboBox<>();
      displayFractalsCombo.getItems().addAll("InvalidFractal", "Mandelbrot", "Julia"); // Add some sample items
      displayFractalsCombo.setValue("InvalidFractal");

      mockStatic(ChaosGameDescriptionFactory.class);
      when(ChaosGameDescriptionFactory.createFractal(anyString())).thenThrow(FileNotFoundException.class);

      runOnFxThread(() -> actionController.displayFractalsCombo(feedbackTextArea, imageView, displayFractalsCombo));
      WaitForAsyncUtils.waitForFxEvents();

      assertTrue(feedbackTextArea.getText().contains("File not found"));
    }
  }

  @Test
  public void testPrintAsciiButtonCanvasNull() {
    TextArea feedbackTextArea = new TextArea();
    ImageView imageView = new ImageView();

    actionController.printAsciiButton(feedbackTextArea, imageView);

    assertTrue(feedbackTextArea.getText().contains("No fractal to display. Please run iterations first."));
    assertNull(imageView.getImage());
  }

  @Test
  public void testGenerateFractalImageMandelbrot() {
    when(mandelbrotController.getMandelbrotIterations()).thenReturn(1000);
    Image result = actionController.generateFractalImage("Mandelbrot");
    assertNotNull(result);
    verify(mandelbrotController).setActive(true);
  }

  private void runOnFxThread(Runnable action) {
    WaitForAsyncUtils.asyncFx(action);
  }
}
