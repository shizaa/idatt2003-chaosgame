package edu.ntnu.stud.controllertTests;

import edu.ntnu.stud.controller.MediatorController;
import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.controller.fractalactioncontroller.MandelbrotController;
import edu.ntnu.stud.model.chaosgame.*;
import edu.ntnu.stud.model.exception.FileOperationException;
import edu.ntnu.stud.model.exception.InvalidFormatException;
import edu.ntnu.stud.model.validators.InputValidator;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("ActionController Tests")
public class ActionControllerNonJavaFxTests extends ApplicationTest {


  @Mock
  private MediatorController mediatorController;

  @Mock
  private ChaosGameFractalFactory fractalFactory;

  @Mock
  private MandelbrotController mandelbrotController;

  @Mock
  private ChaosGameFileHandler chaosGameFileHandler;

  @InjectMocks
  private ActionController actionController;


  @Nested
  @DisplayName("Confirm Name Button Tests")
  class ConfirmNameButtonTests {

    @Test
    @DisplayName("Confirm Name Button - Valid Input")
    public void testConfirmNameButtonValidInput() throws InvalidFormatException, FileOperationException {
      ComboBox<String> fileComboBox = new ComboBox<>();
      TextArea feedbackTextArea = new TextArea();
      ComboBox<String> createEmptyFractalCombo = new ComboBox<>();
      createEmptyFractalCombo.setValue("Affine");
      TextField emptyFractalName = new TextField("testFractal");
      TextField numTransformationsField = new TextField("3");

      ChaosGameDescription description = mock(ChaosGameDescription.class);
      when(fractalFactory.createDescription(anyString(), any(TextField.class))).thenReturn(description);

      runOnFxThread(() -> actionController.confirmNameButton(fileComboBox, feedbackTextArea, createEmptyFractalCombo, emptyFractalName, numTransformationsField));
      WaitForAsyncUtils.waitForFxEvents();

      verify(chaosGameFileHandler).writeToFile(eq(description), anyString());
      verify(mediatorController).updateFileComboBox(eq(fileComboBox));
      assertTrue(feedbackTextArea.getText().contains("New fractal file created successfully"));
    }


    @Test
    @DisplayName("Confirm Name Button - File Operation Error")
    public void testConfirmNameButtonFileOperationError() throws FileOperationException, InvalidFormatException {
      ComboBox<String> fileComboBox = new ComboBox<>();
      TextArea feedbackTextArea = new TextArea();
      ComboBox<String> createEmptyFractalCombo = new ComboBox<>();
      createEmptyFractalCombo.setValue("Affine");
      TextField emptyFractalName = new TextField("testFractal");
      TextField numTransformationsField = new TextField("3");

      ChaosGameDescription description = mock(ChaosGameDescription.class);
      when(fractalFactory.createDescription(anyString(), any(TextField.class))).thenReturn(description);
      doThrow(new FileOperationException("File operation error")).when(chaosGameFileHandler).writeToFile(eq(description), anyString());

      runOnFxThread(() -> actionController.confirmNameButton(fileComboBox, feedbackTextArea, createEmptyFractalCombo, emptyFractalName, numTransformationsField));
      WaitForAsyncUtils.waitForFxEvents();

      assertTrue(feedbackTextArea.getText().contains("File operation error"));
    }
  }


  @Nested
  @DisplayName("Mandelbrot Iteration Tests")
  class MandelbrotIterationTests {

    @Test
    @DisplayName("Increase Mandelbrot Iterations")
    public void testIncreaseMandelbrotIterations() {
      actionController.increaseMandelbrotIterations();
      verify(mandelbrotController).increaseIterations();
    }

    @Test
    @DisplayName("Decrease Mandelbrot Iterations")
    public void testDecreaseMandelbrotIterations() {
      actionController.decreaseMandelbrotIterations();
      verify(mandelbrotController).decreaseIterations();
    }
  }

  @Nested
  @DisplayName("Canvas Color Tests")
  class CanvasColorTests {

    @Test
    @DisplayName("Change Color of Canvas")
    public void testChangeColorOfCanvas() {
      ImageView imageView = new ImageView();
      actionController.canvas = mock(ChaosGameCanvas.class);

      // Create a mock Image to return
      Image mockImage = new Image("file:src/test/resources/sample.png");

      // Ensure the mock canvas returns this image
      when(actionController.canvas.generateFractalImage()).thenReturn(mockImage);

      runOnFxThread(() -> actionController.cycleCanvasColor(imageView));
      WaitForAsyncUtils.waitForFxEvents();

      // Verify the image view has been updated with the mock image
      assertNotNull(imageView.getImage());
      assertEquals(mockImage, imageView.getImage());
    }

    @Test
    @DisplayName("Reset Color of Canvas")
    public void testResetColorOfCanvas() {
      ImageView imageView = new ImageView();
      actionController.canvas = mock(ChaosGameCanvas.class);

      // Create a mock Image to return
      Image mockImage = new Image("file:src/test/resources/sample.png");

      // Ensure the mock canvas returns this image
      when(actionController.canvas.generateFractalImage()).thenReturn(mockImage);

      runOnFxThread(() -> actionController.resetColorOfCanvas(imageView));
      WaitForAsyncUtils.waitForFxEvents();

      // Verify the image view has been updated with the mock image
      assertNotNull(imageView.getImage());
      assertEquals(mockImage, imageView.getImage());
    }
  }


  @Nested
  @DisplayName("Iteration Tests")
  class IterationTests {

    @Test
    @DisplayName("Confirm Iterations Button - Valid Input")
    public void testConfirmIterationsButtonValidInput() throws InvalidFormatException {
      TextArea feedbackTextArea = new TextArea();
      TextField iterationsField = new TextField("1000");
      actionController.description = mock(ChaosGameDescription.class);
      actionController.game = mock(ChaosGame.class);

      runOnFxThread(() -> actionController.confirmIterationsButton(feedbackTextArea, iterationsField));
      WaitForAsyncUtils.waitForFxEvents();

      verify(actionController.game).runSteps(1000);
      assertTrue(feedbackTextArea.getText().contains("Iterations completed successfully"));
    }

    @Test
    @DisplayName("Confirm Iterations Button - Invalid Input")
    public void testConfirmIterationsButtonInvalidInput() throws InvalidFormatException {
      TextArea feedbackTextArea = new TextArea();
      TextField iterationsField = new TextField("invalid");

      try (var mockedValidator = Mockito.mockStatic(InputValidator.class)) {
        mockedValidator.when(() -> InputValidator.validateNumberInput(anyString(), anyString()))
                .thenThrow(new InvalidFormatException("Invalid number of iterations entered"));

        runOnFxThread(() -> actionController.confirmIterationsButton(feedbackTextArea, iterationsField));
        WaitForAsyncUtils.waitForFxEvents();
      }

      assertTrue(feedbackTextArea.getText().contains("Invalid number of iterations entered"));
    }
  }

  private void runOnFxThread(Runnable action) {
    WaitForAsyncUtils.asyncFx(action);
  }
}
