package edu.ntnu.stud;

import edu.ntnu.stud.config.AppInitializer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;


/**
 * <h3>Main</h3>
 * <p>Main class that initialized the program by calling the {@link AppInitializer} class.
 * The class extends {@link Application} and overrides the start method. </p>
 * Responsibility: Start the JavaFX model and initialize the program.
 *
 * @author Shiza Ahmad
 * @version v1.0.0
 * @see AppInitializer
 * @since v0.1.0
 */


public class Main extends Application {
  private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

  /**
   * <p>Main method that launches the JavaFX application.</p>
   *
   * @param args The arguments that are passed to the main method.
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * <p>Overrided start method that initializes the program by calling the {@link AppInitializer}
   * class. Surrounded by try catch block to catch any exceptions that might occur.</p>
   *
   * @param primaryStage The stage that is initialized in the start method.
   */
  @Override
  public void start(Stage primaryStage) {
    try {
      AppInitializer appInitializer = new AppInitializer();
      appInitializer.initialize(primaryStage);

    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, "An exception occurred", e);
    }
  }
}
