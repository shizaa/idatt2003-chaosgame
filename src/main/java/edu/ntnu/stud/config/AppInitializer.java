package edu.ntnu.stud.config;

import edu.ntnu.stud.controller.fractalactioncontroller.UiHandler;
import edu.ntnu.stud.utils.DependencyManager;
import edu.ntnu.stud.controller.MediatorController;
import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.controller.fractalactioncontroller.MandelbrotController;
import edu.ntnu.stud.controller.parameteractioncontroller.FileOperations;
import edu.ntnu.stud.controller.parameteractioncontroller.ParameterController;
import edu.ntnu.stud.model.chaosgame.ChaosGameCanvas;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler;
import edu.ntnu.stud.model.chaosgame.ChaosGameFractalFactory;
import edu.ntnu.stud.model.controls.KeyboardsControls;
import edu.ntnu.stud.view.display.ChaosGameDisplay;
import edu.ntnu.stud.view.panels.ParameterElements;
import edu.ntnu.stud.view.panels.PanelCreator;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import java.util.Objects;

/**
 * <p>The {@code AppInitializer} class initializes the program by calling the appropriate classes.
 * This class is responsible for setting up the main components and controllers of the application.
 * </p>

 * Responsibility: Initialize the program.
 *
 * @see Scene
 * @see Stage
 * @see TextArea
 * @see KeyEvent
 * @see DependencyManager
 * @author Shiza Ahmad
 * @version v1.0.0
 * @since v0.1.0
 */

public class AppInitializer {

  private final ChaosGameDisplay display;
  private final KeyboardsControls keyboardsControls;

  /**
   * <p>Constructor for the {@code AppInitializer} class.
   * Initializes the main components and controllers of the application.
   * Utilizes the {@code DependencyManager} class to create the necessary objects.
   * </p>
   */

  public AppInitializer() {

    ParameterElements parameterElements = new ParameterElements();

    DependencyManager dependencyManager = new DependencyManager();

    MediatorController mediatorController = dependencyManager.createMediatorController();
    ChaosGameFileHandler chaosGameFileHandler = dependencyManager.createChaosGameFileHandler();
    FileOperations fileOperations = dependencyManager.createFileOperations(chaosGameFileHandler);
    ChaosGameDescription chaosGameDescription = dependencyManager.createChaosGameDescription();
    ChaosGameFractalFactory chaosGameFractalFactory = dependencyManager.createChaosGameFractalFactory();
    PanelCreator panelCreator = dependencyManager.createPanelCreator(chaosGameFractalFactory);
    ParameterController parameterController = dependencyManager.createParameterController(parameterElements, mediatorController, fileOperations, panelCreator);
    UiHandler uiHandler = dependencyManager.createUIHandler();
    MandelbrotController mandelbrotController = dependencyManager.createMandelbrotController();
    ActionController actionController = dependencyManager.createActionController(parameterController, mediatorController, chaosGameFractalFactory, uiHandler, mandelbrotController);
    ChaosGameCanvas chaosGameCanvas = dependencyManager.createChaosGameCanvas();

    display = dependencyManager.createChaosGameDisplay(actionController, parameterController, chaosGameFileHandler, chaosGameDescription, chaosGameCanvas, panelCreator);

    keyboardsControls = dependencyManager.createKeyboardsControls();
    dependencyManager.createKeyboardController(display, keyboardsControls);
  }

  /**
   * <p>Initializes the program by setting up the main components and controllers of the application.
   * </p>
   *
   * @param primaryStage The stage that is initialized in the start method.
   */

  public void initialize(Stage primaryStage) {
    Scene scene = new Scene(display.getLayout());
    scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/stylesheets/displayStyles.css")).toExternalForm());
    scene.addEventHandler(KeyEvent.KEY_PRESSED, keyboardsControls);
    scene.addEventHandler(KeyEvent.KEY_RELEASED, keyboardsControls);

    primaryStage.setTitle("Chaos Game");
    primaryStage.setScene(scene);
    primaryStage.setFullScreen(true);
    primaryStage.show();
  }
}
