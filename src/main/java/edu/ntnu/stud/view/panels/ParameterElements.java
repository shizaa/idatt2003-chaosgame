package edu.ntnu.stud.view.panels;

import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * <p>The {@code ParameterElements} class is responsible for creating the different elements
 * that are used in the change parameters part of the functionality. </p>
 *
 * <p>The class contains methods that create the different elements such as text fields, buttons,
 * labels, and text areas. </p>
 * Responsibility: Create the different elements used in the parameter panel.
 *
 * @author Shiza Ahmad
 * @version v1.0.0
 * @see ParameterElements
 * @see GridPane
 * @see VBox
 * @see HBox
 * @see TextField
 * @see TextArea
 * @see Button
 * @see Label
 * @since v0.1.0
 */
public class ParameterElements {

  public Stage editorStage;

  /**
   * <p> Constructor for the {@code ParameterElements} class.
   * Implemented for testing purposes </p>
   */
  public ParameterElements() {
    Platform.runLater(() -> editorStage = new Stage());
  }

  /**
   * <p> Method that returns a {@code HBox} with the given buttons. </p>
   *
   * @param nextButton The button that is used to display the next transformation.
   * @param prevButton The button that is used to display the previous transformation.
   * @return The {@code HBox} with the given buttons.
   */
  public static HBox getNextPrevTransformationButtonBox(Button nextButton, Button prevButton) {
    HBox buttonBox = new HBox(10, nextButton, prevButton);
    buttonBox.getStyleClass().add("hbox-bottom");
    return buttonBox;
  }

  /**
   * <p> Method that returns a {@code GridPane} with the appropriate styling and the
   * given number of {@code TextField} elements. </p>
   *
   * @param textFields The list of {@code TextField} elements
   *                   that are added to the {@code GridPane}.
   * @return The {@code GridPane} with the given {@code TextField} elements.
   */
  public GridPane getGridPane(List<TextField> textFields) {
    GridPane grid = new GridPane();
    grid.setHgap(20);
    grid.setVgap(20);
    grid.setPadding(new Insets(10, 20, 20, 20));

    for (int i = 0; i < textFields.size(); i++) {
      TextField textField = textFields.get(i);
      textField.getStyleClass().add("text-field");
      grid.add(textFields.get(i), i % 2, i / 2);
    }
    return grid;
  }

  /**
   * <p> Method that returns a {@code VBox} with the appropriate styling and the
   * given elements. </p>
   *
   * @param buttonBox The {@code HBox} that contains the buttons.
   * @param grid The {@code GridPane} that contains the {@code TextField} elements.
   * @param fileNameField The {@code TextField} that is used to enter the file name.
   * @param textFields The list of {@code TextField}
   *                   elements that are added to the {@code GridPane}.
   * @param feedbackTextArea The {@code TextArea} that is used to display feedback.
   * @param action The action that is performed when the submit button is pressed.
   * @return The {@code VBox} with the given elements.
   */
  public VBox createVbox(
          HBox buttonBox,
          GridPane grid,
          TextField fileNameField,
          List<TextField> textFields,
          TextArea feedbackTextArea,
          BiConsumer<List<TextField>, TextArea> action) {
    Label instructions = getLabel();
    Button submitButton = getSubmitButton(textFields, feedbackTextArea, action);
    HBox fileBox = gethBox(new HBox(20, fileNameField, submitButton));

    VBox vbox = new VBox(20, instructions, grid, fileBox, buttonBox);
    vbox.setAlignment(Pos.CENTER);
    return vbox;
  }

  /**
   * Method that returns a {@code HBox} with the appropriate styling.
   *
   * @param fileNameField The {@code TextField} that is used to enter the file name.
   * @return The {@code HBox} with the given {@code TextField}.
   */
  public HBox gethBox(HBox fileNameField) {
    fileNameField.setAlignment(Pos.CENTER);
    return fileNameField;
  }

  /**
   * Method that returns a {@code Button} with the given text and action.
   *
   * @param textFields The list of {@code TextField}
   *                   elements that are added to the {@code GridPane}.
   *
   * @param feedbackTextArea The {@code TextArea} that is used to display feedback.
   *
   * @param action The action that is performed when the submit button is pressed.
   * @return The {@code Button} with the given text and action.
   */
  public Button getSubmitButton(
          List<TextField> textFields,
          TextArea feedbackTextArea,
          BiConsumer<List<TextField>, TextArea> action) {
    Button submitButton = new Button("Submit Chages");
    submitButton.getStyleClass().add("button-submit");
    submitButton.setOnAction(l -> action.accept(textFields, feedbackTextArea));
    return submitButton;
  }

  /**
   * Method that returns a {@code TextField} with the appropriate styling.
   *
   * @return The {@code TextField} with the given styling.
   */
  public TextField getTextField() {
    TextField fileNameField = new TextField();
    fileNameField.setPromptText("Enter file name");
    return fileNameField;
  }

  /**
   * Method that returns a {@code Label} with the given text.
   *
   * @return The {@code Label} with the given text.
   */
  public Label getLabel() {
    Label label = new Label(
            "To save changes in a new file, enter name.\n"
                    + "To save in the read file, just press submit.");
    label.getStyleClass().add("label-instructions");
    return label;
  }

  /**
   * Method that returns a {@code Button} with the given text.
   *
   * @param text The text that is displayed on the button.
   * @return The {@code Button} with the given text.
   */
  public Button createButton(String text) {
    return new Button(text);
  }

  /**
   * <p>Method that returns a {@code Button} with the given text and action.
   * The button is used to display the next transformation. </p>
   *
   * @param action The action that is performed when the button is pressed.
   * @return The {@code Button} with the given text and action.
   */
  public Button createNextTransformationButton(BiConsumer<Void, Void> action) {
    Button nextButton = createButton("Next Transformation");
    nextButton.getStyleClass().add("button-nav");

    nextButton.setOnAction(e -> action.accept(null, null));
    return nextButton;
  }

  /**
   * <p> Method that returns a {@code Button} with the given text and action.
   * The button is used to display the previous transformation. </p>
   *
   * @param action The action that is performed when the button is pressed.
   * @return The {@code Button} with the given text and action.
   */
  public Button createPrevTransformationButton(BiConsumer<Void, Void> action) {
    Button prevButton = createButton("Previous Transformation");
    prevButton.getStyleClass().add("button-nav");
    prevButton.setOnAction(e -> action.accept(null, null));
    return prevButton;
  }

  /**
   * <p> Method that returns a {@code List} of
   * {@code TextField} elements based on the given transformation.
   * Checks the type of the transformation and
   * creates the appropriate number of {@code TextField} elements. </p>
   *
   * @param transform The transformation that is used to create the {@code TextField} elements.
   * @return The {@code List} of {@code TextField} elements.
   */
  public List<TextField> createTextFields(Object transform) {
    if (transform instanceof Vector2D vector) {
      return List.of(new TextField(String.valueOf(vector.getX0())),
              new TextField(String.valueOf(vector.getX1())));
    } else if (transform instanceof Matrix2x2 matrix) {
      return List.of(
              new TextField(String.valueOf(matrix.getA00())),
              new TextField(String.valueOf(matrix.getA01())),
              new TextField(String.valueOf(matrix.getA10())),
              new TextField(String.valueOf(matrix.getA11()))
      );
    }
    throw new IllegalArgumentException("Unknown transformation type");
  }

  /**
   * <p> Method that set up the {@code Stage} with the given title. </p>
   *
   * @param title The title of the {@code Stage}.
   */
  public void setupEditorStage(String title) {
    editorStage.setTitle(title);
    editorStage.setAlwaysOnTop(true);
  }

  /**
   * <p> Method that sets the given {@code VBox} as the scene of the {@code Stage}. </p>
   *
   * @param vbox The {@code VBox} that is set as the scene of the {@code Stage}.
   */
  public void setScene(VBox vbox) {
    Scene scene = new Scene(vbox);
    String css = Objects.requireNonNull(getClass()
            .getResource("/stylesheets/displayStyles.css")).toExternalForm();
    scene.getStylesheets().add(css);
    editorStage.setScene(scene);
    editorStage.show();
  }

  /**
   * Method used in {@code AppInitializer} to create a {@code ComboBox} with the file names.
   *
   * @return The {@code ComboBox}.
   */
  public ComboBox<String> getFileComboBox() {
    return new ComboBox<>();
  }
}