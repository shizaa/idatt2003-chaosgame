package edu.ntnu.stud.view.panels;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * <p>The {@code BottomPanel} class is responsible for creating the panel at the application.
 * This panel will contain the "Clear"-button and the "Exit".button. </p>
 *
 * <p>This class contains a constructor for the panel, functions for creating the buttons
 * and the getters for the buttons</p>
 * Responsibility: Creating and implementing getters for the bottom panel.
 *
 * @see PanelCreator
 * @see Button
 * @author Shiza Ahmad
 * @version v1.0.0
 * @since v0.1.0
 */

public class BottomPanel {
  private static Button clearCanvasButton;
  private static Button exitButton;

  /**
   * <p>Constructor for the {@code BottomPanel} class.</p>
   *
   * <p>Creates two new buttons for clearing the canvas and exiting the application.</p>
   */

  public BottomPanel() {
    clearCanvasButton = createButton("Clear canvas");
    exitButton = createButton("Exit application");
  }

  /**
   * <p>Getter for the "Clear canvas" button.
   * This method is called by {@code ChaosGameDisplay} when it
   * will set the action for the button.</p>
   *
   * @return The "Clear canvas" button.
   */
  public  Button getClearCanvasButton() {
    return clearCanvasButton;
  }

  /**
   * <p>Getter for the "Exit application" button.
   * This method is called by {@code ChaosGameDisplay} when it
   * will set the action for the button.</p>
   *
   * @return The "Exit application" button.
   */
  public Button getExitButton() {
    return exitButton;
  }

  /**
   * <p>Creates a new {@code Button} with the given text. This method is called in the constructor
   * and {@code createBottomHBox} when they need buttons.</p>
   *
   * @param text The text for the button.
   * @return A new {@code Button} with the given text.
   */

  public static Button createButton(String text) {
    Button button = new Button(text);
    button.setPrefWidth(220);
    return button;
  }

  /**
   * <p>Creates a new {@code HBox} with the "Clear canvas" and "Exit application" buttons.
   * This method is called by {@code PanelCreator} when it will create the bottom panel.</p>
   *
   * @return A new {@code HBox} with the "Clear canvas" and "Exit application" buttons.
   */

  public static HBox createBottomHbox() {
    clearCanvasButton = createButton("Clear canvas");
    exitButton = createButton("Exit application");
    exitButton.getStyleClass().add("button-exit");
    exitButton.setPrefWidth(220);

    HBox bottomHbox = new HBox(160);
    bottomHbox.getChildren().addAll(clearCanvasButton, exitButton);

    return bottomHbox;
  }

}