package edu.ntnu.stud.view.panels;

import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.model.chaosgame.ChaosGameFractalFactory;
import edu.ntnu.stud.view.display.ChaosGameDisplay;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import java.io.File;

/**
 * The {@code PanelCreator} class is responsible for creating the different panels
 * that are used in the application.
 *
 * <p>The class contains methods that create the different panels such as the title panel,
 * the content panel, and the control panel.
 * </p>
 * Responsibility: Create the different panels used in the application.
 *
 * @author Shiza Ahmad
 * @version v1.0.0
 * @see FeedbackPanel
 * @see BottomPanel
 * @since v0.1.0
 */

public class PanelCreator {
  private static File folder;
  private static Button changeMatrix;
  private static Button changeVector;
  private static Button changeConstantC;
  private static Button changeMinCoords;
  private static Button changeMaxCoords;
  private static ComboBox<String> displayFractalsCombo;
  private static ComboBox<String> fileComboBox;
  private static Button confirmButton;
  private static TextField iterationsField;
  private static Button confirmIterationsButton;
  private static Button printAsciiButton;
  private static ComboBox<String> createEmptyFractalCombo;
  private static TextField emptyFractalName;
  private final ChaosGameFractalFactory fractalFactory;
  private Button confirmNameEmptyFractalFile;
  private TextArea feedbackTextArea;
  private TextField numTransformationsField;

  private ImageView imageView;

  /**
   * <p>Constructor for the {@code PanelCreator} class.</p>
   *
   * <p>Creates a new {@code PanelCreator} object
   * with the given {@code ChaosGameFractalFactory} object.</p>
   *
   * @param fractalFactory The {@code ChaosGameFractalFactory}
   *                       object that is used to create the fractals.
   */
  public PanelCreator(ChaosGameFractalFactory fractalFactory) {
    this.fractalFactory = fractalFactory;
  }

  /**
   * <p>Creates a {@code HBox} object that contains the title of the application.
   * It sets a style for the HBox. This method is called by {@link ChaosGameDisplay} to
   * place the HBox in the display.</p>
   *
   * @return new styled {@code HBox} object that contains the title of the application.
   */
  public static HBox getTitleHbox() {
    HBox titleHbox = new HBox();
    titleHbox.setAlignment(Pos.CENTER);
    titleHbox.setPrefHeight(120);
    AnchorPane.setLeftAnchor(titleHbox, 0.0);
    AnchorPane.setRightAnchor(titleHbox, 0.0);

    Text titleText = new Text("CHAOS GAME");
    titleText.setFill(Color.WHITE);
    titleText.setFont(new Font(70));

    titleHbox
            .widthProperty()
            .addListener(
                    (observable, oldValue, newValue) -> {
                      double width = newValue.doubleValue();
                      double fontSize = 70 * (width / 1200);
                      titleText.setFont(
                              new Font(
                                      fontSize > 10 ? fontSize : 10)); 
                    });

    titleHbox.getChildren().add(titleText);
    return titleHbox;
  }

  /**
   * <p>Creates a {@code VBox} object that contains the right side of the control panel. This side
   * contains the parameter-buttons of the application. This method is called by
   * {@link #createControlHbox} to place the {@code VBox} in the {@code HBox} that it returns.</p>
   *
   * @return new {@code VBox} object that contains the right side of the control panel.
   */
  public static VBox createControlVbox2() {
    VBox controlVBox2 = new VBox(20);
    controlVBox2.setAlignment(Pos.TOP_CENTER);
    controlVBox2.setPrefSize(300, 470);

    Separator divider5 = createDivider();
    Separator divider6 = createDivider();

    Text changeText = new Text("Change fractal parameters (read file first): ");
    changeText.setFill(Color.WHITE);
    changeText.setFont(new Font(15));

    changeMatrix = createButton("Change matrix A values");
    changeVector = createButton("Change vector B values");
    changeConstantC = createButton("Change constant C value");
    changeMinCoords = createButton("Change min coordinates");
    changeMaxCoords = createButton("Change max coordinates");

    changeMatrix.setVisible(false);
    changeVector.setVisible(false);
    changeConstantC.setVisible(false);
    changeMinCoords.setVisible(false);
    changeMaxCoords.setVisible(false);

    controlVBox2
            .getChildren()
            .addAll(
                    divider5,
                    changeText,
                    divider6,
                    changeMatrix,
                    changeVector,
                    changeConstantC,
                    changeMinCoords,
                    changeMaxCoords);

    return controlVBox2;
  }

  /**
   * <p>Create a {@code VBox} object that contains the generate fractal from read file.
   * This method is called by {@link #createControlVbox1} to place the {@code VBox} in the {@code VBox}
   * that it returns.
   *
   * @return new {@code VBox} object that contains the generate fractal from read file.
   */
  private static VBox createGenerateFractalBox() {
    Separator divider4 = createDivider();
    Separator divider1 = createDivider();

    Text generateText = new Text("Generate fractal from read file: ");
    generateText.setFill(Color.WHITE);
    generateText.setFont(new Font(15));

    iterationsField = new TextField();
    iterationsField.setPromptText("Enter number of iterations");
    iterationsField.setPrefWidth(110);

    confirmIterationsButton = new Button("Confirm Iterations");
    confirmIterationsButton.getStyleClass().add("button-confirm");
    confirmIterationsButton.setPrefWidth(120);

    printAsciiButton = new Button("Print ASCII fractal");
    printAsciiButton.setPrefWidth(210);

    HBox iterationButtonBox = new HBox(20);
    iterationButtonBox.setAlignment(Pos.CENTER);
    iterationButtonBox.getChildren().addAll(iterationsField, confirmIterationsButton);

    VBox generateFractalBox = new VBox(20);
    generateFractalBox.setAlignment(Pos.CENTER);
    generateFractalBox.setPadding(new Insets(0, 0, 20, 0)); // Add top padding
    generateFractalBox
            .getChildren()
            .addAll(divider4, generateText, divider1, iterationButtonBox, printAsciiButton);

    return generateFractalBox;
  }

  /**
   * <p>Create a {@code ComboBox} object where the user selects the type of empty fractal to be created.
   * The method is called by {@link #createEmptyFractalBox} to place the {@code ComboBox} in the {@code VBox}.</p>
   */

  private static void createEmptyFractalComboBox() {
    createEmptyFractalCombo = new ComboBox<>();
    createEmptyFractalCombo.setPrefSize(210, 25);
    createEmptyFractalCombo.setPromptText("Create empty fractal");
    createEmptyFractalCombo.getItems().addAll("Julia", "Affine");
  }

  /**
   * <p>Creates a {@code Separator} object that is
   * used to separate the different sections of the control panel.
   * This method is called by {@link #createControlVbox1} to
   * place the {@code Separator} in the {@code VBox}.</p>
   *
   * @return new {@code Separator} object that separates the sections in the control panel.
   */
  private static Separator createDivider() {
    Separator separator = new Separator();
    separator.getStyleClass().add("separator-divider");
    return separator;
  }

  /**
   * <p>Creates a {@code Button} object with the given text, {@code text}. The method is called by
   * the functions in called by many functions in this class to create buttons.</p>
   *
   * @param text, the {@code String}, text, that the button will display.
   * @return new {@code Button} object with the given text.
   */
  private static Button createButton(String text) {
    return BottomPanel.createButton(text);
  }

  /**
   * <p>Returns the {@code Button} object
   * that is used to confirm the creation of an empty fractal file.
   * This method is called when {@link ChaosGameDisplay} will set the action of this button.</p>
   *
   * @return The {@code Button} object that is used to confirm the creation of an empty fractal file.
   */

  public Button getConfirmNameEmptyFractalFile() {
    return confirmNameEmptyFractalFile;
  }

  /**
   * <p>Returns the {@code Button} object that is used to confirm the file to be read.
   * This method is called when {@link ChaosGameDisplay} will set the action of this button.</p>
   *
   * @return the {@code Button} object that is used to confirm the file to be read.
   */
  public Button getConfirmButton() {
    return confirmButton;
  }

  /**
   * <p>Returns the {@code Button} object that is used to confirm the number of iterations.
   * This method is called when {@link ChaosGameDisplay} will set the action of this button.</p>
   *
   * @return the {@code Button} object that is used to confirm the number of iterations.
   */
  public Button getConfirmIterationsButton() {
    return confirmIterationsButton;
  }

  /**
   * <p>Returns the {@code Button} object that is used to print the fractal.
   * This method is called when {@link ChaosGameDisplay} will set the action of this button.</p>
   *
   * @return the {@code Button} object that is used to print the fractal.
   */
  public Button getPrintAsciiButton() {
    return printAsciiButton;
  }

  /**
   * <p>Returns the {@code Button} object that is used to open a panel to change matrix values in
   * affine transform. This method is called when {@link ChaosGameDisplay} will set the action of
   * this button or change its visibility.</p>
   *
   * @return the {@code Button} object that is used to open a panel to change matrix values.
   */
  public Button getChangeMatrix() {
    return changeMatrix;
  }

  /**
   * <p>Returns the {@code Button} object that is used to open a panel to change vector values
   * in affine transform. This method is called when {@link ChaosGameDisplay} will set the
   * action of this button or change its visibility.</p>
   *
   * @return the {@code Button} object that is used to open a panel to change vector values.
   */
  public Button getChangeVector() {
    return changeVector;
  }

  /**
   * <p> Returns the {@code Button} object that is used to open a panel to change the constant C
   * value in Julia Transforms. This method is called when {@link ChaosGameDisplay} will set the
   * action of this button or change its visibility.</p>
   *
   * @return the {@code Button} object that is used to open a panel to change the constant C.
   */
  public Button getChangeConstantC() {
    return changeConstantC;
  }

  /**
   * <p>Returns the {@code Button} object that is used to open a panel to change the minimum coordinates.
   * This method is called when {@link ChaosGameDisplay} will set the action of this button or change
   * its visibility.</p>
   *
   * @return the {@code Button} object that is used to open a panel to change the minimum coordinates.
   */
  public Button getChangeMinCoords() {
    return changeMinCoords;
  }

  /**
   * <p>Returns the {@code TextField} object that the user will enter the name of the empty fractal file.
   * This method is called in {@link ChaosGameDisplay} when it will get the name that the user has entered
   * and pass it to the {@link ActionController} </p>
   *
   * @return the {@code TextField} object that is used to enter the name of the empty fractal file.
   */
  public TextField getEmptyFractalName() {
    return emptyFractalName;
  }

  /**
   * <p>Returns the {@code ComboBox} object that is used to select the type of empty fractal to be created.
   * The selected type will be passed to the {@link ActionController} to create the empty fractal file.</p>
   *
   * @return the {@code ComboBox} object that is used to select the type of empty fractal to be created.
   */
  public ComboBox<String> getCreateEmptyFractalCombo() {
    return createEmptyFractalCombo;
  }

  /**
   * <p>Returns the {@code File} object that is used to get the folder where the fractal files are stored.
   * The method is called by {@link ChaosGameDisplay} to get the folder where the fractal files are stored.</p>
   *
   * @return the {@code File} object that is used to get the folder where the fractal files are stored.
   */
  public File getFolder() {
    return folder;
  }

  /**
   * <p>Returns the {@code Button} object that is used to open a panel to change the maximum coordinates.
   * This method is called when {@link ChaosGameDisplay} will set the action of this button or change
   * its visibility.</p>
   *
   * @return the {@code Button} object that is used to open a panel to change the maximum coordinates.
   */
  public Button getChangeMaxCoords() {
    return changeMaxCoords;
  }

  /**
   * <p>Returns the {@code ComboBox} object where the user selects the predefined fractal to print.
   * This method is called when {@link ChaosGameDisplay} will set the action of this button
   * or when the {@code ComboBox} is passed to {@link ActionController} to print the fractal.</p>
   *
   * @return the {@code ComboBox} object where the user selects the predefined fractal to print.
   */
  public ComboBox<String> getDisplayFractalsCombo() {
    return displayFractalsCombo;
  }

  /**
   * <p>Returns the {@code ComboBox} object where the user selects the file to be read. This method
   * is called when {@link ChaosGameDisplay} will set the action of this button or when the {@code ComboBox}
   * is passed to {@link ActionController} to get the value to print the file.</p>
   *
   * @return the {@code ComboBox} object where the user selects the file to be read.
   */
  public ComboBox<String> getFileComboBox() {
    return fileComboBox;
  }

  /**
   * <p>Returns the {@code TextField} object where the user enters the number of iterations to
   * generate the fractal. this method is called when {@link ChaosGameDisplay} will pass {@code TextField}
   * to {@link ActionController} to get the number of iterations to generate the fractal.</p>
   *
   * @return the {@code TextField} object where the user enters the number of iterations.
   */
  public TextField getIterationsField() {
    return iterationsField;
  }

  /**
   * <p>Returns the {@code TextField}
   * object where the user enters the number of transformations in the
   * fractal file for Affine transformation.
   * This method is called when {@link ChaosGameDisplay} will pass
   * {@code TextField} to {@link ActionController}
   * to get the number of transformations to create the empty fractal file.</p>
   *
   * @return the {@code TextField} object where the user enters the number of transformations.
   */
  public TextField getNumTransformationsField() {
    return numTransformationsField;
  }

  /**
   * <p>Returns the {@code ImageView} object that displays the fractal image. This method is called when
   * The display will change/reset the color of the image or print a new fractal.</p>
   *
   * @return the {@code ImageView} object that displays the fractal image.
   */
  public ImageView getImageView() {
    return imageView;
  }

  /**
   * <p>Returns the {@code TextArea} object that displays the feedback messages. 
   * This method is called when
   * The display will pass the {@code TextArea} 
   * to {@link ActionController} to display a new
   * feedback message.</p>
   *
   * @return the {@code TextArea} object that displays the feedback messages.
   */
  public TextArea getFeedbackTextArea() {
    return feedbackTextArea;
  }

  /**
   * <p>Creates a {@code HBox} object that contains the content of the application.
   * It sets a style for the HBox. This method is called by {@link ChaosGameDisplay} to
   * place the HBox in the display.</p>
   *
   * @return new styled {@code HBox} object that contains the content of the application.
   */
  public HBox createContentHbox() {
    HBox contentHbox = new HBox(10);
    contentHbox.setPadding(new Insets(40, 30, 40, 30));

    Pane imagePane = createImagePane();
    VBox panelItemsBox = createPanelItemsBox();

    contentHbox.getChildren().addAll(imagePane, panelItemsBox);
    HBox.setHgrow(imagePane, Priority.ALWAYS);

    return contentHbox;
  }

  /**
   * <p>Creates a {@code Pane} object that contains the image of the fractal.
   * It sets a style for the Pane. This method is called by {@link ChaosGameDisplay} to
   * place the Pane in the display.</p>
   *
   * @return new styled {@code Pane} object that contains the image of the fractal.
   */
  public Pane createImagePane() {
    Pane imagePane = new Pane();
    
    Text topLeftText =
            new Text("Key P: Increase mandelbrot iterations " +
                    "\nKey M: Decrease mandelbrot iterations " +
                    "\nKey X: Change color \nKey Y: Default color");
    topLeftText.setFill(Color.WHITE);
    topLeftText.setFont(new Font(15));

    // Position the text in the top left corner
    topLeftText.setTranslateX(10); // 10 pixels from the left edge
    topLeftText.setTranslateY(20); // 20 pixels from the top edge

    imageView = new ImageView();
    imageView.fitWidthProperty().bind(imagePane.widthProperty());
    imageView.fitHeightProperty().bind(imagePane.heightProperty());
    imageView.setPreserveRatio(true);

    // Add the text to the image pane
    imagePane.getChildren().addAll(imageView, topLeftText);

    return imagePane;
  }

  /**
   * <p>Creates a {@code VBox} object that contains the feedback panel and control items of
   * the application. This method is called by {@link #createContentHbox} to place the
   * {@code VBox} in the {@code HBox} that it creates.</p>
   *
   * @return new {@code VBox} object that contains
   * the feedback panel and control items of the application.
   */
  public VBox createPanelItemsBox() {
    VBox panelItemsBox = new VBox(35);
    panelItemsBox.setAlignment(Pos.TOP_CENTER);
    panelItemsBox.setPrefSize(600, 670);

    HBox controlHbox = createControlHbox();
    feedbackTextArea = FeedbackPanel.createFeedbackTextArea();
    HBox bottomHbox = BottomPanel.createBottomHbox();

    panelItemsBox.getChildren().addAll(controlHbox, feedbackTextArea, bottomHbox);

    return panelItemsBox;
  }

  /**
   * <p>Creates a {@code HBox} object that contains the control items of the application. This
   * method is called by {@link #createPanelItemsBox} to place the {@code HBox} in the {@code VBox}
   * that it returns.</p>
   *
   * @return new {@code HBox} object that contains the control items of the application.
   */
  public HBox createControlHbox() {
    HBox controlHbox = new HBox(20);
    controlHbox.setPrefSize(440, 470);

    VBox controlVbox1 = createControlVbox1();
    VBox controlVbox2 = createControlVbox2();

    controlHbox.getChildren().addAll(controlVbox1, controlVbox2);
    return controlHbox;
  }

  /**
   * <p>Creates a {@code VBox} object that contains the left side of the control panel. This side
   * contains the fractal-actions. This method is called by {@link #createControlHbox} to place the
   * {@code VBox} in the {@code HBox} that it returns.</p>
   *
   * @return new {@code VBox} object that contains the left side of the control panel.
   */
  public VBox createControlVbox1() {
    VBox controlVbox1 = new VBox(20);
    controlVbox1.setAlignment(Pos.TOP_CENTER);
    controlVbox1.setPrefSize(300, 470);

    Separator divider1 = createDivider();
    Separator divider2 = createDivider();

    Text text = new Text("Create empty fractal file:");
    text.setFill(Color.WHITE);
    text.setFont(new Font(15));

    VBox generateFractalBox = createGenerateFractalBox();
    VBox createEmptyFractalBox = createEmptyFractalBox();
    VBox predifinedBox = createPredifinedBox();

    controlVbox1
            .getChildren()
            .addAll(predifinedBox, generateFractalBox,
                    divider1, text, divider2, createEmptyFractalBox);

    return controlVbox1;
  }

  /**
   * <p>Create a {@code VBox} object that contains the predefined fractal actions.
   * This method is called by {@link #createControlVbox1} 
   * to place the {@code VBox} in the {@code VBox}
   * that it returns.</p>
   *
   * @return new {@code VBox} object that contains the predefined fractal actions.
   */
  private VBox createPredifinedBox() {
    displayFractalsCombo = new ComboBox<>();
    displayFractalsCombo.setPrefSize(245, 25);
    displayFractalsCombo.setPromptText("Display predefined fractals");

    displayFractalsCombo.getItems().addAll(
            "Sierpinski triangle", "Barnsley leaf", "Julia set 1", 
            "Julia set 2", "Julia set 3", "Mandelbrot");

    fileComboBox = new ComboBox<>();
    fileComboBox.setPromptText("Select a file");
    fileComboBox.setPrefWidth(110);
    folder = fractalFactory.populateFileComboBox(fileComboBox);

    confirmButton = new Button("Confirm text file");
    confirmButton.getStyleClass().add("button-confirm");
    confirmButton.setPrefWidth(120);

    HBox displayFractalBox = new HBox(20);
    displayFractalBox.setAlignment(Pos.CENTER);
    displayFractalBox.getChildren().addAll(fileComboBox, confirmButton);

    VBox predifinedBox = new VBox(20);
    predifinedBox.setAlignment(Pos.CENTER);
    predifinedBox.setPadding(new Insets(0, 0, 20, 0));
    predifinedBox.getChildren().addAll(displayFractalsCombo, displayFractalBox);

    return predifinedBox;
  }

  /**
   * <p>Create a {@code VBox} object that contains the create empty fractal actions.
   * This method is called by {@link #createControlVbox1}
   * to place the {@code VBox} in the {@code VBox}
   * that it returns.</p>
   *
   * @return new {@code VBox} object that contains the create empty fractal actions.
   */
  public VBox createEmptyFractalBox() {
    createEmptyFractalComboBox();

    HBox nameNumField = new HBox(20);
    nameNumField.setAlignment(Pos.CENTER);

    emptyFractalName = new TextField();
    emptyFractalName.setMaxWidth(210);
    emptyFractalName.setPromptText("Name of new file");

    numTransformationsTextField();

    nameNumField.getChildren().add(emptyFractalName);

    createEmptyFractalCombo.setOnAction(e ->
            fractalFactory.handleFractalComboBoxAction(
                    createEmptyFractalCombo, nameNumField, numTransformationsField));

    confirmButton();

    VBox createEmptyFractalBox = new VBox(20);
    createEmptyFractalBox.setAlignment(Pos.CENTER);
    createEmptyFractalBox.getChildren().addAll(createEmptyFractalCombo,
            nameNumField, confirmNameEmptyFractalFile);

    return createEmptyFractalBox;
  }

  /**
   * <p>Creates a {@code Button} object with the text "Confirm new fractal file". The method
   * is called by {@link #createEmptyFractalBox} to place the button int the {@code VBox}.</p>
   */
  private void confirmButton() {
    confirmNameEmptyFractalFile = new Button("Confirm new fractal file");
    confirmNameEmptyFractalFile.getStyleClass().add("button-confirm");
    confirmNameEmptyFractalFile.setPrefWidth(220);
  }

  /**
   * <p>Create a {@code TextField} object where the user enters the number of transformations
   * in the fractal file for Affine transformation.
   * This method is called by {@link #createEmptyFractalBox}
   * to place the {@code TextField} in the {@code VBox}.</p>
   */
  private void numTransformationsTextField() {
    numTransformationsField = new TextField();
    numTransformationsField.setMaxWidth(210);
    numTransformationsField.setPromptText("Number of transformations");
    numTransformationsField.setVisible(false);
  }
}
