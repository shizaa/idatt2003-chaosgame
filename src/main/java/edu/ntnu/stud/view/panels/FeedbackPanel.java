package edu.ntnu.stud.view.panels;

import javafx.geometry.Insets;
import javafx.scene.control.TextArea;


/**
 * <p>The {@code FeedbackPanel} class is responsible for creating a panel for feedback messages.
 * The panel contains {@code TextArea} where the user can see feedback messages.</p>
 *
 * <p>The class contains a constructor, and a function for creating the {@code TextArea}.</p>
 * Responsibility: Creating a feedback panel.
 *
 * @author Shiza Ahmad
 * @version v1.0.0
 * @see PanelCreator
 * @see BottomPanel
 * @see TextArea
 * @since v0.1.0
 */

public class FeedbackPanel {
  /**
   * <p>Creates a {@code TextArea} for feedback messages. This method is called in
   * the constructor of the class.</p>
   *
   * @return A {@code TextArea} for feedback messages.
   */
  public static TextArea createFeedbackTextArea() {
    TextArea feedbackTextArea = new TextArea();
    feedbackTextArea.setPrefSize(200, 150);
    feedbackTextArea.setEditable(false);
    feedbackTextArea.getStyleClass().add("text-area-feedback");
    feedbackTextArea.setPadding(new Insets(10, 10, 10, 10));
    feedbackTextArea.setWrapText(true);

    return feedbackTextArea;
  }

}