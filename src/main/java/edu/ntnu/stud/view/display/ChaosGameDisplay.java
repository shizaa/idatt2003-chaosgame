package edu.ntnu.stud.view.display;

import edu.ntnu.stud.config.AppInitializer;
import edu.ntnu.stud.controller.KeyboardController;
import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.controller.parameteractioncontroller.ParameterController;
import edu.ntnu.stud.model.chaosgame.ChaosGame;
import edu.ntnu.stud.model.chaosgame.ChaosGameCanvas;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler;
import edu.ntnu.stud.model.interfaces.ChaosGameObserver;
import edu.ntnu.stud.view.panels.BottomPanel;
import edu.ntnu.stud.view.panels.PanelCreator;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * <p>The class ChaosGameDisplay is responsible for displaying the chaos game.
 * It is an observer of the chaos game
 * and updates the display based on the actions taken by the user.</p>

 * Responsibility: Display the chaos game and
 * update the display based on the actions taken by the user.
 *
 * @see ChaosGameObserver
 * @see ChaosGame
 * @see ChaosGameCanvas
 * @see ChaosGameDescription
 * @see ChaosGameFileHandler
 * @see ActionController
 * @see ParameterController
 * @see PanelCreator
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class ChaosGameDisplay implements ChaosGameObserver {


  public static final int HEIGHT = 1000;
  public static final int WIDTH = 1000;

  private final ActionController actionController;
  private final ParameterController parameterController;
  private final ChaosGame chaosGame;
  private AnchorPane layout;
  private final PanelCreator panelCreator;
  private final BottomPanel bottomPanel = new BottomPanel();

  /**
   * <p>Constructor for the ChaosGameDisplay class. The {@link AppInitializer} class initializes
   * the chaos arguments and they will be set as attributes for the new {@code ChaosGameDisplay}
   * object.</p>
   *
   * @param actionController The action controller for the chaos game.
   * @param parameterController The parameter controller for the chaos game.
   * @param chaosGameFileHandler The file handler for the chaos game.
   * @param chaosGameDescription The description of the chaos game.
   * @param chaosGameCanvas The canvas for the chaos game.
   * @param panelCreator The panel creator for the chaos game.
   */
  public ChaosGameDisplay(
          ActionController actionController,
          ParameterController parameterController,
          ChaosGameFileHandler chaosGameFileHandler,
          ChaosGameDescription chaosGameDescription,
          ChaosGameCanvas chaosGameCanvas,
          PanelCreator panelCreator) {
    this.actionController = actionController;
    this.parameterController = parameterController;
    this.chaosGame = new ChaosGame(chaosGameDescription, chaosGameCanvas);
    this.panelCreator = panelCreator;

    initialize(chaosGameFileHandler);
  }

  /**
   * <p> by setting the chaos game file handler and creating the display layout. It is a
   * submethod of the constructor to set the {@code FileHander} of the {@link ActionController}.
   * It also adds this object as an observer to {@link ChaosGame}. Then it sets the layout.</p>
   *
   * @param chaosGameFileHandler The file handler for the chaos game.
   */

  private void initialize(ChaosGameFileHandler chaosGameFileHandler) {
    this.actionController.setChaosGameFileHandler(chaosGameFileHandler);
    this.chaosGame.addObserver(this);
    createDisplayLayout();
  }

  /**
   * <p>Getter for the title {@code HBox} from {@link PanelCreator}.</p>
   *
   * @return The title HBox for the chaos game display.
   */

  private HBox getTitleHbox() {
    return PanelCreator.getTitleHbox();
  }

  /**
   * <p>Creates the layout for the display of the chaos game. It creates an anchor pane and sets the
   * background color. It then creates the title and content
   * {@code HBox} and adds them to the layout.</p>
   */

  private void createDisplayLayout() {
    layout = new AnchorPane();
    layout.setStyle("-fx-background-color: #2E2F2C;");

    HBox titleHbox = getTitleHbox();
    HBox contentHbox = createContentHbox();

    layout.getChildren().addAll(titleHbox, contentHbox);
    AnchorPane.setTopAnchor(contentHbox, 100.0);
    AnchorPane.setBottomAnchor(contentHbox, 0.0);
    AnchorPane.setLeftAnchor(contentHbox, 0.0);
    AnchorPane.setRightAnchor(contentHbox, 0.0);

    setOnActions();
  }

  /**
   * <p>Creates the content {@code HBox} for the chaos game display. It returns the content {@code HBox}
   * from {@link PanelCreator}. The method is called in {@link #createDisplayLayout}.</p>
   *
   * @return The content HBox for the chaos game display.
   */

  private HBox createContentHbox() {
    return panelCreator.createContentHbox();
  }

  /**
   * <p>Sets the actions for the chaos game display.
   * It sets the file actions, iteration actions, ascii actions,
   * clear and exit actions, fractal actions, and parameter actions.
   * The method is called in {@link #createDisplayLayout}.</p>
   */

  private void setOnActions() {
    setFileActions();
    setIterationActions();
    setAsciiActions();
    setClearExitActions();
    setFractalActions();
    setParameterActions();
  }

  /**
   * <p>Sets the file actions for the chaos game display. It sets the actions for the confirm name empty fractal file
   * and the confirm button. The method is called in {@link #setOnActions}.</p>
   */

  private void setFileActions() {
    panelCreator.getConfirmNameEmptyFractalFile().setOnAction(e -> chaosGame.notifyObservers("CREATE_FILE"));
    panelCreator.getConfirmButton().setOnAction(e -> chaosGame.notifyObservers("READ_FILE"));
  }

  /**
   * <p>Sets the iteration actions for the chaos game display. It sets the action for the confirm iterations button.
   * The method is called in {@link #setOnActions}.</p>
   */

  private void setIterationActions() {
    panelCreator.getConfirmIterationsButton().setOnAction(e ->
            chaosGame.notifyObservers("READ_ITERATIONS"));
  }

  /**
   * <p>Sets the ascii actions for the chaos game display. It sets the action for the print ascii button.
   * The method is called in {@link #setOnActions}.</p>
   */

  private void setAsciiActions() {
    panelCreator.getPrintAsciiButton().setOnAction(e -> chaosGame.notifyObservers("PRINT_ASCII"));
  }

  /**
   * <p>Sets the clear and exit actions for the chaos game display. It sets the actions for the clear canvas button
   * and the exit button. The method is called in {@link #setOnActions}.</p>
   */

  private void setClearExitActions() {
    bottomPanel.getClearCanvasButton().setOnAction(e ->
            chaosGame.notifyObservers("CLEAR_CANVAS"));
    bottomPanel.getExitButton().setOnAction(e -> chaosGame.notifyObservers("EXIT"));
  }

  /**
   * <p>Sets the fractal actions for the chaos game display. It sets the action for the display fractals combo.
   * The method is called in {@link #setOnActions}.</p>
   */

  private void setFractalActions() {
    panelCreator.getDisplayFractalsCombo().setOnAction(e ->
            chaosGame.notifyObservers("DISPLAY_FRACTALS"));
  }

  /**
   * <p>Sets the parameter actions for the chaos game display. It sets the actions for the change matrix, change vector,
   * change constant c, change min coords, and change max coords buttons. The method is called in {@link #setOnActions}.</p>
   */

  private void setParameterActions() {
    panelCreator.getChangeMatrix().setOnAction(e -> chaosGame.notifyObservers("CHANGE_MATRIX"));
    panelCreator.getChangeVector().setOnAction(e -> chaosGame.notifyObservers("CHANGE_VECTOR"));
    panelCreator.getChangeConstantC().setOnAction(e -> chaosGame.notifyObservers("CHANGE_CONSTANT_C"));
    panelCreator.getChangeMinCoords().setOnAction(e -> chaosGame.notifyObservers("CHANGE_MIN_COORDS"));
    panelCreator.getChangeMaxCoords().setOnAction(e -> chaosGame.notifyObservers("CHANGE_MAX_COORDS"));
  }

  /**
   * <p>Method to handle the key pressed event for the chaos game display.
   * Pressing X will change the color of the canvas.
   * It calls the notifyObservers method with the appropriate action based on the key pressed.
   * The method is called in {@link KeyboardController} when X is pressed.</p>
   */

  public void xpressed() {
    chaosGame.notifyObservers("CHANGE_COLOR");
  }

  /**
   * <p>Method to handle the key pressed event for the chaos game display.
   * Pressing Y will reset the color of the canvas.
   * It calls the notifyObservers method with the appropriate action based on the key pressed.
   * The method is called in {@link KeyboardController} when Y is pressed.</p>
   */

  public void ypressed() {
    chaosGame.notifyObservers("RESET_COLOR");
  }

  /**
   * <p>Method to handle the key pressed event for the chaos game display.
   * Pressing P will increase the number of iterations in the mandelbrot fractal.
   * It calls the notifyObservers method with the appropriate action based on the key pressed.
   * The method is called in {@link KeyboardController} when P is pressed.</p>
   */

  public void ppressed() {
    chaosGame.notifyObservers("ADD_ITER");
  }

  /**
   * <p>Method to handle the key pressed event for the chaos game display.
   * Pressing M will decrease the number of iterations in the mandelbrot fractal.
   * It calls the notifyObservers method with the appropriate action based on the key pressed.
   * The method is called in {@link KeyboardController} when M is pressed.</p>
   */

  public void mpressed() {
    chaosGame.notifyObservers("SUB_ITER");
  }

  /**
   * <p>Implements the {@code update} method from the {@link ChaosGameObserver} interface.
   * It updates the display based on the action taken by the user. The method is called in
   * {@link ChaosGame} when the {@code notifyObservers} method is called. The String to notify
   * this function is specified in the submethods of {@link #setOnActions} method.</p>
   *
   * @param action The action taken by the user.
   */

  @Override
  public void update(String action) {
    switch (action) {
      case "CREATE_FILE":
        actionController.confirmNameButton(
                panelCreator.getFileComboBox(),
                panelCreator.getFeedbackTextArea(),
                panelCreator.getCreateEmptyFractalCombo(),
                panelCreator.getEmptyFractalName(),
                panelCreator.getNumTransformationsField()
        );
        break;
      case "ADD_ITER":
        actionController.increaseMandelbrotIterations();
        actionController.displayFractalsCombo(panelCreator.getFeedbackTextArea(),
                panelCreator.getImageView(), panelCreator.getDisplayFractalsCombo());
        break;
      case "SUB_ITER":
        actionController.decreaseMandelbrotIterations();
        actionController.displayFractalsCombo(panelCreator.getFeedbackTextArea(),
                panelCreator.getImageView(), panelCreator.getDisplayFractalsCombo());
        break;
      case "READ_FILE":
        actionController.confirmButton(
                panelCreator.getFileComboBox(), panelCreator.getFolder(),
                panelCreator.getFeedbackTextArea(),
                panelCreator.getChangeMatrix(), panelCreator.getChangeVector(),
                panelCreator.getChangeMaxCoords(),
                panelCreator.getChangeMinCoords(), panelCreator.getChangeConstantC());
        break;
      case "READ_ITERATIONS":
        actionController.confirmIterationsButton(panelCreator.getFeedbackTextArea(),
                panelCreator.getIterationsField());
        break;
      case "PRINT_ASCII":
        actionController.printAsciiButton(panelCreator.getFeedbackTextArea(),
                panelCreator.getImageView());
        break;
      case "CHANGE_COLOR":
        actionController.cycleCanvasColor(panelCreator.getImageView());
        break;
      case "RESET_COLOR":
        actionController.resetColorOfCanvas(panelCreator.getImageView());
        break;
      case "CLEAR_CANVAS":
        panelCreator.getImageView().setImage(null);
        panelCreator.getFeedbackTextArea().appendText("Canvas cleared. \n");
        break;
      case "EXIT":
        Stage stage = (Stage) layout.getScene().getWindow();
        stage.close();
        break;
      case "DISPLAY_FRACTALS":
        actionController.displayFractalsCombo(panelCreator.getFeedbackTextArea(),
                panelCreator.getImageView(), panelCreator.getDisplayFractalsCombo());
        break;
      case "CHANGE_MATRIX":
        parameterController.changeMatrix(panelCreator.getFeedbackTextArea());

        break;
      case "CHANGE_VECTOR":
        parameterController.changeVector(panelCreator.getFeedbackTextArea());

        break;
      case "CHANGE_CONSTANT_C":
        parameterController.changeConstantC(panelCreator.getFeedbackTextArea());

        break;
      case "CHANGE_MIN_COORDS":
        parameterController.changeMinCoords(panelCreator.getFeedbackTextArea());
        break;
      case "CHANGE_MAX_COORDS":
        parameterController.changeMaxCoords(panelCreator.getFeedbackTextArea());
        break;
      default:
        break;
    }
  }
  /**
   * <p>Getter for the layout of the chaos game display.</p>
   *
   * @return The layout of the chaos game display.
   */

  public AnchorPane getLayout() {
    return layout;
  }
}


