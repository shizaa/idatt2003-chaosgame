package edu.ntnu.stud;

/**
 * <h3>Chaos Game Application.</h3>
 *
 * <p>App class to start the main method in Main class.
 * This is needed to start the JavaFX application.
 * </p>
 * Responsibility: Launch the main method in {@code Main} class
 *
 * @author Shiza Ahmad
 * @version v1.0.0
 * @see Main
 * @since v0.1.0
 */

public class ChaosGameApplication {
  public static void main(String[] args) {
    Main.main(args);
  }
}