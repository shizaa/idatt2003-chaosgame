package edu.ntnu.stud.model.format;

import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescriptionFactory;
import edu.ntnu.stud.model.exception.FileOperationException;
import edu.ntnu.stud.model.interfaces.FileFormat;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import edu.ntnu.stud.model.validators.FileExceptionValidator;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <h3>Text File Format</h3>
 * <p>The class {@code TextFileFormat} implements the {@link FileFormat}
 * interface and provides methods for reading
 * and writing {@link ChaosGameDescription} objects to and from text files.</p>

 * Responsibility: Read and write {@link ChaosGameDescription} objects to and from text files.
 *
 * @see FileFormat
 * @see ChaosGameDescription
 * @see FileExceptionValidator
 * @see FileOperationException
 * @see AffineTransform2D
 * @see JuliaTransform
 * @see Vector2D
 * @see Transform2D
 * @see Complex
 * @see Matrix2x2
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class TextFileFormat implements FileFormat {

  /**
   * <p>Reads a ChaosGameDescription object from a file. This method understands the format of the
   * different files of Affine and Julia transformations with its submethods. This method is called in
   * {@link ChaosGameDescriptionFactory} to return the {@link ChaosGameDescription} to it from file.</p>
   *
   * @param path {@code String} the path to the file.
   * @return a new {@code ChaosGameDescription} object.
   * @throws FileNotFoundException if the file is not found.
   */

  @Override
  public ChaosGameDescription readFromFile(String path) throws FileNotFoundException {
    FileExceptionValidator.validateFilePath(path);
    try (Scanner scanner = new Scanner(new File(path))) {
      scanner.useDelimiter(",|\\s+");
      String transformType = identifyTransformation(scanner);
      Vector2D minCoords = readMinMaxCoords(scanner);
      Vector2D maxCoords = readMinMaxCoords(scanner);
      List<Transform2D> transforms = new ArrayList<>();
      scanner.useDelimiter("\\R");
      scanner.tokens()
              .map(String::trim)
              .filter(line -> !line.startsWith("#") && !line.isEmpty())
              .forEach(line -> {
                String[] parts = line.split("#")[0].split(",");
                if (transformType.equals("Affine2D")) {
                  addAffineTransformation(parts, transforms);
                }
                if (transformType.equals("Julia2D")) {
                  addJuliaTransformation(parts, transforms);
                }
              });
      return new ChaosGameDescription(minCoords, maxCoords, transforms);
    } catch (FileNotFoundException e) {
      FileExceptionValidator.logException(e, "File not found: " + path);
      throw e;
    } catch (Exception e) {
      FileExceptionValidator.logException(e, "An unexpected error occurred: " + path);
      throw new RuntimeException("An unexpected error occurred while reading the file: " + path, e);
    }
  }

  /**
   * <p>Writes a ChaosGameDescription object to a file. This method understands the format of the
   * different files of Affine and Julia transformations with its submethods. This method is called when
   * a file needs to be written.</p>
   *
   * @param description {@code ChaosGameDescription} the description to be written to the file.
   * @param path {@code String} the path to the file.
   * @throws FileOperationException if an error occurs while writing to the file.
   */

  @Override
  public void writeToFile(ChaosGameDescription description, String path) throws FileOperationException {
    FileExceptionValidator.validateFilePath(path);
    FileExceptionValidator.validateDescription(description);
    File file = new File(path);
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
      FileExceptionValidator.ensureFileExistValidator(path, file);
      Set<String> transformTypes = extractTransformTypes(description);
      transformTypes.forEach(
              type -> {
                try {
                  writer.write(type + "\n");
                } catch (IOException e) {
                  FileExceptionValidator.logException(e, "An error occurred while writing to the file: " + path);
                }
              });
      writeCoordinates(writer, description);
      writeTransformations(writer, description);
      writer.flush();
    } catch (IOException e) {
      FileExceptionValidator.throwException(e, "An error occurred while writing to the file: ", path);
    } catch (Exception e) {
      FileExceptionValidator.logException(e, "An unexpected error occurred: " + path);
      throw new RuntimeException("An unexpected error occurred while writing to the file: " + path, e);
    }
  }

  /**
   * <p>Extracts the transform types from a {@link ChaosGameDescription} object. This method is called in
   * {@link #writeToFile} to write the transformation type in the header.</p>
   *
   * @param description {@code ChaosGameDescription} the description to extract the transform types from.
   * @return a {@code Set} of {@code String} representing the transform types.
   */
  private Set<String> extractTransformTypes(ChaosGameDescription description) {
    return description.getTransformationsList().stream()
            .map(transform -> {
              if (transform instanceof AffineTransform2D) {
                return "Affine2D";
              } else if (transform instanceof JuliaTransform) {
                return "Julia2D";
              }
              return null;
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
  }

  /**
   * <p>Writes the coordinates to the file. This method is called in {@link #writeToFile} to write the
   * coordinates to the file.</p>
   *
   * @param writer {@code BufferedWriter} the writer to write to the file.
   * @param description {@code ChaosGameDescription} the description to extract the coordinates from.
   * @throws IOException if an error occurs while writing to the file.
   */

  private void writeCoordinates(BufferedWriter writer, ChaosGameDescription description) throws IOException {
    writer.write(description.getMinCoords().getX0() + "," + description.getMinCoords().getX1() + "\n");
    writer.write(description.getMaxCoords().getX0() + "," + description.getMaxCoords().getX1() + "\n");
  }

  /**
   * <p>Writes the transformations to the file. This method is called in {@link #writeToFile} to write the
   * transformations to the file.</p>
   *
   * @param writer {@code BufferedWriter} the writer to write to the file.
   * @param description {@code ChaosGameDescription} the description to extract the transformations from.
   * @throws FileOperationException if an error occurs while writing to the file.
   */

  private void writeTransformations(BufferedWriter writer, ChaosGameDescription description) throws FileOperationException {
    final boolean[] hasWrittenJulia = {false};
    description.getTransformationsList().forEach(transform -> {
      try {
        if (transform instanceof AffineTransform2D affine) {
          writeAffine(writer, affine);
        } else if (transform instanceof JuliaTransform julia && !hasWrittenJulia[0]) {
          writeJulia(writer, julia, hasWrittenJulia);
        }
      } catch (IOException e) {
        FileExceptionValidator.logException(e, "An error occurred while writing transformations to the file");
      }
    });
  }

  /**
   * <p>Writes the Julia transformation constant to file. The method is called in {@code writeTransformations}</p>
   *
   * @param writer {@code BufferedWriter} the writer to write to the file.
   * @param julia {@code JuliaTransform} the Julia transformation to extract the constant from.
   * @param hasWrittenJulia {@code boolean[]} an array to check if the Julia transformation has been written.
   * @throws IOException if an error occurs while reading the file.
   */

  private static void writeJulia(BufferedWriter writer, JuliaTransform julia, boolean[] hasWrittenJulia) throws IOException {
    Complex c = julia.getComplex();
    writer.write(c.getRealPart() + "," + c.getImaginaryPart() + "\n");
    hasWrittenJulia[0] = true;
  }
  /**
   * <p>Writes the Affine transformation to file. Will write the 2x2 matrix and the vector.
   * The method is called in {@code writeTransformations}</p>
   *
   * @param writer {@code BufferedWriter} the writer to write to the file.
   * @param affine {@code AffineTransform2D} the Affine transformation to extract the matrix and vector from.
   * @throws IOException if an error occurs while reading the file.
   */

  private static void writeAffine(BufferedWriter writer, AffineTransform2D affine) throws IOException {
    Matrix2x2 matrix = affine.getMatrix();
    Vector2D vector = affine.getVector();
    writer.write(matrix.getA00() + "," +
            matrix.getA01() + "," +
            matrix.getA10() + "," +
            matrix.getA11() + "," +
            vector.getX0() + "," +
            vector.getX1() + "\n"
    );
  }
  /**
   * <p>Adds a Julia transformation to the list of transformations. Will check if the parts are of length 2.
   * Then it will extract the complex constant and add the resulting Julia transformation to the list.
   * The method is called in {@code readFromFile}</p>
   *
   * @param parts {@code String[]} the parts of the Julia transformation. Validated to be of length 2.
   * @param transforms {@code List<Transform2D>} the list of transformations to add the Julia transformation to.
   */

  private static void addJuliaTransformation(String[] parts, List<Transform2D> transforms) {
    FileExceptionValidator.validateArrayLength(parts, 2, "Invalid Julia transformation data");
    Complex c = new Complex(
            Double.parseDouble(parts[0].trim()),
            Double.parseDouble(parts[1].trim())
    );
    transforms.add(new JuliaTransform(c, 1));
    transforms.add(new JuliaTransform(c, -1));
  }

  /**
   * <p>Adds an Affine transformation to the list of transformations. Will check if the parts are of length 6.
   * Then it will extract the matrix and vector and add the resulting Affine transformation to the list.
   * The method is called in {@code readFromFile}</p>
   *
   * @param parts {@code String[]} the parts of the Affine transformation. Validated to be of length 6.
   * @param transforms {@code List<Transform2D>} the list of transformations to add the Affine transformation to.
   */

  private static void addAffineTransformation(String[] parts, List<Transform2D> transforms) {
    FileExceptionValidator.validateArrayLength(parts, 6, "Invalid Affine transformation data");
    Matrix2x2 matrix = new Matrix2x2(
            Double.parseDouble(parts[0].trim()), Double.parseDouble(parts[1].trim()),
            Double.parseDouble(parts[2].trim()), Double.parseDouble(parts[3].trim())
    );
    Vector2D vector = new Vector2D(
            Double.parseDouble(parts[4].trim()),
            Double.parseDouble(parts[5].trim())
    );
    transforms.add(new AffineTransform2D(matrix, vector));
  }

  /**
   * <p>Reads the minimum and maximum coordinates from the file. Will read the next two lines and split them
   * by comma. The resulting values will be used to create a new {@link Vector2D} object.
   * The method is called in {@code readFromFile}</p>
   *
   * @param scanner {@code Scanner} the scanner to read the lines from.
   * @return a new {@code Vector2D} object.
   */

  private static Vector2D readMinMaxCoords(Scanner scanner) {
    FileExceptionValidator.validateHasNextLine(scanner);
    String[] lowerLeft = scanner.nextLine().split("#")[0].split(",");
    FileExceptionValidator.validateArrayLength(lowerLeft, 2, "Invalid coordinate data");
    return new Vector2D(
            Double.parseDouble(lowerLeft[0].trim()),
            Double.parseDouble(lowerLeft[1].trim())
    );
  }

  /**
   * <p>Identifies the transformation type from the file. Will read the first line that is not a comment or empty.
   * The method is called in {@code readFromFile}</p>
   *
   * @param scanner {@code Scanner} the scanner to read the lines from.
   * @return a {@code String} representing the transformation type.
   */

  private static String identifyTransformation(Scanner scanner) {
    return Stream.generate(scanner::nextLine)
            .map(String::trim)
            .filter(line -> !line.startsWith("#") && !line.isEmpty())
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Invalid or missing transformation type"));
  }
}
