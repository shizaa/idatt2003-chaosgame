package edu.ntnu.stud.model.controls;

import edu.ntnu.stud.model.interfaces.KeyboardObserver;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import edu.ntnu.stud.model.validators.ArgumentValidator;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>The class {@code KeyboardsControls} is responsible for handling keyboard events.
 * The class implements the {@link EventHandler} interface. The class is used in
 * {@link edu.ntnu.stud.controller.fractalactioncontroller.ActionController} to handle keyboard events.</p>

 * Responsibility: Handle keyboard events
 *
 * @see edu.ntnu.stud.controller.fractalactioncontroller.ActionController
 * @see edu.ntnu.stud.model.interfaces.KeyboardObserver
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @see javafx.event.EventHandler
 * @see javafx.scene.input.KeyCode
 * @see javafx.scene.input.KeyEvent
 * @see java.util.ArrayList
 * @see java.util.List
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class KeyboardsControls implements EventHandler<KeyEvent> {
  private final List<KeyboardObserver> observers = new ArrayList<>();

  /**
   * <p>Method for adding an observer to the list of observers. Is initialized in Appinitializer.</p>
   *
   * @param observer the observer to be added
   * @throws IllegalArgumentException if the observer is null.
   */

  public void addObserver(KeyboardObserver observer) {
    ArgumentValidator.observerNotNullValidator(observer);
    observers.add(observer);
  }

  /**
   * <p>Method for removing an observer from the list of observers.</p>
   *
   * @param observer the observer to be removed
   * @throws IllegalArgumentException if the observer is null.
   */
  public void removeObserver(KeyboardObserver observer) {
    ArgumentValidator.observerNotNullValidator(observer);
    observers.remove(observer);
  }

  /**
   * <p>Method for handling keyboard events. Overrides the handle method in the
   * EventHandler interface. The method notifies the observers when a key is pressed.</p>
   *
   * @param e the key event
   */
  @Override
  public void handle(KeyEvent e) {
    if (e.getEventType().getName().equals("KEY_PRESSED")) {
      for (KeyboardObserver observer : observers) {
        observer.onKeyPressed(e);
      }
    }
  }
}

