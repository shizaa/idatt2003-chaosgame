package edu.ntnu.stud.model.exception;

/**
 * <h3>Invalid Format Exception</h3>
 * <p>The class, {@code InvalidFormatException}, is a custom exception that extends {@link java.lang.IllegalArgumentException}.
 * It is used to handle exceptions that occur when an invalid format is encountered.</p>
 *
 * @see java.lang.IllegalArgumentException
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */


public class InvalidFormatException extends IllegalArgumentException {
  /**
   * Constructs a new {@code InvalidFormatException} with the specified detail message.
   *
   * @param message the detail message
   */
  public InvalidFormatException(String message) {
    super(message);
  }
}