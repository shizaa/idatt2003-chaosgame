package edu.ntnu.stud.model.exception;

/**
 * <h3>Invalid Range Exception</h3>
 * <p>The class, {@code InvalidRangeException}, is a custom exception that extends {@link java.lang.IllegalArgumentException}.
 * It is used to handle exceptions that occur when a value is out of the expected range.</p>
 *
 * @see java.lang.IllegalArgumentException
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class InvalidRangeException extends IllegalArgumentException {

  /**
   * Constructs a new {@code InvalidRangeException} with the specified detail message.
   *
   * @param message the detail message
   */
  public InvalidRangeException(String message) {
    super(message);
  }
}