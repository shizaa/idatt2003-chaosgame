package edu.ntnu.stud.model.exception;

import java.io.IOException;

/**
 * <h3>File Operation Exception</h3>
 * <p>The class, {@code FileOperationException}, is a custom exception
 * that extends {@link java.io.IOException}.
 * It is used to handle exceptions that occur during file operations.</p>
 *
 * @see java.io.IOException
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class FileOperationException extends IOException {

  /**
     * Constructs a new {@code FileOperationException} with the specified detail message.
     *
     *  @param message the detail message
     */

  public FileOperationException(String message) {
    super(message);
  }

  /**
  *  Constructs a new {@code FileOperationException} with the specified detail message and cause.
  *
  * @param message the detail message
  * @param cause the cause of the exception
  */

  public FileOperationException(String message, Throwable cause) {
    super(message, cause);
  }
}
