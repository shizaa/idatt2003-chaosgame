package edu.ntnu.stud.model.exception;

/**
 * <h3>Null Argument Exception</h3>
 * <p>The class, {@code NullArgumentException}, is a custom exception that extends {@link java.lang.IllegalArgumentException}.
 * It is used to handle exceptions that occur when a null argument is encountered where it is not allowed.</p>
 *
 * @see java.lang.IllegalArgumentException
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class NullArgumentException extends IllegalArgumentException {

  /**
   * Constructs a new {@code NullArgumentException} with the specified detail message.
   *
   * @param message the detail message
   */
  public NullArgumentException(String message) {
    super(message);
  }
}