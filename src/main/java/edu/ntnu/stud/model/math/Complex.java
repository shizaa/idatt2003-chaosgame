package edu.ntnu.stud.model.math;

import edu.ntnu.stud.model.validators.ArgumentValidator;


/**
 * <h3>Complex</h3>
 * The class {@code Complex} represents a complex number with a real and an imaginary part.
 * Responsibility: Represent a complex number.
 *
 * @see edu.ntnu.stud.model.math.Vector2D
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class Complex extends Vector2D {

  /**
   * The constructor initializes the complex number with the given real and imaginary parts.
   *
   * @param realPart the real part
   * @param imaginaryPart the imaginary part
   * @throws IllegalArgumentException if the values are not within the valid range
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
    ArgumentValidator.validateRealPartValue(realPart);
    ArgumentValidator.validateImaginaryPartValue(imaginaryPart);
  }


  /**
   * Method for getting the real part of the complex number.
   *
   * @return double The real part of the complex number.
   */
  public double getRealPart() {
    return getX0();
  }

  /**
   * Method for getting the imaginary part of the complex number.
   *
   * @return double The imaginary part of the complex number.
   */
  public double getImaginaryPart() {
    return getX1();
  }

  /**
   * <p>Calculates the principal square root of the complex number. The principal square root is the
   * one with the non-negative real part. If both square roots have a real part of zero, the one
   * with positive imaginary part is considered principal.</p>
   *
   * <p>The principal square root of the complex number. This is a new Complex object
   * whose real part is the square root of the magnitude (modulus) of the original complex
   * number multiplied by the cosine of half the angle, and whose imaginary part is the square
   * root of the magnitude of the original complex number multiplied by the sine of half the
   * angle.</p>
   *
   * @return Complex - The principal square root of the complex number.
   */
  public Complex sqrt() {
    double magnitude = Math.sqrt(Math.sqrt(Math.pow(getX0(), 2) + Math.pow(getX1(), 2)));
    double angle = Math.atan2(getX1(), getX0()) / 2.0;

    double real = magnitude * Math.cos(angle);
    double imag = magnitude * Math.sin(angle);
    return new Complex(real, imag);
  }

  /**
   * Method for subtracting two complex numbers.
   *
   * @param otherComplexNum the other complex number to add to this complex number
   * @return {@code Complex} The sum of the two complex numbers.
   * @throws IllegalArgumentException if the other complex number is null
   */
  public Complex subtract(Complex otherComplexNum) {
    ArgumentValidator.complexNumberNotNullValidator(otherComplexNum);
    double real = getX0() - otherComplexNum.getX0();
    double imag = getX1() - otherComplexNum.getX1();
    ArgumentValidator.validateRealPartValue(real);
    ArgumentValidator.validateImaginaryPartValue(imag);
    return new Complex(real, imag);
  }
}
