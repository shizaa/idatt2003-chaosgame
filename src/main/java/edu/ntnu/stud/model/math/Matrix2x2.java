package edu.ntnu.stud.model.math;

import edu.ntnu.stud.model.validators.ArgumentValidator;

/**
 * <h3>Matrix2x2</h3>
 * <p>The class Matrix2x2 represents a 2x2 matrix with four values. The class contains a method for
 * multiplying the matrix with a vector.</p>

 * Responsibility: Represent a 2x2 matrix and multiply it with a vector.
 *
 * @see edu.ntnu.stud.model.math.Vector2D
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @see edu.ntnu.stud.model.transformation.AffineTransform2D
 * @author Shiza Ahmad
 * @author Matteus Mitei
 * @version 1.0.0
 * @since 0.1.0
 */

public class Matrix2x2 {
  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;

  /**
   * Constructor for the {@code Matrix2x2} class.
   *
   * @param a00 value of first row and first column.
   * @param a01 value of first row and second column.
   * @param a10 value of second row and first column.
   * @param a11 value of second row and second column.
   * @throws IllegalArgumentException if any of the values are NaN.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    ArgumentValidator.matrix2x2Validators(Double.isNaN(a00)
            || Double.isNaN(a01) || Double.isNaN(a10) || Double.isNaN(a11), "NaN is not allowed");
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Method for multiplying the matrix with a vector.
   *
   * @param vector the vector to be multiplied with the matrix.
   * @return Vector2D The resulting vector.
   * @throws IllegalArgumentException if the vector is null.
   */
  public Vector2D multiply(Vector2D vector) {
    ArgumentValidator.matrix2x2Validators(vector == null, "Null Vector2D is not allowed");
    double x0 = a00 * vector.getX0() + a01 * vector.getX1();
    double x1 = a10 * vector.getX0() + a11 * vector.getX1();
    return new Vector2D(x0, x1);
  }

  /**
   * Method for getting the value of the first row and first column.
   *
   * @return double The value of the first row and first column.
   */
  public double getA00() {
    return a00;
  }


  /**
   * Method for getting the value of the first row and second column.
   *
   * @return double The value of the first row and second column.
   */
  public double getA01() {
    return a01;
  }

  /**
   * Method for getting the value of the second row and first column.
   *
   * @return double The value of the second row and first column.
   */
  public double getA10() {
    return a10;
  }

  /**
   * Method for getting the value of the second row and second column.
   *
   * @return double The value of the second row and second column.
   */
  public double getA11() {
    return a11;
  }

  /**
   * Equals method for the {@code Matrix2x2} class.
   * <p>Compares the object with another object. Returns true if the object is the same as the
   * object being compared with.Implemented for testing purposes. </p>
   *
   * @param o the object to compare with.
   * @return boolean true if the object is the same as the object being compared with.
   */

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Matrix2x2 matrix2x2 = (Matrix2x2) o;
    return Double.compare(matrix2x2.a00, a00) == 0 &&
            Double.compare(matrix2x2.a01, a01) == 0 &&
            Double.compare(matrix2x2.a10, a10) == 0 &&
            Double.compare(matrix2x2.a11, a11) == 0;
  }
}
