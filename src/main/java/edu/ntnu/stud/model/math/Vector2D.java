package edu.ntnu.stud.model.math;

import edu.ntnu.stud.model.validators.ArgumentValidator;

/**
 * <h3>Vector2D</h3>
 * <p>The class Vector2D represents a 2D vector with x and y components. The class contains
 * a method for adding two vectors.</p>

 * Responsibility: Represent a 2D vector and add two vectors.
 *
 * @see edu.ntnu.stud.model.math.Matrix2x2
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @see edu.ntnu.stud.model.transformation.AffineTransform2D
 * @see edu.ntnu.stud.model.transformation.JuliaTransform
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class Vector2D {
  private final double x0;
  private final double x1;

  /**
   * The constructor initializes the vector with the given x and y components.
   *
   * @param x0 the x 0
   * @param x1 the x 1
   */
  public Vector2D(double x0, double x1) {
    ArgumentValidator.vector2DConstructorValidator(x0, x1);
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Gets the x0 component.
   *
   * @return the x 0
   */
  public double getX0() {
    return x0;
  }

  /**
   * Gets the x1 component.
   *
   * @return the x 1
   */
  public double getX1() {
    return x1;
  }

  /**
   * Method that adds two vectors and returns the result.
   *
   * @param otherVector the other vector to add to this vector
   * @return the vector 2 d
   * @throws IllegalArgumentException if the vector is null.
   */
  public Vector2D add(Vector2D otherVector) {
    ArgumentValidator.vector2DAddValidator(otherVector);
    return new Vector2D(x0 + otherVector.x0, x1 + otherVector.x1);
  }
}
