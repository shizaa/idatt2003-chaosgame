package edu.ntnu.stud.model.chaosgame;

import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.model.exception.InvalidFormatException;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.view.panels.PanelCreator;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <h3>Chaos Game Fractal Factory</h3>
 * Factory class for creating ChaosGameDescription objects.
 *
 * @see ChaosGameDescription
 * @see AffineTransform2D
 * @see JuliaTransform
 * @see Transform2D
 * @see Vector2D
 * @see Matrix2x2
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class ChaosGameFractalFactory {
  private static final String TEXT_FILES_DIR = "src/main/resources/textFiles/";

  /**
   * <p>Creates an empty affine description with the specified number of transformations.
   * The method is called in {@link ChaosGameFractalFactory#createDescription(String, TextField)}
   * to create an empty affine description with the specified number of transformations.</p>
   *
   * @param numTransformations The number of transformations to include in the description.
   * @return An empty affine description with the specified number of transformations.
   */
  public ChaosGameDescription createEmptyAffineDescription(int numTransformations) {
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    List<Transform2D> transformationList = IntStream.range(0, numTransformations)
            .mapToObj(i -> new AffineTransform2D(new Matrix2x2(0.0, 0.0, 0.0, 0.0), new Vector2D(0.0, 0.0)))
            .collect(Collectors.toList());
    return new ChaosGameDescription(minCoords, maxCoords, transformationList);
  }

  /**
   * <p>Creates an empty Julia description with a single transformation.
   * The method is called in {@link ChaosGameFractalFactory#createDescription(String, TextField)}
   * to create an empty Julia description.</p>
   *
   * @return An empty Julia {@link ChaosGameDescription} with a single transformation.
   */
  public ChaosGameDescription createEmptyJuliaDescription() {
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new JuliaTransform(new Complex(0.0, 0.0), 1));
    return new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  /**
   * <p>Creates an empty fractal description based on the selected fractal type.
   * The method is called in {@link ChaosGameFractalFactory#createDescription(String, TextField)}
   * to create an empty fractal description based on the selected fractal type.</p>
   *
   * @param createEmptyFractalCombo The {@code ComboBox} containing the selected fractal type.
   * @param nameNumField The {@code HBox} containing the TextField for the number of transformations.
   * @param numTransformationsField The {@code TextField} for the number of transformations.
   */
  public void handleFractalComboBoxAction(ComboBox<String> createEmptyFractalCombo, HBox nameNumField, TextField numTransformationsField) {
    String selectedType = createEmptyFractalCombo.getValue();
    if ("Affine".equals(selectedType)) {
      numTransformationsField.setVisible(true);
      if (!nameNumField.getChildren().contains(numTransformationsField)) {
        nameNumField.getChildren().add(numTransformationsField);
      }
    } else {
      numTransformationsField.setVisible(false);
      nameNumField.getChildren().remove(numTransformationsField);
    }
  }

  /**
   * <p>Creates a description based on the selected fractal type and the number of transformations.
   * The method is called in {@link ActionController} to create a description based on the selected
   * fractal type and the number of transformations.</p>
   *
   * @param selectedFractalType The selected fractal type.
   * @param numTransformationsField The {@code TextField} for the number of transformations.
   * @return A {@link ChaosGameDescription} based on the selected fractal type and the number of transformations.
   * @throws InvalidFormatException If the number of transformations is not a valid integer.
   */

  public ChaosGameDescription createDescription(String selectedFractalType, TextField numTransformationsField) throws InvalidFormatException {
    if ("Affine".equalsIgnoreCase(selectedFractalType)) {
      try {
        int numTransformations = Integer.parseInt(numTransformationsField.getText());
        return createEmptyAffineDescription(numTransformations);
      } catch (NumberFormatException e) {
        throw new InvalidFormatException("Number of transformations must be a valid integer.");
      }
    } else if ("Julia".equalsIgnoreCase(selectedFractalType)) {
      return createEmptyJuliaDescription();
    } else {
      throw new InvalidFormatException("Invalid fractal type selected.");
    }
  }

  /**
   * <p>Gets the files in the text files directory and adds them to the ComboBox.
   * The method is called by the {@link PanelCreator} to add the files.</p>
   *
   * @param fileComboBox The {@code ComboBox} to add the files to.
   * @return The files in the directory.
   */

  public File populateFileComboBox(ComboBox<String> fileComboBox) {
    List<String> files = listFilesInDirectory(TEXT_FILES_DIR, name -> name.endsWith(".txt"));
    files.forEach(fileComboBox.getItems()::add);
    return new File(TEXT_FILES_DIR);
  }

  /**
   * <p>Lists the files in the specified directory based on the filter.
   * The method is called in {@link ChaosGameFractalFactory#populateFileComboBox(ComboBox)} to list the files
   * in the specified directory based on the filter.</p>
   *
   * @param directory The directory to list the files from.
   * @param filter The filter to apply to the files.
   * @return The {@code List} of files in the directory based on the filter.
   */
  public List<String> listFilesInDirectory(String directory, Predicate<String> filter) {
    File folder = new File(directory);
    return Arrays.stream(Optional.ofNullable(folder.listFiles()).orElse(new File[0]))
            .filter(File::isFile)
            .map(File::getName)
            .filter(filter)
            .collect(Collectors.toList());
  }
}