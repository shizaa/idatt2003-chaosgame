package edu.ntnu.stud.model.chaosgame;

import edu.ntnu.stud.model.exception.FileOperationException;
import edu.ntnu.stud.model.interfaces.FileHandler;
import edu.ntnu.stud.model.interfaces.FileFormat;
import edu.ntnu.stud.model.format.TextFileFormat;
import edu.ntnu.stud.model.validators.FileExceptionValidator;
import javafx.scene.control.TextArea;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * <h3>Chaos Game File Handler</h3>
 * <p>The class, {@code ChaosGameFileHandler}, implements the {@link edu.ntnu.stud.model.interfaces.FileHandler} interface.
 * It handles file operations such as reading from and writing to files, specifically for {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} objects.
 * The default file format is set to {@link edu.ntnu.stud.model.format.TextFileFormat}, but a different format can be specified.</p>
 *
 * @see edu.ntnu.stud.model.interfaces.FileHandler
 * @see edu.ntnu.stud.model.interfaces.FileFormat
 * @see edu.ntnu.stud.model.format.TextFileFormat
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see edu.ntnu.stud.model.validators.FileExceptionValidator
 * @see javafx.scene.control.TextArea
 * @version 1.0.0
 * @since 0.1.0
 */

public class ChaosGameFileHandler implements FileHandler {

  private final FileFormat fileFormat;

  /**
   * Constructs a new {@code ChaosGameFileHandler} with the default file format {@link edu.ntnu.stud.model.format.TextFileFormat}.
   */
  public ChaosGameFileHandler() {
    this.fileFormat = new TextFileFormat();
  }


  /**
   * Reads a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} from a file.
   *
   * @param path the path to the file to read from
   * @return the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} read from the file
   * @throws FileNotFoundException if the file is not found
   * @throws FileOperationException if an error occurs during the file operation
   */

  @Override
  public ChaosGameDescription readFromFile(String path) throws FileNotFoundException, FileOperationException {
    return fileFormat.readFromFile(path);
  }

  /**
   * Writes a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to a file.
   *
   * @param description the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to write to the file
   * @param path the path to the file to write to
   * @throws FileOperationException if an error occurs during the file operation
   */

  @Override
  public void writeToFile(ChaosGameDescription description, String path) throws FileOperationException {
    fileFormat.writeToFile(description, path);
  }

  /**
   * Validates the given file and loads a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription}.
   *
   * @param file the file to validate and load
   * @param feedbackTextArea the {@link javafx.scene.control.TextArea} to provide feedback
   * @return the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} loaded from the file
   * @throws FileNotFoundException if the file is not found or cannot be read
   * @throws FileOperationException if an error occurs during the file operation
   */

  @Override
  public ChaosGameDescription validateAndLoadFile(File file, TextArea feedbackTextArea) throws FileNotFoundException, FileOperationException {
    if (file.exists() && !file.isDirectory()) {
      if (!file.canRead()) {
        throw new FileNotFoundException("File not found: " + file.getName());
      }
      FileExceptionValidator.validateFileExists(file);
      return readFromFile(file.getAbsolutePath());
    } else {
      throw new FileNotFoundException("File does not exist: " + file.getName());
    }
  }
}
