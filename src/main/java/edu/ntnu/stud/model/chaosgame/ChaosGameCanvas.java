package edu.ntnu.stud.model.chaosgame;

import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.validators.ArgumentValidator;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import edu.ntnu.stud.model.math.Matrix2x2;

/**
 * <h3>Chaos Game Canvas</h3>
 * <p>Represents a {@code ChaosGameCanvas}, which is a canvas that can be drawn on by a {@link ChaosGame}.
 * The canvas is represented as a 2D array of integers, where 1 represents a pixel and 0 represents no pixel.
 * The canvas can be transformed to an image, where the pixels are colored according to a color scheme.</p>
 * <p>
 * Responsibility: Represent a canvas that can be drawn on by a {@link ChaosGame}.
 *
 * @author Shiza Ahmad
 * @version 1.0.0
 * @see ChaosGame
 * @see ChaosGameDescription
 * @see Vector2D
 * @see AffineTransform2D
 * @see Image
 * @see PixelWriter
 * @see WritableImage
 * @see Color
 * @see Matrix2x2
 * @see ArgumentValidator
 * @see IllegalArgumentException
 * @since 0.1.0
 */

public class ChaosGameCanvas {

  /**
   * Counter for the color scheme of the fractal.
   */
  public static int colorCounter = 0;

  private final int width;
  private final int height;
  private final int[][] canvas;
  private final Vector2D minCoords;
  private final Vector2D maxCoords;
  private final AffineTransform2D transformCoordsToIndices;

  /**
   * <p> Constructs a {@code ChaosGameCanvas} with a given width, height, and coordinate bounds.
   * The parameters are final and cannot be changed after construction because they are required for the {@code ChaosGameCanvas} to function. </p>
   * Utilizes the {@link ArgumentValidator} to validate the dimensions and coordinates of the canvas.
   *
   * @param width     The width of the canvas.
   * @param height    The height of the canvas.
   * @param minCoords The minimum coordinates of the canvas.
   * @param maxCoords The maximum coordinates of the canvas.
   * @throws IllegalArgumentException if the dimensions or coordinates are invalid.
   */


  public ChaosGameCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    ArgumentValidator.validateDimensionsCanvas(width, height);
    ArgumentValidator.validateCoordinatesCanvas(minCoords, maxCoords);
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[width][height];
    this.transformCoordsToIndices = transformCoordsToIndices();
  }

  /**
   * <p>Draws a pixel at a given point on the canvas.
   * The point is transformed to the canvas indices before drawing the pixel. </p>
   *
   * @param point The point to draw a pixel at.
   */

  public void putPixel(Vector2D point) {
    ArgumentValidator.validatePoint(point);
    Vector2D transformedPoint = transformCoordsToIndices.transform(point);
    int i = (int) transformedPoint.getX0();
    int j = (int) transformedPoint.getX1();

    if (i >= 0 && i < height && j >= 0 && j < width) {
      canvas[i][j] = 1;
    }
  }

  /**
   * <p> The transformation is an {@code AffineTransform2D} that scales and translates the coordinates to the indices.
   * This is done by calculating the scaling factors and the translation vector.
   * The transformation is represented by a {@code matrix} and a {@code vector}. </p>
   *
   * @return The {@code AffineTransform2D} with matrix and vector.
   */

  private AffineTransform2D transformCoordsToIndices() {
    ArgumentValidator.validateCoordinateDifferencesCanvas(minCoords, maxCoords);
    Matrix2x2 matrix = new Matrix2x2(
            (width - 1) / ((maxCoords.getX0() - minCoords.getX0())),
            0,
            0,
            (height - 1) / ((minCoords.getX1() - maxCoords.getX1()))
    );
    Vector2D vector = new Vector2D(
            (width - 1) * minCoords.getX0() / (minCoords.getX0() - maxCoords.getX0()),
            (height - 1) * maxCoords.getX1() / (maxCoords.getX1() - minCoords.getX1())
    );

    return new AffineTransform2D(matrix, vector);
  }

  /**
   * <p> Returns the color of a pixel at a given position on the canvas.
   * The color is determined by the {@code pixelValue}, {@code colorCounter}, and the position {@code x} and {@code y} of the pixel.
   * Method is used to give fractals a gradient display if wanted. </p>
   *
   * @param pixelValue   The value of the pixel.
   * @param colorCounter The counter for the color scheme.
   * @param x            The x-coordinate of the pixel.
   * @param y            The y-coordinate of the pixel.
   * @return The color of the pixel.
   */

  public Color getColor(int pixelValue, int colorCounter, int x, int y) {
    double ratio = (double) y / (double) height;
    Color baseColor = switch (colorCounter % 10) {
      case 1 -> Color.GREEN;
      case 2 -> Color.BLUE;
      case 3 -> Color.RED;
      case 4 -> Color.YELLOW;
      case 5 -> Color.PURPLE;
      case 6 -> Color.ORANGE;
      default -> Color.GREY;
    };
    if (pixelValue == 1) {
      return Color.color(1 - (1 - baseColor.getRed()) * ratio, 1 -
              (1 - baseColor.getGreen()) * ratio, 1 - (1 - baseColor.getBlue()) * ratio);
    } else {
      return Color.BLACK;
    }
  }

  /**
   * <p> Generates an image of the fractal on the canvas.
   * The image is generated by iterating over the canvas and coloring the pixels according to the color scheme.
   * Method generates a {@code WritableImage} and uses a {@code PixelWriter} to write the pixels to the image. </p>
   *
   * @return The image of the fractal.
   */

  public Image generateFractalImage() {
    WritableImage fractalImage = new WritableImage(width, height);
    PixelWriter pixelWriter = fractalImage.getPixelWriter();

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Color color = getColor(canvas[x][y], colorCounter, x, y);
        pixelWriter.setColor(x, y, color);
      }
    }

    return fractalImage;
  }
}
