package edu.ntnu.stud.model.chaosgame;

import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.model.exception.FileOperationException;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.validators.ArgumentValidator;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>A factory for creating {@link ChaosGameDescription} objects. The class can create
 * a few predefined fractals. The methods are mostly called in {@link ActionController}.</p>

 * Responsibility: Create ChaosGameDescription objects for different fractals.
 *
 * @see ChaosGameDescription
 * @see ChaosGameFileHandler
 * @see ArgumentValidator
 * @see Transform2D
 * @see Vector2D
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class ChaosGameDescriptionFactory {

  public static ChaosGameFileHandler fileHandler = new ChaosGameFileHandler();

  /**
   * <p>Method for choosing the fractal to be generated. Uses submethods to create the fractal
   * that is determined.</p>
   *
   * @param fractalType {@code String} the type of fractal to be generated.
   * @return a new {@code ChaosGameDescription} object for the chosen fractal.
   * @throws FileNotFoundException if the file is not found.
   */
  public static ChaosGameDescription createFractal(String fractalType) throws FileNotFoundException {
    ArgumentValidator.createFractalFractalTypeValidator(fractalType);

    return switch (fractalType) {
      case "Julia set 1" -> createJulia();
      case "Julia set 2" -> createJuliaTwo();
      case "Julia set 3" -> createJuliaThree();
      case "Barnsley leaf" -> createBarnsleyFern();
      case "Sierpinski triangle" -> createSierpinskiTriangle();
      case "Mandelbrot" -> createMandelbrot();
      default -> throw new FileNotFoundException("Invalid fractal type: " + fractalType);
    };
  }

  /**
   * Create a new ChaosGameDescription object for the Mandelbrot fractal.
   *
   * @return a new ChaosGameDescription object for the Mandelbrot fractal.
   */
  public static ChaosGameDescription createMandelbrot() {
    Vector2D minCoords = new Vector2D(-2, -2);
    Vector2D maxCoords = new Vector2D(2, 2);
    List<Transform2D> transforms = new ArrayList<>();
    ArgumentValidator.validateTransforms(transforms);
    return new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  /**
   * Create a new ChaosGameDescription object for the Sierpinski triangle fractal.
   *
   * @return a new ChaosGameDescription object for the Sierpinski triangle fractal.
   * @throws FileNotFoundException if the file is not found.
   */
  private static ChaosGameDescription createSierpinskiTriangle() throws FileNotFoundException {
    return readFractalFile("src/main/resources/textFiles/sierpinski.txt");
  }

  /**
   * Create a new ChaosGameDescription object for the Barnsley fern fractal.
   *
   * @return a new ChaosGameDescription object for the Barnsley fern fractal.
   * @throws FileNotFoundException if the file is not found.
   */
  private static ChaosGameDescription createBarnsleyFern() throws FileNotFoundException {
    return readFractalFile("src/main/resources/textFiles/barnsley.txt");
  }

  /**
   * Method that generates a julia fractal from a given point.
   *
   * @return a new ChaosGameDescription object for the Julia fractal.
   * @throws FileNotFoundException if the file is not found.
   */
  private static ChaosGameDescription createJulia() throws FileNotFoundException {
    return readFractalFile("src/main/resources/textFiles/julia_one.txt");
  }

  /**
   * Method that generates a julia fractal from a given point.
   *
   * @return a new ChaosGameDescription object for the Julia fractal.
   * @throws FileNotFoundException if the file is not found.
   */
  private static ChaosGameDescription createJuliaTwo() throws FileNotFoundException {
    return readFractalFile("src/main/resources/textFiles/julia_two.txt");
  }

  /**
   * Method that generates a julia fractal from a given point.
   *
   * @return a new ChaosGameDescription object for the Julia fractal.
   * @throws FileNotFoundException if the file is not found.
   */
  private static ChaosGameDescription createJuliaThree() throws FileNotFoundException {
    return readFractalFile("src/main/resources/textFiles/julia_three.txt");
  }

  /**
   * Reads a fractal file and returns the ChaosGameDescription.
   *
   * @param filePath the path to the fractal file.
   * @return a new ChaosGameDescription object.
   * @throws FileNotFoundException if the file is not found.
   */
  private static ChaosGameDescription readFractalFile(String filePath) throws FileNotFoundException {
    File file = new File(filePath);
    ArgumentValidator.readFractalFileValidator(filePath, file);
    try {
      return fileHandler.readFromFile(filePath);
    } catch (FileOperationException e) {
      throw new RuntimeException("An unexpected error occurred while reading the file: " + filePath, e);
    }
  }
}