package edu.ntnu.stud.model.validators;

import java.io.File;
import java.io.FileNotFoundException;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.exception.FileOperationException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.TextArea;
import java.util.Scanner;

/**
 * <h3>File Exception Validator</h3>
 * <p>Class for validating file-related operations and handling exceptions.
 * It ensures that file operations are performed safely and logs any exceptions that occur.</p>

 * Responsibility: Validate file operations and log exceptions.
 *
 * @author shizaahmad
 * @version v1.0.0
 * @see FileOperationException
 * @see FileNotFoundException
 * @see IOException
 * @see ChaosGameDescription
 * @since v0.1.0
 */
public class FileExceptionValidator {
  private static final Logger LOGGER = Logger.getLogger(FileExceptionValidator.class.getName());
  private static final String FILE_NOT_FOUND = "File not found: ";

  /**
   * Validates if the file exists and is not a directory.
   *
   * @param file the file to validate
   * @throws FileNotFoundException if the file does not exist or is a directory
   */

  public static void validateFileExists(File file) throws FileNotFoundException {
    if (!file.exists() || file.isDirectory()) {
      LOGGER.log(Level.SEVERE, "File does not exist: " + file.getName());
      throw new FileNotFoundException("File does not exist: " + file.getName());
    }
  }

  /**
   * Logs an exception with a specified message.
   *
   * @param ex      the exception to log
   * @param message the message to log with the exception
   */
  public static void logException(Exception ex, String message) {
    LOGGER.log(Level.SEVERE, message, ex);
  }

  /**
   * Logs an IOException and then throws a FileOperationException.
   *
   * @param e    the IOException to log and rethrow
   * @param x    the error message prefix
   * @param path the file path related to the exception
   * @throws FileOperationException always thrown with the provided message and the original exception
   */
  public static void throwException(IOException e, String x, String path) throws FileOperationException {
    FileExceptionValidator.logException(e, x + path);
    throw new FileOperationException(x + path, e);
  }

  /**
   * Validates that the provided file path is not null or empty.
   *
   * @param path the file path to validate
   * @throws IllegalArgumentException if the path is null or empty
   */

  public static void validateFilePath(String path) {
    if (path == null || path.trim().isEmpty()) {
      throw new IllegalArgumentException("File path cannot be null or empty");
    }
  }

  /**
   * Validates a ChaosGameDescription object.
   *
   * @param description the description to validate
   * @throws IllegalArgumentException if the description or its critical fields are null or empty
   */

  public static void validateDescription(ChaosGameDescription description) {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }
    if (description.getMinCoords() == null || description.getMaxCoords() == null) {
      throw new IllegalArgumentException("Min and max coordinates cannot be null");
    }
    if (description.getTransformationsList() == null || description.getTransformationsList().isEmpty()) {
      throw new IllegalArgumentException("Transforms cannot be null or empty");
    }
  }

  /**
   * Ensures that a file exists, creating it if necessary.
   *
   * @param path the file path to ensure existence for
   * @param file the file object representing the path
   * @throws IOException if the file cannot be created
   */

  public static void ensureFileExistValidator(String path, File file) throws IOException {
    if (!file.exists() && !file.createNewFile()) {
      throw new FileOperationException("Failed to create file: " + path);
    }
  }

  /**
   * Validates the length of an array against an expected length.
   *
   * @param array          the array to validate
   * @param expectedLength the expected length of the array
   * @param errorMessage   the error message to throw if validation fails
   * @throws IllegalArgumentException if the array length is less than the expected length
   */

  public static void validateArrayLength(String[] array, int expectedLength, String errorMessage) {
    if (array.length < expectedLength) {
      throw new IllegalArgumentException(errorMessage);
    }
  }

  /**
   * Validates that a scanner has a next line available.
   *
   * @param scanner the scanner to validate
   * @throws IllegalArgumentException if the scanner does not have a next line
   */

  public static void validateHasNextLine(Scanner scanner) {
    if (!scanner.hasNextLine()) {
      throw new IllegalArgumentException("Missing coordinate data");
    }
  }

  /**
   * Handles a FileNotFoundException by logging it and appending an error message to a TextArea.
   *
   * @param ex               the FileNotFoundException to handle
   * @param feedbackTextArea the TextArea to append the error message to
   * @param file             the file that was not found
   */

  public static void handleFileNotFoundException(FileNotFoundException ex, TextArea feedbackTextArea, File file) {
    FileExceptionValidator.logException(ex, FILE_NOT_FOUND + file.getName());
    feedbackTextArea.appendText(FILE_NOT_FOUND + file.getName() + "\n");
  }
}
