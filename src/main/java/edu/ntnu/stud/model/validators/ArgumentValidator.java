package edu.ntnu.stud.model.validators;

import edu.ntnu.stud.model.chaosgame.ChaosGameCanvas;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.exception.InvalidFormatException;
import edu.ntnu.stud.model.exception.InvalidRangeException;
import edu.ntnu.stud.model.exception.NullArgumentException;
import edu.ntnu.stud.model.interfaces.ChaosGameObserver;
import edu.ntnu.stud.model.interfaces.KeyboardObserver;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Vector2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * <h3>Argument Validator</h3>
 * <p>Class for validating various arguments used across different methods and constructors.
 * It ensures that inputs are valid and throws appropriate exceptions when validation fails.</p>

 * Responsibility: Validate method arguments and constructor parameters.
 *
 * @see InvalidFormatException
 * @see InvalidRangeException
 * @see NullArgumentException
 * @see ChaosGameDescription
 * @see ChaosGameCanvas
 * @see Transform2D
 * @author Shiza Ahmad
 * @version v1.0.0
 * @since v0.1.0
 */
public class ArgumentValidator {

  /**
   * Validates the constructor parameters for Julia Transform.
   *
   * @param point the complex point
   * @param sign the sign value, must be either +1 or -1
   * @throws IllegalArgumentException if the point is null or the sign is invalid
   */

  public static void JuliaTransformConstructorValidator(Complex point, int sign) {
    if (point == null) {
      throw new IllegalArgumentException("Complex point cannot be null");
    }
    if (sign != 1 && sign != -1) {
      throw new IllegalArgumentException("Sign must be either +1 or -1");
    }
  }

  /**
   * Validates that the provided vector is not null.
   *
   * @param point the vector to validate
   * @param Vector_v_cannot_be_null the error message if the vector is null
   * @throws IllegalArgumentException if the vector is null
   */

  public static void transformPointNullValidator(Vector2D point, String Vector_v_cannot_be_null) {
    if (point == null) {
      throw new IllegalArgumentException(Vector_v_cannot_be_null);
    }
  }

  /**
   * Validates the constructor parameters for Mandelbrot set.
   *
   * @param width the width, must be 1000
   * @param height the height, must be 1000
   * @param maxIterations the maximum number of iterations, must be greater than zero and less than or equal to 1000
   * @throws IllegalArgumentException if the width or height are not 1000
   * @throws InvalidRangeException if maxIterations is not within the valid range
   */
  public static void mandelbrotSetConstructorValidator(int width, int height, int maxIterations) {
    if (width != 1000) {
      throw new IllegalArgumentException("Width must be 1000");
    }
    if (height != 1000) {
      throw new IllegalArgumentException("Height must be 1000");
    }
    if (maxIterations <= 0 || maxIterations > 1000) {
      throw new InvalidRangeException("Max iterations must be greater than zero");
    }
  }

  /**
   * Validates the affine transform constructor parameters.
   *
   * @param matrix the matrix flag
   * @param s the error message if validation fails
   * @throws IllegalArgumentException if the matrix flag is true
   */

  public static void affineTransformConstructorValidator(boolean matrix, String s) {
    if (matrix) {
      throw new IllegalArgumentException(s);
    }
  }

  /**
   * Validates the constructor parameters for Vector2D.
   *
   * @param x0 the x0 coordinate
   * @param x1 the x1 coordinate
   * @throws IllegalArgumentException if the coordinates are NaN or outside the valid range
   * @throws InvalidRangeException if the coordinates are outside the valid range
   */
  public static void vector2DConstructorValidator(double x0, double x1) {
    if (Double.isNaN(x0) || Double.isNaN(x1)) {
      throw new IllegalArgumentException("NaN is not allowed");
    }
    if (x0 < -1e6 || x0 > 1e6 || x1 < -1e6 || x1 > 1e6) {
      throw new InvalidRangeException("Coordinates must be within the range -1e5 to 1e5.");
    }
  }

  /**
   * Validates that the provided Vector2D object is not null.
   *
   * @param otherVector the vector to validate
   * @throws IllegalArgumentException if the vector is null
   */

  public static void vector2DAddValidator(Vector2D otherVector) {
    if (otherVector == null) {
      throw new IllegalArgumentException("Null Vector2D is not allowed");
    }
  }

  /**
   * Validates matrix values.
   *
   * @param a00 the matrix value to validate
   * @param NaN_is_not_allowed the error message if the value is NaN
   * @throws IllegalArgumentException if the value is NaN
   */

  public static void matrix2x2Validators(boolean a00, String NaN_is_not_allowed) {
    if (a00) {
      throw new IllegalArgumentException(NaN_is_not_allowed);
    }
  }

  /**
   * Validates the real part value.
   *
   * @param realPart the real part value to validate
   * @throws InvalidRangeException if the real part is not within the valid range
   */

  public static void validateRealPartValue(double realPart) {
    if (realPart < -10 || realPart > 10) {
      throw new InvalidRangeException("Real part must be between -10 and 10");
    }
  }

  /**
   * Validates the imaginary part value.
   *
   * @param imaginaryPart the imaginary part value to validate
   * @throws IllegalArgumentException if the imaginary part is not within the valid range
   */
  public static void validateImaginaryPartValue(double imaginaryPart) {
    if (imaginaryPart < -10 || imaginaryPart > 10) {
      throw new InvalidRangeException("Imaginary part must be between -10 and 10");
    }
  }

  /**
   * Validates that the provided complex number is not null.
   *
   * @param otherComplexNum the complex number to validate
   * @throws IllegalArgumentException if the complex number is null
   */
  public static void complexNumberNotNullValidator(Complex otherComplexNum) {
    if (otherComplexNum == null) {
      throw new IllegalArgumentException("Null Complex number is not allowed.");
    }
  }

  /**
   * Validates that the provided KeyboardObserver is not null.
   *
   * @param observer the observer to validate
   * @throws IllegalArgumentException if the observer is null
   */

  public static void observerNotNullValidator(KeyboardObserver observer) {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
  }

  /**
   * Validates that a file exists and can be read.
   *
   * @param filePath the file path to validate
   * @param file the file to validate
   * @throws FileNotFoundException if the file does not exist or cannot be read
   */

  public static void readFractalFileValidator(String filePath, File file) throws FileNotFoundException {
    if (!file.exists() || !file.canRead()) {
      throw new FileNotFoundException("File not found or cannot be read: " + filePath);
    }
  }

  /**
   * Validates the list of Transform2D objects.
   *
   * @param transforms the list of Transform2D objects to validate
   * @throws IllegalArgumentException if the list or any of its elements are null
   */

  public static void validateTransforms(List<Transform2D> transforms) {
    if (transforms == null) {
      throw new IllegalArgumentException("Transforms list cannot be null");
    }
    for (Transform2D transform : transforms) {
      if (transform == null) {
        throw new IllegalArgumentException("Transform2D object cannot be null");
      }
    }
  }


  /**
   * Validates the fractal type.
   *
   * @param fractalType the fractal type to validate
   * @throws IllegalArgumentException if the fractal type is null or empty
   */

  public static void createFractalFractalTypeValidator(String fractalType) {
    if (fractalType == null || fractalType.trim().isEmpty()) {
      throw new IllegalArgumentException("Fractal type cannot be null or empty");
    }
  }

  /**
   * Validates the file path.
   *
   * @param filePath the file path to validate
   * @throws NullArgumentException if the file path is null or empty
   */
  public static void validateFilePath(String filePath) {
    if (filePath == null || filePath.trim().isEmpty()) {
      throw new NullArgumentException("File path cannot be null or empty");
    }
  }

  /**
   * Validates the directory path.
   *
   * @param textFilesDir the directory path to validate
   * @throws NullArgumentException if the directory path is invalid
   */
  public static void validateDirectoryPathMediator(String textFilesDir) {
    File dir = new File(textFilesDir);
    if (!dir.exists() || !dir.isDirectory()) {
      throw new NullArgumentException("Invalid directory path: " + textFilesDir);
    }
  }

  /**
   * Validates the index within a given size.
   *
   * @param index the index to validate
   * @param size the size of the collection
   * @throws IndexOutOfBoundsException if the index is out of bounds
   */

  public static void validateIndex(int index, int size) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException("Invalid index: " + index);
    }
  }

  /**
   * Validates the values of a 2x2 matrix.
   *
   * @param a the first value
   * @param b the second value
   * @param c the third value
   * @param d the fourth value
   * @throws IllegalArgumentException if any value is NaN or infinite
   */
  public static void validateMatrixValues(double a, double b, double c, double d) {
    if (Double.isNaN(a) || Double.isNaN(b) || Double.isNaN(c) || Double.isNaN(d)
            || Double.isInfinite(a) || Double.isInfinite(b) || Double.isInfinite(c) || Double.isInfinite(d)) {
      throw new IllegalArgumentException("Matrix values must be valid numbers");
    }
  }

  /**
   * Validates the values of a vector.
   *
   * @param x the x-coordinate value
   * @param y the y-coordinate value
   * @throws InvalidFormatException if any value is NaN or infinite
   */

  public static void validateVectorValues(double x, double y) {
    if (Double.isNaN(x) || Double.isNaN(y) || Double.isInfinite(x) || Double.isInfinite(y)) {
      throw new InvalidFormatException("Vector values must be valid numbers");
    }
  }

  /**
   * Validates that the provided coordinate is not null.
   *
   * @param coordinate the coordinate to validate
   * @param message the error message if the coordinate is null
   * @throws NullArgumentException if the coordinate is null
   */

  public static void validateCoordinate(Vector2D coordinate, String message) {
    if (coordinate == null) {
      throw new NullArgumentException(message);
    }
  }

  /**
   * Validates the minimum and maximum coordinates.
   *
   * @param minCoords the minimum coordinates to validate
   * @param maxCoords the maximum coordinates to validate
   * @throws NullArgumentException if either set of coordinates is null
   */

  public static void validateMinMaxCoordinates(Vector2D minCoords, Vector2D maxCoords) {
    if (minCoords == null || maxCoords == null) {
      throw new NullArgumentException("Coordinates cannot be null");
    }
  }

  /**
   * Validates the dimensions of a canvas.
   *
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @throws InvalidRangeException if the width or height are less than or equal to zero
   */
  public static void validateDimensionsCanvas(int width, int height) {
    if (width <= 0 || height <= 0) {
      throw new InvalidRangeException("Width and height must be greater than zero");
    }
  }

  /**
   * Validates the coordinates for a canvas.
   *
   * @param minCoords the minimum coordinates to validate
   * @param maxCoords the maximum coordinates to validate
   * @throws NullArgumentException if either set of coordinates is null
   * @throws IllegalArgumentException if the minimum coordinates are not less than the maximum coordinates
   */

  public static void validateCoordinatesCanvas(Vector2D minCoords, Vector2D maxCoords) {
    if (minCoords == null || maxCoords == null) {
      throw new NullArgumentException("MinCoords and MaxCoords cannot be null");
    }
    if (minCoords.getX0() > maxCoords.getX0() || minCoords.getX1() > maxCoords.getX1()) {
      throw new IllegalArgumentException("MinCoords must be less than MaxCoords");
    }
  }

  /**
   * Validates that the coordinate differences for a canvas are not zero.
   *
   * @param minCoords the minimum coordinates
   * @param maxCoords the maximum coordinates
   * @throws IllegalArgumentException if the differences are zero
   */
  public static void validateCoordinateDifferencesCanvas(Vector2D minCoords, Vector2D maxCoords) {
    if (minCoords.getX0() == maxCoords.getX0() || minCoords.getX1() == maxCoords.getX1()) {
      throw new IllegalArgumentException("Coordinates must have a difference");
    }
  }

  /**
   * Validates the arguments for a chaos game.
   *
   * @param description the chaos game description
   * @param canvas the chaos game canvas
   * @throws IllegalArgumentException if the description or canvas are null
   */
  public static void validateChaosGameArguments(ChaosGameDescription description, ChaosGameCanvas canvas) {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }
    if (canvas == null) {
      throw new IllegalArgumentException("Canvas cannot be null");
    }
  }

  /**
   * Validates the number of steps.
   *
   * @param steps the number of steps
   * @throws InvalidRangeException if the steps are negative
   */

  public static void validateSteps(int steps) {
    if (steps < 0) {
      throw new InvalidRangeException("Number of steps must be non-negative");
    }
  }

  /**
   * Validates that the provided ChaosGameObserver is not null.
   *
   * @param observer the observer to validate
   * @throws IllegalArgumentException if the observer is null
   */
  public static void validateObserver(ChaosGameObserver observer, List<ChaosGameObserver> observers) {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
    if (observers.contains(observer)) {
      throw new IllegalArgumentException("Observer already added");
    }
  }

  /**
   * Validates that a parameter is not null.
   *
   * @param parameterElements the parameter to validate
   * @param s the error message if validation fails
   * @throws IllegalArgumentException if the parameter is null
   */
  public static void validateNonNullParameterController(boolean parameterElements, String s) {
    if (parameterElements) {
      throw new IllegalArgumentException(s);
    }
  }

  /**
   * Validates that the provided point is not null.
   *
   * @param point the point to validate
   * @throws IllegalArgumentException if the point is null
   */
  public static void validatePoint(Vector2D point) {
    if (point == null) {
      throw new IllegalArgumentException("Point cannot be null");
    }
  }

  /**
   * Validates that the list of transformations is not empty.
   *
   * @param transformationsList the list of transformations to validate
   * @throws IllegalArgumentException if the list is empty
   */

  public static void validateTransformationsList(List<Transform2D> transformationsList) {
    if (transformationsList.isEmpty()) {
      throw new IllegalArgumentException("TransformationsList is empty");
    }
  }

  /**
   * Validates that the provided errors list is empty.
   *
   * @param errors the list of errors to validate
   * @throws InvalidFormatException if the list is not empty
   */

  public static void validateErrors(List<String> errors) throws InvalidFormatException {
    if (!errors.isEmpty()) {
      throw new InvalidFormatException(String.join("\n", errors));
    }
  }
}
