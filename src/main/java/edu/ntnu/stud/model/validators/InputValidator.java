package edu.ntnu.stud.model.validators;

import edu.ntnu.stud.model.exception.InvalidFormatException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextField;


/**
 * <h3>InputValidator</h3>
 * <p>Class for validating input fields in the GUI.
 * It is responsible for the validation visible in the GUI.</p>

 * Responsibility: Validate input fields in the GUI.
 *
 * @see InvalidFormatException
 * @author Shiza Ahmad
 * @version v1.0.0
 * @since v0.1.0
 *
 */
public class InputValidator {

  /**
   * Validates the input for a text field.
   *
   * @param input the input string to validate
   * @param errorMessage the error message to display if validation fails
   * @throws InvalidFormatException if the input is null, empty, or only whitespace
   */
  public static void validateTextFieldInput(String input, String errorMessage) {
    if (input == null || input.trim().isEmpty()) {
      throw new InvalidFormatException(errorMessage);
    }
  }

  /**
   * Validates if the input string is a valid number.
   *
   * @param input the input string to validate
   * @param errorMessage the error message to display if validation fails
   * @throws InvalidFormatException if the input is not a valid number
   */
  public static void validateNumberInput(String input, String errorMessage) {
    try {
      Double.parseDouble(input);
    } catch (NumberFormatException e) {
      throw new InvalidFormatException(errorMessage);
    }
  }

  /**
   * Validates the input fields for constant C, checking if they contain valid numbers.
   *
   * @param fields a list of text fields to validate
   * @return a list of error messages if validation fails, otherwise an empty list
   */
  public static List<String> validateConstantcFields(List<TextField> fields) {
    List<String> errors = new ArrayList<>();
    try {
      validateNumberInput(fields.getFirst().getText(), "Invalid real part value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    try {
      validateNumberInput(fields.get(1).getText(), "Invalid imaginary part value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    return errors;
  }

  /**
   * Validates the input fields for coordinates, checking if they contain valid numbers.
   *
   * @param fields a list of text fields to validate
   * @return a list of error messages if validation fails, otherwise an empty list
   */
  public static List<String> validateCoordsFields(List<TextField> fields) {
    List<String> errors = new ArrayList<>();
    try {
      validateNumberInput(fields.getFirst().getText(), "Invalid X0 value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    try {
      validateNumberInput(fields.get(1).getText(), "Invalid X1 value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    return errors;
  }

  /**
   * Validates the input fields for a matrix, checking if they contain valid numbers.
   *
   * @param fields a list of text fields to validate
   * @return a list of error messages if validation fails, otherwise an empty list
   */
  public static List<String> validateMatrixFields(List<TextField> fields) {
    List<String> errors = new ArrayList<>();
    try {
      validateNumberInput(fields.getFirst().getText(), "Invalid A00 value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    try {
      validateNumberInput(fields.get(1).getText(), "Invalid A01 value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    try {
      validateNumberInput(fields.get(2).getText(), "Invalid A10 value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    try {
      validateNumberInput(fields.get(3).getText(), "Invalid A11 value");
    } catch (InvalidFormatException e) {
      errors.add(e.getMessage());
    }
    return errors;
  }

  /**
   * Validates the inputs for fractal configuration, including fractal type, file name,
   * and number of transformations for Affine fractals.
   *
   * @param selectedFractalType the selected fractal type
   * @param newFileName the name of the new file
   * @param numTransformationsField the text field for the number of transformations
   * @throws InvalidFormatException if any validation checks fail
   */

  public static void validateFractalInputs(String selectedFractalType, String newFileName, TextField numTransformationsField) throws InvalidFormatException {
    InputValidator.validateTextFieldInput(selectedFractalType, "No fractal type selected.");
    InputValidator.validateTextFieldInput(newFileName, "No file name entered.");
    if ("Affine".equalsIgnoreCase(selectedFractalType)) {
      InputValidator.validateTextFieldInput(numTransformationsField.getText(), "Number of transformations is required for Affine type.");
    }
  }
}
