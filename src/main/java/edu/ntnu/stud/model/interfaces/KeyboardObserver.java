package edu.ntnu.stud.model.interfaces;

import javafx.scene.input.KeyEvent;

/**
 * <p>The interface {@code KeyboardObserver} is used to notify observers when a key is pressed.
 * The interface is implemented in {@link edu.ntnu.stud.controller.KeyboardController}.</p>
 *
 * @see edu.ntnu.stud.model.controls.KeyboardsControls
 * @see edu.ntnu.stud.controller.KeyboardController
 * @see javafx.scene.input.KeyEvent
 * @see java.util.List
 * @see java.util.ArrayList
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public interface KeyboardObserver {

  /**
   * <p>Method {@code onKeyPressed} is called when a key is pressed. The method is called
   * in {@code edu.ntnu.stud.model.controls.KeyboardsControls} when a key is pressed. The method
   * is implemented in {@code edu.ntnu.stud.controller.KeyboardController}. The parameter is
   * the {@code KeyEvent} object that contains information about the keyboard event that ocurred</p>
   *
   * @param e The {@code KeyEvent} object generated when a key is pressed.
   */

  void onKeyPressed(KeyEvent e);
}
