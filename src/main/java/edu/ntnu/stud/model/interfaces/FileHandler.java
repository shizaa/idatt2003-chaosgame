package edu.ntnu.stud.model.interfaces;

import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.exception.FileOperationException;
import javafx.scene.control.TextArea;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * <h3>File Handler</h3>
 * <p>The interface, {@code FileHandler}, provides methods for reading from, writing to,
 * and validating files. It handles file operations related to
 * {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} and provides feedback through a {@link javafx.scene.control.TextArea}.</p>
 *
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see FileNotFoundException
 * @see FileOperationException
 * @see javafx.scene.control.TextArea
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public interface FileHandler {

  /**
   * Reads a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} from a file.
   *
   * @param path the path to the file to read from
   * @return the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} read from the file
   * @throws FileNotFoundException if the file is not found at the specified path
   * @throws FileOperationException if an error occurs during the file operation
   */

  ChaosGameDescription readFromFile(String path) throws FileNotFoundException, FileOperationException;

  /**
   * Writes a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to a file.
   *
   * @param description the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to write to the file
   * @param path the path to the file to write to
   * @throws FileOperationException if an error occurs during the file operation
   */

  void writeToFile(ChaosGameDescription description, String path) throws FileOperationException;

  /**
   * Validates the given file and loads a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription}.
   *
   * @param file the file to validate and load
   * @param feedbackTextArea the TextArea to provide feedback
   * @return the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} loaded from the file
   * @throws FileNotFoundException if the file is not found
   * @throws FileOperationException if an error occurs during the file operation
   */

  ChaosGameDescription validateAndLoadFile(File file, TextArea feedbackTextArea) throws FileNotFoundException, FileOperationException;
}
