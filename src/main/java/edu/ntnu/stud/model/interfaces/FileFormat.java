package edu.ntnu.stud.model.interfaces;

import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.exception.FileOperationException;
import java.io.FileNotFoundException;

/**
 * <h3>File Format</h3>
 * <p>The interface, {@code FileFormat}, provides methods for reading from and writing to files.
 * It is intended to handle file operations related to {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription}.</p>
 *
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see FileNotFoundException
 * @see FileOperationException
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public interface FileFormat {
  /**
   * Reads a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} from a file.
   *
   * @param path the path to the file to read from
   * @return the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} read from the file
   * @throws FileNotFoundException if the file is not found at the specified path
   * @throws FileOperationException if an error occurs during the file operation
   */
  ChaosGameDescription readFromFile(String path) throws FileNotFoundException, FileOperationException;

  /**
   * Writes a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to a file.
   *
   * @param description the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to write to the file
   * @param path the path to the file to write to
   * @throws FileOperationException if an error occurs during the file operation
   */
  void writeToFile(ChaosGameDescription description, String path) throws FileOperationException;
}
