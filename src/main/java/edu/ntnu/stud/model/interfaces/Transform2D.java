package edu.ntnu.stud.model.interfaces;

import edu.ntnu.stud.model.math.Vector2D;


/**
 * <h3>Transform2D</h3>
 * <p>The interface {@code Transform2D} represents a 2D transformation. In has a single method
 * {@code transform} to transform a point with the transformation as implemented in the classes.</p>

 * Responsibility: Represent a 2D transformation.
 *
 * @see edu.ntnu.stud.model.transformation.AffineTransform2D
 * @see edu.ntnu.stud.model.transformation.JuliaTransform
 * @see edu.ntnu.stud.model.math.Vector2D
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescriptionFactory
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public interface Transform2D {

  /**
   * <p>Method for transforming a point using the transformation. This method is implemented in
   * {@link edu.ntnu.stud.model.transformation.AffineTransform2D} as an affine transformation
   * by multiplying the point with a matrix.
   * The method is also implemented in {@link edu.ntnu.stud.model.transformation.JuliaTransform}
   * as a transformation using a complex number.</p>
   *
   * @param point the point to be transformed
   * @return {@code Vector2D} The resulting point.
   */
  Vector2D transform(Vector2D point);
}
