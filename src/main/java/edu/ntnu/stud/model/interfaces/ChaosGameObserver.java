package edu.ntnu.stud.model.interfaces;

/**
 * <h3>Chaos Game Observer</h3>
 * <p>The class, {@code ChaosGameObserver} is an interface used to update the display
 * when the model changes. The only method in the interface is the {@code ChaosGameObserver#update} method.
 *
 * @see edu.ntnu.stud.view.display.ChaosGameDisplay
 * @see edu.ntnu.stud.model.chaosgame.ChaosGame
 * @see javafx.scene.control.Button
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public interface ChaosGameObserver {

  /**
   * <p>The method, {@code update} is used to update the display when the model changes.
   * It is called by {@link edu.ntnu.stud.model.chaosgame.ChaosGame}, when it notifies
   * the observers of a change in the model. A change in the model is triggered by a button
   * being pressed in the GUI. (and the {@code Button#SetOnAction} method is called in the
   * {@link edu.ntnu.stud.view.display.ChaosGameDisplay}.) {@code update} is implemented in
   * {@link edu.ntnu.stud.view.display.ChaosGameDisplay}.
   *
   * @param action The action that is to be updated.
   */

  void update(String action);
}
