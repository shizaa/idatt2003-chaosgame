package edu.ntnu.stud.model.transformation;

import edu.ntnu.stud.model.chaosgame.ChaosGame;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.validators.ArgumentValidator;

/**
 * <h3>JuliaTransform</h3>
 * <p>The class {@code JuliaTransform} represents a transformation in 2D space. The transformation is a square
 * root transformation of a complex number pluss some constant C. The transformation is used to generate
 * the Julia set, which is a fractal. The class implements the {@link Transform2D} interface.
 * The transform method is implemented in {@link ChaosGame}</p>
 *
 * <p>Mathematical background: The Julia set is the set of numbers under the infinite iteration
 * of the function f(z) = z^2 + C, where C is a specified complex number. The set is a fractal,
 * meaning that it is self-similar in many places.</p>

 * Responsibility: Represent a transformation in 2D space.
 *
 * @see edu.ntnu.stud.model.chaosgame.ChaosGame
 * @see edu.ntnu.stud.model.interfaces.Transform2D
 * @see edu.ntnu.stud.model.math.Complex
 * @see edu.ntnu.stud.model.math.Vector2D
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @author Shiza Ahmad
 * @author Matteus Mitei
 * @version 1.0.0
 * @since 0.1.0
 */
public class  JuliaTransform implements Transform2D {
  private Complex point;
  private final int sign;

  /**
   * Constructor for {@code JuliaTransform}. The constructor takes a complex number and a sign as parameters.
   *
   * @param point the point
   * @param sign the sign
   * @throws IllegalArgumentException if the point is null, or the sign is not -1 or 1.
   */
  public JuliaTransform(Complex point, int sign) {
    ArgumentValidator.JuliaTransformConstructorValidator(point, sign);
    this.point = point;
    this.sign = sign;
  }


  /**
   * <p>Method for transforming a point. Overrides the transform method in the
   * Transform2D interface. The method transforms a point with z_i+1 = z_i^2 + C.</p>
   *
   * @param point the previous point in the Julia set which is validated not to be 0 with {@link ArgumentValidator}
   * @return The next {@code Vector2D}, point in the Julia Set.
   * @throws IllegalArgumentException if the point is null.
   */

  @Override
  public Vector2D transform(Vector2D point) {
    ArgumentValidator.transformPointNullValidator(point, "Vector V cannot be null");
    Complex z = new Complex(point.getX0(), point.getX1());
    Complex subtracted = z.subtract(this.point);
    Complex result = subtracted.sqrt();

    if (this.sign == -1) {
      return new Vector2D(result.getRealPart() * -1, result.getImaginaryPart() * -1);
    }
    return new Vector2D(result.getRealPart(), result.getImaginaryPart());
  }

  /**
   * <p>Method for getting the complex number of the Julia Transform.</p>
   *
   * @return the complex number of the Julia Transform
   */

  public Complex getComplex() {
    return point;
  }

  /**
   * <p>Method for setting the complex number of the Julia Transform.</p>
   *
   * @param complex the complex number of the Julia Transform
   * @throws IllegalArgumentException if the complex number is null.
   */

  public void setComplex(Complex complex) {
    ArgumentValidator.transformPointNullValidator(complex, "Complex cannot be null");
    this.point = complex;
  }

}

