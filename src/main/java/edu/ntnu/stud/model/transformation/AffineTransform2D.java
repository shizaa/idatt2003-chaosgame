package edu.ntnu.stud.model.transformation;

import edu.ntnu.stud.model.chaosgame.ChaosGame;
import edu.ntnu.stud.model.interfaces.Transform2D;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.validators.ArgumentValidator;

/**
 * <h3>AffineTransform2D</h3>
 * <p>The class {@code AffineTransform2D} represents an affine transformation in 2D space.
 * The transformation is a matrix-vector multiplication. The class implements the
 * {@link Transform2D} interface. {@code transform} is called in {@link ChaosGame} to run
 * every transformation.</p>
 *
 * Responsibility: Represent an affine transformation in 2D space.
 *
 * @see edu.ntnu.stud.model.chaosgame.ChaosGame
 * @see edu.ntnu.stud.model.interfaces.Transform2D
 * @see edu.ntnu.stud.model.math.Matrix2x2
 * @see edu.ntnu.stud.model.math.Vector2D
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @author Shiza Ahmad
 * @version 1.0.0
 */

public class AffineTransform2D implements Transform2D {
  private Matrix2x2 matrix;
  private Vector2D vector;

  /**
   * The constructor initializes the affine transformation with the given matrix and vector.
   *
   * @param matrix the matrix
   * @param vector the vector
   * @throws IllegalArgumentException if the matrix or vector is null.
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    ArgumentValidator.affineTransformConstructorValidator(matrix == null || vector == null, "Matrix and Vector cannot be null");
    this.matrix = matrix;
    this.vector = vector;
  }


  /**
   * Method for transforming a point using the affine transformation. Implemented form
   * {@link Transform2D} interface.
   *
   * @param point the point to be transformed. Previous point in the set.
   * @return {@link Vector2D} The resulting point that is next point in set.
   * @throws IllegalArgumentException if the point is null.
   */
  @Override
  public Vector2D transform(Vector2D point) {
    ArgumentValidator.affineTransformConstructorValidator(point == null, "Point cannot be null");
    return matrix.multiply(point).add(vector);
  }

  /**
   * Method for getting the matrix of the affine transformation.
   *
   * @return {@link Matrix2x2} The matrix of the affine transformation.
   */
  public Matrix2x2 getMatrix() {
    return matrix;
    }
  /**
   * Method for getting the vector of the affine transformation.
   *
   * @return {@link Vector2D} The vector of the affine transformation.
   */
  public Vector2D getVector() {
    return vector;
    }

  /**
   * Method for setting the matrix of the affine transformation.
   * @param matrix2x2 the {@link Matrix2x2} of the affine transformation.
   * @throws IllegalArgumentException if the matrix is null.
   */
  public void setMatrix(Matrix2x2 matrix2x2) {
    ArgumentValidator.affineTransformConstructorValidator(matrix2x2 == null, "Matrix cannot be null");
    this.matrix = matrix2x2;
    }
  /**
   * Method for setting the vector of the affine transformation.
   *
   * @param vector2D the {@link Vector2D} of the affine transformation.
   * @throws IllegalArgumentException if the vector is null.
   */
  public void setVector(Vector2D vector2D) {
    ArgumentValidator.affineTransformConstructorValidator(vector2D == null, "Vector cannot be null");
    this.vector = vector2D;
  }


}
