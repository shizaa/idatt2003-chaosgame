package edu.ntnu.stud.model.transformation;
import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.controller.fractalactioncontroller.MandelbrotController;
import edu.ntnu.stud.utils.ColorUtils;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import edu.ntnu.stud.model.validators.ArgumentValidator;

/**
 * <h3>MandelbrotSet</h3>
 * <p>This class generates an {@code WritableImage} object of an approximation of the Mandelbrot set.
 * The class is interacted with through {@link ActionController}, and to controll the number of
 * iterations in the numerical approxation we use {@link MandelbrotController}.</p>
 *
 * <p>Mathematical backgorund: The Mandelbrot set is the set of complex numbers, C,
 * such that when the function f(z) = z^2 + C is iterated from z = 0 + 0i doesn't diverge.
 * In other words, it is the set of stable Julia transforms from z = 0. The set is a fractal,
 * meaning that it is self-similar in many places.</p>
 *
 * <p>We do not prove that a chosen C (a pixel) is in the Mandelbrot set, but it is proven that
 * any z with modulus > 2 diverges when iterated on any Julia Transform, regardless of the
 * argument. We can utilize this to approximate the Mandelbrot set, by iterating the Julia transform
 * from z = 0 for a given C, and if an iteration has modulo above 2, we can stop as we know
 * that the constant does not belong to the Mandelbrot set. If we pass though {@code maxIterations}
 * without any iteration leaving the circle of radius 2, we assume that the point belongs in the
 * Mandelbrot Set.</p>
 *
 * <p>We color the points in the Mandelbrot set Black. We color the points that diverges based on
 * how quickly they diverge. We use the number of iterations it took for the point to diverge to
 * color the point. We use the HSB color space, where the hue is the number of iterations divided
 * by the maximum number of iterations. Points that diverge quicklu will be red, and points that
 * diverge slowly will be blue or green.</p>
 *
 * Responsibility: Generate an image of the Mandelbrot set.
 *
 * @see ActionController
 * @see MandelbrotController
 * @see ColorUtils
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @see edu.ntnu.stud.model.transformation.JuliaTransform
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class MandelbrotSet {
  private final int width;
  private final int height;
  private final int maxIterations;

  /**
   * <p>Constructor for {@code MandelbrotSet} with the given width, height and maximum number of iterations.
   * This method is called in {@code AppInitializer} and the object is passed to {@link ActionController}</p>
   *
   * @param width the width of the image that is validated to be 1000 with {@link ArgumentValidator}
   * @param height the height of the image that is validated to be 1000 with {@link ArgumentValidator}
   * @param maxIterations the maximum number of iterations that is validated with {@link ArgumentValidator}
   */
  public MandelbrotSet(int width, int height, int maxIterations) {
    ArgumentValidator.mandelbrotSetConstructorValidator(width, height, maxIterations);
    this.width = width;
    this.height = height;
    this.maxIterations = maxIterations;
  }
  /**
   * <p>Generates an {@code WritableImage} object of the approximated Mandelbrot set.
   * The method is called in {@link ActionController}.</p>
   *
   * Algorithm:
   * We iterate over all pixels:
   *     Set z = 0
   *     transform the pixel to a complex number c
   *     iterate {@code maxIterations} times:
   *         z = z^2 + c
   *         if |z| > 2, break
   *     if we broke out of the loop: color the pixel based on the number of iterations before break
   *     else: color the pixel black
   * return image
   *
   * @return a {@code WritableImage} object of the approximated Mandelbrot set.
   */
  public WritableImage generateMandelbrotSet() {
    WritableImage image = new WritableImage(width, height);
    PixelWriter pixelWriter = image.getPixelWriter();

    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        double zReal = 0;
        double zImag = 0;
        double cReal = (x - width / 2) / (0.35 * width);
        double cImag = (y - height / 2) / (0.35 * height);
        int iter = maxIterations;
        while (zReal * zReal + zImag * zImag < 4 && iter > 0) {
          double tmp = zReal * zReal - zImag * zImag + cReal;
          zImag = 2.0 * zReal * zImag + cImag;
          zReal = tmp;
          iter--;
        }
        int rgb = ColorUtils.hsbToRgb((float) iter / maxIterations, 1, iter > 0 ? 1 : 0);
        pixelWriter.setColor(x, y, Color.rgb((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF));
      }
    }
    return image;
  }
}
