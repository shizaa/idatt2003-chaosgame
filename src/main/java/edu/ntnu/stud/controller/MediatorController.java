package edu.ntnu.stud.controller;

import javafx.scene.control.ComboBox;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import edu.ntnu.stud.model.validators.ArgumentValidator;

/**
 * <h3>Mediator Controller</h3>
 * <p>The class, {@code MediatorController}, implements the Mediator design pattern to manage
 * and coordinate the interaction between different parts of the application.</p>

 * Responsibility: Updating a ComboBox with file names from a specified directory
 *
 * @see javafx.scene.control.ComboBox
 * @see edu.ntnu.stud.model.validators.ArgumentValidator
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class MediatorController {

  private final String textFilesDir;

  /**
   * Constructs a new {@code MediatorController} with the specified directory for text files.
   *
   * @param textFilesDir the directory containing text files
   * @throws IllegalArgumentException if the directory path is invalid
   */

  public MediatorController(String textFilesDir) {
    ArgumentValidator.validateDirectoryPathMediator(textFilesDir);
    this.textFilesDir = textFilesDir;
  }

  /**
   * <p>Updates the provided ComboBox with the names of text files from the specified directory.
   * Files are filtered to only include those with a ".txt" extension.
   * Utilizes the {@link #listFilesInDirectory(Predicate)} method to list files in the directory.</p>
   *
   * @param fileComboBox the ComboBox to update with file names
   */

  public void updateFileComboBox(ComboBox<String> fileComboBox) {
    List<String> files = listFilesInDirectory(name -> name.endsWith(".txt"));
    files.forEach(name -> {
      if (!fileComboBox.getItems().contains(name)) {
        fileComboBox.getItems().add(name);
      }
    });
  }

  /**
   * Lists the files in the specified directory that match the given filter.
   *
   * @param filter a predicate to filter file names
   * @return a list of file names that match the filter
   */

  public List<String> listFilesInDirectory(Predicate<String> filter) {
    File folder = createFolder();
    return Arrays.stream(Optional.ofNullable(folder.listFiles()).orElse(new File[0]))
            .filter(File::isFile)
            .map(File::getName)
            .filter(filter)
            .collect(Collectors.toList());
  }

  /**
   * <p>Creates a folder object for the specified directory path.
   * This method can be overridden in tests to provide a mock directory.</p>
   *
   * @return a File object representing the directory
   */
  public File createFolder() {
    return new File(this.textFilesDir);
  }
}
