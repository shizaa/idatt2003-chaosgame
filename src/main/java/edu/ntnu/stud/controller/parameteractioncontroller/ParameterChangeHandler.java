package edu.ntnu.stud.controller.parameteractioncontroller;

import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.exception.InvalidFormatException;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.validators.ArgumentValidator;
import edu.ntnu.stud.model.validators.InputValidator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.util.List;

/**
 * <h3>Parameter Change Handler</h3>
 * <p>The class, {@code ParameterChangeHandler}, handles the update of parameters related to
 * {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} and writes these changes to a file using {@link FileOperations}.
 * It also provides methods to create and update text fields in the user interface.
 * It uses {@link edu.ntnu.stud.model.validators.InputValidator} to validate user input.
 * The class was created to delegate the responsibility of handling parameter changes and writing to files from {@link ParameterController}.
 * It is used by {@link ParameterController} to handle parameter changes.
 * </p>
 *
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see FileOperations
 * @see InvalidFormatException
 * @see javafx.scene.control.TextArea
 * @see javafx.scene.control.ComboBox
 * @see javafx.scene.control.TextField
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class ParameterChangeHandler {

  private final ChaosGameDescription description;
  private final FileOperations fileOperations;

  /**
   * Constructs a new {@code ParameterChangeHandler} with the specified {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription}
   * and {@link FileOperations}.
   *
   * @param description the chaos game description to update
   * @param fileOperations the file operations handler
   */

  public ParameterChangeHandler(ChaosGameDescription description, FileOperations fileOperations) {
    this.description = description;
    this.fileOperations = fileOperations;
  }

  /**
   * <p>Creates text fields for the {@code real} and {@code imaginary} parts of the Julia constant.
   * The method is used in {@link ParameterController} to create text fields for the user interface.</p>
   *
   * @return a list of text fields for the Julia constant
   */

  public List<TextField> createConstantcTextFields() {
    return List.of(
            new TextField(String.valueOf(description.getJuliaConstant().getRealPart())),
            new TextField(String.valueOf(description.getJuliaConstant().getImaginaryPart()))
    );
  }
  /**
   * <p>Updates the Julia constant from the provided text fields and writes the changes to a file.
   * The method is called in {@link ParameterController} when the user clicks the "Update button for the Julia constant.
   * Utilizes {@link InputValidator} to validate the input values, {@link ArgumentValidator} to validate the errors,
   * and {@link FileOperations} to write the changes to a file.</p>
   * </p>
   *
   * @param fields the text fields containing the new Julia constant values
   * @param feedbackArea the text area for providing feedback
   * @param originalFilePath the path of the original file
   * @param fileNameField the text area for the file name
   * @throws InvalidFormatException if the input values are invalid
   */

  public void updateConstantcAndWriteToFile(List<TextField> fields, TextArea feedbackArea, String originalFilePath, TextField fileNameField) throws InvalidFormatException {
    List<String> errors = InputValidator.validateConstantcFields(fields);
    ArgumentValidator.validateErrors(errors);
    description.updateJuliaConstant(Double.parseDouble(fields.get(0).getText()), Double.parseDouble(fields.get(1).getText()));
    fileOperations.writeChangesToFile(description, originalFilePath, feedbackArea, fileNameField.getText());
  }

  /**
   * <p>Creates text fields for the minimum coordinates.
   * The method is used in {@link ParameterController} to create text fields for the user interface.</p>
   *
   * @return a list of text fields for the minimum coordinates
   */

  public List<TextField> createMinCoordsTextFields() {
    return List.of(
            new TextField(String.valueOf(description.getMinCoords().getX0())),
            new TextField(String.valueOf(description.getMinCoords().getX1()))
    );
  }

  /**
   * <p>Updates the minimum coordinates from the provided text fields and writes the changes to a file.
   * The method is called in {@link ParameterController} when the user clicks the "Update" button for the minimum coordinates.
   * Utilizes {@link InputValidator} to validate the input values, {@link ArgumentValidator} to validate the errors,
   * and {@link FileOperations} to write the changes to a file.</p>
   *
   * @param fields the text fields containing the new minimum coordinates
   * @param feedbackArea the text area for providing feedback
   * @param originalFilePath the path of the original file
   * @param fileNameField the text area for the file name
   * @throws InvalidFormatException if the input values are invalid
   */

  public void updateMinCoordsAndWriteToFile(List<TextField> fields, TextArea feedbackArea, String originalFilePath, TextField fileNameField) throws InvalidFormatException {
    List<String> errors = InputValidator.validateCoordsFields(fields);
    ArgumentValidator.validateErrors(errors);
    description.setMinCoords(new Vector2D(Double.parseDouble(fields.get(0).getText()), Double.parseDouble(fields.get(1).getText())));
    fileOperations.writeChangesToFile(description, originalFilePath, feedbackArea, fileNameField.getText());
  }

  /**
   * <p>Creates text fields for the maximum coordinates.
   * The method is used in {@link ParameterController} to create text fields for the user interface.</p>
   *
   * @return a list of text fields for the maximum coordinates
   */

  public List<TextField> createMaxCoordsTextFields() {
    return List.of(
            new TextField(String.valueOf(description.getMaxCoords().getX0())),
            new TextField(String.valueOf(description.getMaxCoords().getX1()))
    );
  }

  /**
   * <p>Updates the maximum coordinates from the provided text fields and writes the changes to a file.
   * The method is called in {@link ParameterController} when the user clicks the "Update" button for the maximum coordinates.
   * Utilizes {@link InputValidator} to validate the input values, {@link ArgumentValidator} to validate the errors,
   * and {@link FileOperations} to write the changes to a file.</p>
   *
   * @param fields the text fields containing the new maximum coordinates
   * @param feedbackArea the text area for providing feedback
   * @param originalFilePath the path of the original file
   * @throws InvalidFormatException if the input values are invalid
   */

  public void updateMaxCoordsAndWriteToFile(List<TextField> fields, TextArea feedbackArea, String originalFilePath, TextField fileNameField) throws InvalidFormatException {
    List<String> errors = InputValidator.validateCoordsFields(fields);
    ArgumentValidator.validateErrors(errors);
    description.setMaxCoords(new Vector2D(Double.parseDouble(fields.get(0).getText()), Double.parseDouble(fields.get(1).getText())));
    fileOperations.writeChangesToFile(description, originalFilePath, feedbackArea, fileNameField.getText());
  }

  /**
   * <p>Updates the provided text fields with the values of the specified transform.
   * If the transform is a {@link Matrix2x2}, the text fields are updated with the matrix values.
   * If the transform is a {@link Vector2D}, the text fields are updated with the vector values.
   * The method is used in {@link ParameterController} to update the text fields in the user interface.</p>
   *
   * @param textFields the text fields to update
   * @param transform the transform object containing the new values
   */

  public void updateTextFields(List<TextField> textFields, Object transform) {
    if (transform instanceof Matrix2x2 matrix) {
      textFields.get(0).setText(String.valueOf(matrix.getA00()));
      textFields.get(1).setText(String.valueOf(matrix.getA01()));
      textFields.get(2).setText(String.valueOf(matrix.getA10()));
      textFields.get(3).setText(String.valueOf(matrix.getA11()));
    } else if (transform instanceof Vector2D vector) {
      textFields.get(0).setText(String.valueOf(vector.getX0()));
      textFields.get(1).setText(String.valueOf(vector.getX1()));
    }
  }
}
