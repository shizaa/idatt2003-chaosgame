package edu.ntnu.stud.controller.parameteractioncontroller;

import edu.ntnu.stud.controller.MediatorController;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.validators.ArgumentValidator;
import edu.ntnu.stud.model.validators.InputValidator;
import edu.ntnu.stud.view.panels.PanelCreator;
import edu.ntnu.stud.view.panels.ParameterElements;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * <h3>Parameter Controller</h3>
 * The {@code ParameterController} class manages the UI action events for the parameter editor section in the application.
 * It allows users to update parameters through a graphical user interface and handles the validation and persistence of these changes.
 * The class has delegated the responsibility of handling parameter changes and writing to files to {@link ParameterChangeHandler}.
 *
 * <p>It provides methods to change various parameters of the application, such as constant C value, min/max coordinates, vector values, and matrix values.
 * Each change method validates the input, updates the corresponding parameter in the {@link ChaosGameDescription}, and writes the changes to a file..</p>
 *
 * <p>This class uses {@link ParameterElements} for creating and managing UI elements, {@link MediatorController} for coordinating with other parts of the application,
 * and {@link FileOperations} for file-related operations. It also uses {@link InputValidator} and {@link ArgumentValidator} for input and argument validation.</p>

 * Responsibility: Handling parameter changes and writing to files.
 *
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see ParameterElements
 * @see MediatorController
 * @see FileOperations
 * @see InputValidator
 * @see javafx.scene.control.TextArea
 * @see javafx.scene.control.ComboBox
 * @see javafx.scene.control.TextField
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */
public class ParameterController {

  private static final String REQUIRED_VALUES_ERROR = "Transform type of read file does not include the required values\n";
  private static final String FILE_ERROR = "An error occurred while writing to the file";

  private final ParameterElements parameterElements;
  private final FileOperations fileOperations;
  private final MediatorController mediatorController;

  private ChaosGameDescription description;
  private String originalFilePath;
  private final PanelCreator panelCreator;
  private ParameterChangeHandler parameterChangeHandler;

  /**
   * Constructs a new {@code ParameterController} with the specified {@link ParameterElements},
   * {@link MediatorController}, and {@link FileOperations}.
   *
   * @param parameterElements the parameter elements for the UI
   * @param mediatorController the mediator controller for updating file lists
   * @param fileOperations the file operations handler
   * @param panelCreator the panel creator for creating UI elements
   * @throws IllegalArgumentException if any of the parameters are null
   */
  public ParameterController(ParameterElements parameterElements, MediatorController mediatorController, FileOperations fileOperations, PanelCreator panelCreator) {
    ArgumentValidator.validateNonNullParameterController(parameterElements == null || mediatorController == null, "ParameterElements and MediatorController cannot be null");
    this.parameterElements = parameterElements;
    this.fileOperations = fileOperations;
    this.mediatorController = mediatorController;
    this.panelCreator = panelCreator;
  }
  /**
   * Sets the {@link ChaosGameDescription} and original file path for the controller.
   *
   * @param description the chaos game description
   * @param originalFilePath the path of the original file
   * @throws IllegalArgumentException if the description or originalFilePath is null
   */

  public void setDescription(ChaosGameDescription description, String originalFilePath) {
    ArgumentValidator.validateNonNullParameterController(description == null || originalFilePath == null, "Description and originalFilePath cannot be null");
    this.description = description;
    this.originalFilePath = originalFilePath;
    this.parameterChangeHandler = new ParameterChangeHandler(description, fileOperations);
  }

  /**
   * Changes a parameter using the provided details and action.
   * It sets up the {@code EditorStage} using the {@link ParameterElements#setupEditorStage} method and sets the {@code Scene} using the {@link ParameterElements#setScene} method.
   * The method creates a {@code VBox} using the {@link #createVbox} method, which contains the text fields, feedback area, and buttons for navigation.
   * For the validator and action, it uses the provided {@code BiConsumer} to perform the parameter change.
   *
   * @param title the title of the parameter change
   * @param textFields the text fields for the parameters
   * @param feedbackTextArea the text area for feedback
   * @param action the action to perform on parameter change
   * @param buttonBox the button box for navigation
   * @param fileNameField the text field for the file name
   * @param fileComboBox the combo box for file selection
   * @throws IllegalArgumentException if any of the parameters are null
   */

  public void changeParameter(String title, List<TextField> textFields, TextArea feedbackTextArea,
                              BiConsumer<List<TextField>, TextArea> action, HBox buttonBox, TextField fileNameField, ComboBox<String> fileComboBox) {
    ArgumentValidator.validateNonNullParameterController(textFields == null || feedbackTextArea == null || action == null || buttonBox == null || fileNameField == null || fileComboBox == null, "Arguments cannot be null");

    parameterElements.setupEditorStage(title);
    VBox vbox = createVbox(buttonBox, textFields, feedbackTextArea, action, fileNameField);
    parameterElements.setScene(vbox);
  }

  /**
   * <p>Creates a VBox layout for the parameter editor.
   * It uses the {@link ParameterElements#getGridPane} method to create a GridPane with the provided text fields.
   * Then, it uses the {@link ParameterElements#createVbox} method to create a VBox with the provided button box, grid pane, file name field, text fields, feedback text area, and action.</p>
   *
   * @param buttonBox the button box for navigation
   * @param textFields the text fields for the parameters
   * @param feedbackTextArea the text area for feedback
   * @param action the action to perform on parameter change
   * @param fileNameField the text field for the file name
   * @return a VBox layout for the parameter editor
   */
  private VBox createVbox(HBox buttonBox, List<TextField> textFields, TextArea feedbackTextArea, BiConsumer<List<TextField>, TextArea> action, TextField fileNameField) {
    GridPane grid = parameterElements.getGridPane(textFields);
    return parameterElements.createVbox(buttonBox, grid, fileNameField, textFields, feedbackTextArea, action);
  }

  /**
   * <p>Initiates the process to change the Constant C value.
   * It creates text fields for the Constant C value using the {@link ParameterChangeHandler#createConstantcTextFields} method.
   * Then, it calls the {@link #changeParameter} method with the appropriate parameters to handle the UI changes and the update process.
   * If an error occurs during the process, it catches the exception and appends the error message to the feedback text area. </p>
   *
   * @param feedbackTextArea the text area for feedback
   */
  public void changeConstantC(TextArea feedbackTextArea) {
    try {
      List<TextField> textFields = parameterChangeHandler.createConstantcTextFields();
      TextField fileNameField = parameterElements.getTextField();
      changeParameter("Edit Constant C Value", textFields, feedbackTextArea,
              (fields, feedbackArea) -> {
                try {
                  parameterChangeHandler.updateConstantcAndWriteToFile(fields, feedbackArea, originalFilePath, fileNameField);
                  mediatorController.updateFileComboBox(panelCreator.getFileComboBox());
                } catch (IllegalArgumentException e) {
                  feedbackArea.appendText(e.getMessage() + "\n");
                }
              },
              new HBox(), fileNameField, panelCreator.getFileComboBox());
    } catch (Exception ex) {
      feedbackTextArea.appendText(REQUIRED_VALUES_ERROR);
    }
  }

  /**
   * <p>Initiates the process to change the minimum coordinates.
   * It creates text fields for the minimum coordinates using the {@link ParameterChangeHandler#createMinCoordsTextFields} method.
   * Then, it calls the {@link #changeParameter} method with the appropriate parameters to handle the UI changes and the update process.
   * If an error occurs during the process, it catches the exception and appends the error message to the feedback text area. </p>
   *
   * @param feedbackTextArea the text area for feedback
   */
  public void changeMinCoords(TextArea feedbackTextArea) {
    try {
      List<TextField> textFields = parameterChangeHandler.createMinCoordsTextFields();
      TextField fileNameField = parameterElements.getTextField();
      changeParameter("Edit Min Coordinates", textFields, feedbackTextArea,
              (fields, feedbackArea) -> {
                try {
                  parameterChangeHandler.updateMinCoordsAndWriteToFile(fields, feedbackArea, originalFilePath, fileNameField);
                  mediatorController.updateFileComboBox(panelCreator.getFileComboBox());
                } catch (IllegalArgumentException e) {
                  feedbackArea.appendText("Invalid input: " + e.getMessage() + "\n");
                }
              },
              new HBox(), fileNameField, panelCreator.getFileComboBox());
    } catch (Exception ex) {
      feedbackTextArea.appendText(FILE_ERROR);
    }
  }

  /**
   * Initiates the process to change the maximum coordinates.
   * It creates text fields for the maximum coordinates using the {@link ParameterChangeHandler#createMaxCoordsTextFields} method.
   * Then, it calls the {@link #changeParameter} method with the appropriate parameters to handle the UI changes and the update process.
   * If an error occurs during the process, it catches the exception and appends the error message to the feedback text area.
   *
   * @param feedbackTextArea the text area for feedback
   */
  public void changeMaxCoords(TextArea feedbackTextArea) {
    try {
      List<TextField> textFields = parameterChangeHandler.createMaxCoordsTextFields();
      TextField fileNameField = parameterElements.getTextField();
      changeParameter("Edit Max Coordinates", textFields, feedbackTextArea,
              (fields, feedbackArea) -> {
                try {
                  parameterChangeHandler.updateMaxCoordsAndWriteToFile(fields, feedbackArea, originalFilePath, fileNameField);
                  mediatorController.updateFileComboBox(panelCreator.getFileComboBox());
                } catch (IllegalArgumentException e) {
                  feedbackArea.appendText("Invalid input: " + e.getMessage() + "\n");
                }
              },
              new HBox(), fileNameField, panelCreator.getFileComboBox());
    } catch (Exception ex) {
      feedbackTextArea.appendText(FILE_ERROR);
    }
  }

  /**
   * <p>Initiates the process to change the vector values.
   * It uses the {@link #executeTransformationChange} method, providing it with the necessary parameters to handle the UI changes and the update process.
   * The method uses a lambda function to get the vector at a specific index
   * from the {@link ChaosGameDescription} and another lambda function to update the vector and write to file. </p>
   *
   * @param feedbackTextArea the text area for feedback
   */
  public void changeVector(TextArea feedbackTextArea) {
    executeTransformationChange(feedbackTextArea, "Edit Vector Values",
            index -> description.getVector(index),
            this::updateVectorAndWriteToFile);
  }
  /**
   * <p>Updates the vector values and writes them to a file.
   * It validates the input fields using the {@link InputValidator#validateCoordsFields} method.
   * If the validation is successful, it updates the vector at the specified index using the {@link ChaosGameDescription#updateVector} method.
   * If the validation fails, it throws an IllegalArgumentException with the error messages. </p>
   *
   * @param index the index of the vector to update
   * @param fields the text fields containing the new vector values
   * @throws IllegalArgumentException if the validation fails
   */

  private void updateVectorAndWriteToFile(int index, List<TextField> fields) {
    List<String> errors = InputValidator.validateCoordsFields(fields);
    if (errors.isEmpty()) {
      description.updateVector(index, Double.parseDouble(fields.get(0).getText()), Double.parseDouble(fields.get(1).getText()));
      mediatorController.updateFileComboBox(panelCreator.getFileComboBox());
    } else {
      throw new IllegalArgumentException(String.join("\n", errors));
    }
  }

  /**
   * <p>Initiates the process to change the matrix values.
   * It uses the {@link #executeTransformationChange} method, providing it with the necessary parameters to handle the UI changes and the update process.
   * The method uses a lambda function to get the matrix at a specific index from the {@link ChaosGameDescription} and another lambda function to update the matrix and write to file. </p>
   *
   * @param feedbackTextArea the text area for feedback
   */

  public void changeMatrix(TextArea feedbackTextArea) {
    executeTransformationChange(feedbackTextArea, "Edit Matrix Values",
            index -> description.getMatrix().get(index),
            this::updateMatrixAndWriteToFile);
  }

  /**
   * <p>Updates the matrix values and writes them to a file.
   * It validates the input fields using the {@link InputValidator#validateMatrixFields} method.
   * If the validation is successful, it updates the matrix at the specified index using the {@link ChaosGameDescription#updateMatrix} method.
   * If the validation fails, it throws an IllegalArgumentException with the error messages. </p>
   *
   * @param index the index of the matrix to update
   * @param fields the text fields containing the new matrix values
   * @throws IllegalArgumentException if the validation fails
   */

  private void updateMatrixAndWriteToFile(int index, List<TextField> fields) {
    List<String> errors = InputValidator.validateMatrixFields(fields);
    if (errors.isEmpty()) {
      description.updateMatrix(index,
              Double.parseDouble(fields.get(0).getText()),
              Double.parseDouble(fields.get(1).getText()),
              Double.parseDouble(fields.get(2).getText()),
              Double.parseDouble(fields.get(3).getText()));
      mediatorController.updateFileComboBox(panelCreator.getFileComboBox());
    } else {
      throw new IllegalArgumentException(String.join("\n", errors));
    }
  }

  /**
   * <p>Executes a transformation change for a given type of transformation.
   * It first applies the provided transformation function to get the transformation at the specified index.
   * Then, it creates text fields for the transformation using the {@link ParameterElements#createTextFields} method.
   * It also creates a button box for navigation using the {@link #createButtonBox} method.
   * Finally, it calls the {@link #changeParameter} method with the appropriate parameters to handle the UI changes and the update process.
   * If an error occurs during the process, it catches the exception and appends the error message to the feedback text area. </p>
   *
   * @param <T> the type of the transformation
   * @param feedbackTextArea the text area for feedback
   * @param title the title of the transformation change
   * @param getTransform the function to get the transformation at a specific index
   * @param updateTransform the function to update the transformation and write to file
   */
  private <T> void executeTransformationChange(TextArea feedbackTextArea, String title,
                                               Function<Integer, T> getTransform,
                                               BiConsumer<Integer, List<TextField>> updateTransform) {
    try {
      int[] transformIndex = {0};
      T transform = getTransform.apply(transformIndex[0]);
      List<TextField> textFields = parameterElements.createTextFields(transform);
      TextField fileNameField = parameterElements.getTextField();
      HBox buttonBox = createButtonBox(transformIndex, getTransform, textFields);
      changeParameter(title, textFields, feedbackTextArea, createUpdateAndWriteToFileFunction(updateTransform, transformIndex, fileNameField), buttonBox, fileNameField, panelCreator.getFileComboBox());
    } catch (Exception ex) {
      feedbackTextArea.appendText(REQUIRED_VALUES_ERROR);
    }
  }

  /**
   * <p>Creates a function to update a transformation and write the changes to a file.
   * The function accepts a list of text fields and a feedback text area.
   * It first applies the provided update function to the transformation at the specified index.
   * Then, it writes the changes to a file using the {@link FileOperations#writeChangesToFile} method.
   * Finally, it updates the file combo box using the {@link MediatorController#updateFileComboBox} method.
   * If an error occurs during the process, it catches the IllegalArgumentException and appends the error message to the feedback text area. </p>
   *
   * @param updateTransform the function to update the transformation
   * @param transformIndex the index of the transformation to update
   * @param fileNameField the text field for the file name
   * @return a function that updates a transformation and writes the changes to a file
   */

  private BiConsumer<List<TextField>, TextArea> createUpdateAndWriteToFileFunction(BiConsumer<Integer, List<TextField>> updateTransform, int[] transformIndex, TextField fileNameField) {
    return (fields, feedbackArea) -> {
      try {
        updateTransform.accept(transformIndex[0], fields);
        fileOperations.writeChangesToFile(description, originalFilePath, feedbackArea, fileNameField.getText().trim());
        mediatorController.updateFileComboBox(panelCreator.getFileComboBox());
      } catch (IllegalArgumentException e) {
        feedbackArea.appendText("Invalid input: " + e.getMessage() + "\n");
      }
    };
  }

  /**
   * <p>Creates a box containing buttons for navigating through transformations.
   * It creates a 'Next' button and a 'Previous' button using the {@link ParameterElements#createNextTransformationButton} and {@link ParameterElements#createPrevTransformationButton} methods respectively.
   * The 'Next' button increments the counter and wraps around to the start of the transformations list when it reaches the end.
   * The 'Previous' button decrements the counter and wraps around to the end of the transformations list when it reaches the start.
   * Both buttons call the {@link #transformationIterator} method to update the text fields with the current transformation.
   * Finally, it returns a box containing the 'Next' and 'Previous' buttons using the {@link ParameterElements#getNextPrevTransformationButtonBox} method.</p>
   *
   * @param <T> the type of the transformation
   * @param counter the counter for the current transformation index
   * @param getTransform the function to get the transformation at a specific index
   * @param textFields the text fields for the transformation parameters
   * @return a box containing the 'Next' and 'Previous' buttons
   */
  private <T> HBox createButtonBox(int[] counter, Function<Integer, T> getTransform, List<TextField> textFields) {
    Button nextButton = parameterElements.createNextTransformationButton((v1, v2) -> {
      counter[0] = (counter[0] + 1) % description.getTransformationsList().size();
      transformationIterator(counter, getTransform, textFields);
    });
    Button prevButton = parameterElements.createPrevTransformationButton((v1, v2) -> {
      counter[0] = (counter[0] - 1 + description.getTransformationsList().size()) % description.getTransformationsList().size();
      transformationIterator(counter, getTransform, textFields);
    });

    return ParameterElements.getNextPrevTransformationButtonBox(nextButton, prevButton);
  }

  /**
   * <p>Iterates over transformations and updates the text fields with the current transformation.
   * It first applies the provided transformation function to get the transformation at the specified index.
   * Then, it updates the text fields with the current transformation using the {@link ParameterChangeHandler#updateTextFields} method.
   * The method is run on the JavaFX Application thread to ensure that the UI updates correctly.</p>
   *
   * @param <T> the type of the transformation
   * @param counter the counter for the current transformation index
   * @param getTransform the function to get the transformation at a specific index
   * @param textFields the text fields for the transformation parameters
   */
  private <T> void transformationIterator(int[] counter, Function<Integer, T> getTransform, List<TextField> textFields) {
    Platform.runLater(() -> {
      T transform = getTransform.apply(counter[0]);
      parameterChangeHandler.updateTextFields(textFields, transform);
    });
  }
}
