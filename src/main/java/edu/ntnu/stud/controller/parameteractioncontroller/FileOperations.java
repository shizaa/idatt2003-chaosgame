package edu.ntnu.stud.controller.parameteractioncontroller;

import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler;
import edu.ntnu.stud.model.exception.FileOperationException;
import edu.ntnu.stud.model.validators.FileExceptionValidator;
import javafx.scene.control.TextArea;

/**
 * <h3>File Operations</h3>
 * <p>The class, {@code FileOperations}, handles file operations related to {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription}.
 * It uses {@link edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler} to perform write operations,
 * and provides feedback through a {@link javafx.scene.control.TextArea}.</p>
 *
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler
 * @see edu.ntnu.stud.model.exception.FileOperationException
 * @see edu.ntnu.stud.model.validators.FileExceptionValidator
 * @see javafx.scene.control.TextArea
 * @see javafx.scene.control.ComboBox
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class FileOperations {
  private final ChaosGameFileHandler chaosGameFileHandler;

  /**
   * Constructs a new {@code FileOperations} with the specified {@link edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler}.
   *
   * @param chaosGameFileHandler the file handler to use for file operations
   */
  public FileOperations(ChaosGameFileHandler chaosGameFileHandler) {
    this.chaosGameFileHandler = chaosGameFileHandler;
  }

  /**
   * <p>Writes the changes of a {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to a file.
   * If a new file name is provided, the changes are saved to a new file; otherwise, they are saved to the original file.
   * Feedback is provided through the specified {@link javafx.scene.control.TextArea}.
   * Utilizes {@link edu.ntnu.stud.model.validators.FileExceptionValidator} to log exceptions.</p>
   *
   * @param description the {@link edu.ntnu.stud.model.chaosgame.ChaosGameDescription} to write to the file
   * @param originalFilePath the path of the original file
   * @param feedbackTextArea the {@link javafx.scene.control.TextArea} to provide feedback
   * @param fileName the name of the new file, if applicable
   */

  public void writeChangesToFile(ChaosGameDescription description, String originalFilePath, TextArea feedbackTextArea, String fileName) {
    try {
      String path = fileName.isEmpty() ? originalFilePath : "src/main/resources/textFiles/" + fileName + ".txt";
      chaosGameFileHandler.writeToFile(description, path);
      feedbackTextArea.appendText(
              "Changes saved to "
                      + (fileName.isEmpty() ? "existing file.\n" : "new file: " + fileName + ".txt\n"));
    } catch (FileOperationException e) {
      feedbackTextArea.appendText("An error occurred while writing to the file" + (fileName.isEmpty() ? "" : ": " + fileName + ".txt\n"));
      FileExceptionValidator.logException(e, "Failed to write changes to file: " + fileName);
    }
  }
}
