package edu.ntnu.stud.controller;

import edu.ntnu.stud.config.AppInitializer;
import edu.ntnu.stud.model.interfaces.KeyboardObserver;
import edu.ntnu.stud.view.display.ChaosGameDisplay;
import javafx.scene.input.KeyEvent;
/**
 * <h3>Keyboard Controller</h3>
 * <p>{@code KeyboardController} is a Controller class for handling keyboard input.
 * It implements the {@code KeyboardObserver} interface, which defines a method for
 * handling key presses.</p>
 *
 * <p>Responsibility: Handle key presses and call the appropriate method in the {@code ChaosGameDisplay} class.</p>
 *
 * @see KeyboardObserver
 * @see edu.ntnu.stud.model.controls.KeyboardsControls
 * @see KeyEvent
 * @see ChaosGameDisplay
 * @see AppInitializer
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class KeyboardController implements KeyboardObserver {
  private final ChaosGameDisplay display;

  /**
   * <p>Constructor for the {@code KeyboardController} class. This observer will notify the {@code ChaosGameDisplay}
   * argument of which key was pressed with {@code onKeyPressed}.</p>
   *
   * @param display The {@code ChaosGameDisplay} object to call methods on.
   */
  public KeyboardController(ChaosGameDisplay display) {
    this.display = display;
  }

  /**
   * <p>Method for handling key presses. It checks which key was pressed and calls the appropriate method in the
   * {@code ChaosGameDisplay} class, which will be handled in {@link edu.ntnu.stud.view.display.ChaosGameDisplay#update}</p>
   *
   * @param e The {@code KeyEvent} object that contains information about the key press.
   */

  @Override
  public void onKeyPressed(KeyEvent e) {
    switch (e.getCode()) {
      case X -> display.xpressed();
      case Y -> display.ypressed();
      case P -> display.ppressed();
      case M -> display.mpressed();
    }
  }
}

