package edu.ntnu.stud.controller.fractalactioncontroller;

import java.util.HashMap;

/**
 * <h3>Mandelbrot Controller</h3>
 *
 * <p>{@code MandelbrotController} is the controller for {@link edu.ntnu.stud.model.transformation.MandelbrotSet}.
 * This class is initialized in the {@link edu.ntnu.stud.config.AppInitializer} class and is
 * interacted with in the {@link edu.ntnu.stud.controller.fractalactioncontroller.ActionController} class.</p>
 *
 * <p> Responsibility: This class is responsible for increasing and decreasing the
 * number of iterations used to calculate the Mandelbrot fractal.</p>
 *
 * @see edu.ntnu.stud.model.transformation.MandelbrotSet
 * @see edu.ntnu.stud.controller.fractalactioncontroller.ActionController
 * @see edu.ntnu.stud.config.AppInitializer
 * @see edu.ntnu.stud.view.display.ChaosGameDisplay
 * @see java.util.HashMap
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class MandelbrotController {

  /**
   * <p>{@code INCREASE_MANDELBROT_ITERATIONS} and {@code DECREASE_MANDELBROT_ITERATIONS} are
   * {@link java.util.HashMap} that maps the current number of iterations to the next number
   * of iterations to be used in the Mandelbrot fractal calculation.
   * A {@link java.util.HashMap} was chosen instead of some increasing factor because when
   * the {@link edu.ntnu.stud.controller.fractalactioncontroller.ActionController} tries to increase
   * the number of iterations from 1000 it will still map to 1000 (and vice versa for decreasing from 5).
   * This eliminates the need for testing if the number of iterations has reached some limit, and makes the
   * code simpler.</p>
   */

  private final HashMap<Integer, Integer> increaseMandelbrotIterations =
          new HashMap<>() {
            {
              put(2, 3);
              put(3, 4);
              put(4, 5);
              put(5, 6);
              put(6, 7);
              put(7, 8);
              put(8, 9);
              put(9, 10);
              put(10, 12);
              put(12, 15);
              put(15, 20);
              put(20, 25);
              put(25, 30);
              put(30, 40);
              put(40, 50);
              put(50, 75);
              put(75, 100);
              put(100, 200);
              put(200, 350);
              put(350, 500);
              put(500, 750);
              put(750, 1000);
              put(1000, 1000);
            }
          };
  private static final HashMap<Integer, Integer> DecreaseMandelbrotIterations =
          new HashMap<>() {
            {
              put(1000, 750);
              put(750, 500);
              put(500, 350);
              put(350, 200);
              put(200, 100);
              put(100, 75);
              put(75, 50);
              put(50, 40);
              put(40, 30);
              put(30, 25);
              put(25, 20);
              put(20, 15);
              put(15, 12);
              put(12, 10);
              put(10, 9);
              put(9, 8);
              put(8, 7);
              put(7, 6);
              put(6, 5);
              put(5, 4);
              put(4, 3);
              put(3, 2);
              put(2, 2);
            }
          };

  private int mandelbrotIterations = 40;
  private boolean isActive;

  /**
   * <p>Getter for the number of iterations used in the Mandelbrot fractal calculation.</p>
   *
   * @return {@code int} The number of iterations used in the Mandelbrot fractal calculation.
   */
  public int getMandelbrotIterations() {
    return mandelbrotIterations;
  }

  /**
   * <p>Setter to activate/deactivate the {@link #increaseIterations()} and {@link #decreaseIterations()} methods.
   * When the {@link edu.ntnu.stud.view.display.ChaosGameDisplay} registers that P or M is pressed, it will call these
   * methods, but they will only respond if {@link #isActive} is set to {@code true} by
   * {@link edu.ntnu.stud.controller.fractalactioncontroller.ActionController}.</p>
   *
   * @param active {@code boolean}
   */
  public void setActive(boolean active) {
    isActive = active;
  }

  /**
   * <p>Method to increase the number of iterations used in the Mandelbrot fractal calculation.
   * This method is called by the {@link edu.ntnu.stud.controller.fractalactioncontroller.ActionController} class
   * when the user presses the P key. It uses {@link #increaseMandelbrotIterations} to find the next number in the sequence.</p>
   */

  public void increaseIterations() {
    if (isActive) {
      mandelbrotIterations = increaseMandelbrotIterations.get(mandelbrotIterations);
    }
  }

  /**
   * <p>Method to decrease the number of iterations used in the Mandelbrot fractal calculation.
   * This method is called by the {@link edu.ntnu.stud.controller.fractalactioncontroller.ActionController} class
   * when the user presses the M key. It uses {@link #DecreaseMandelbrotIterations} to find the next number in the sequence.</p>
   */
  public void decreaseIterations() {
    if (isActive) {
      mandelbrotIterations = DecreaseMandelbrotIterations.get(mandelbrotIterations);
    }
  }
}
