package edu.ntnu.stud.controller.fractalactioncontroller;

import edu.ntnu.stud.controller.MediatorController;
import edu.ntnu.stud.controller.parameteractioncontroller.ParameterController;
import edu.ntnu.stud.model.chaosgame.*;
import edu.ntnu.stud.model.exception.FileOperationException;
import edu.ntnu.stud.model.exception.InvalidFormatException;
import edu.ntnu.stud.model.transformation.MandelbrotSet;
import edu.ntnu.stud.model.validators.FileExceptionValidator;
import edu.ntnu.stud.model.validators.InputValidator;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * <h3>ActionController</h3>
 *
 * <p>The {@link ActionController} class is responsible for handling the user actions related to
 * fractals. It is the main controller for the fractal-related user actions, like generating fractals. It catches the calls
 * made by {@link edu.ntnu.stud.view.display.ChaosGameDisplay}. </p>

 * Responibility: Handling fractal-related user actions.
 *
 * @see edu.ntnu.stud.view.display.ChaosGameDisplay
 * @see edu.ntnu.stud.model.chaosgame.ChaosGame
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameCanvas
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescription
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameDescriptionFactory
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameFractalFactory
 * @see edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler
 * @see edu.ntnu.stud.controller.fractalactioncontroller.MandelbrotController
 * @see edu.ntnu.stud.model.transformation.MandelbrotSet
 * @see edu.ntnu.stud.model.validators.InputValidator
 * @see edu.ntnu.stud.model.validators.FileExceptionValidator
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class ActionController {

  /**<p>The height and width of the canvas in the UI. The canvas will not change size so they are final</p>*/
  private static final int HEIGHT = 1000;
  private static final int WIDTH = 1000;

  private final UiHandler uiHandler;
  private final ParameterController parameterController;
  private final ChaosGameFractalFactory fractalFactory;
  private final MediatorController mediatorController;
  private final MandelbrotController mandelbrotController;

  public ChaosGameDescription description;
  private ChaosGameFileHandler chaosGameFileHandler;
  public ChaosGameCanvas canvas;
  public ChaosGame game;

  /**
   * <p>Constructor for the {@link ActionController} class. It will set the argumetns as attributes.
   * The object is initialized in the {@link edu.ntnu.stud.config.AppInitializer}, where all the arguments
   * are initialized too.</p>
   *
   * @param parameterController The parameter controller for handling parameter-related actions.
   * @param mediatorController The mediator controller for handling mediator-related actions.
   * @param fractalFactory The fractal factory for creating fractals.
   * @param uiHandler The UI handler for handling UI-related actions.
   * @param mandelbrotController The Mandelbrot controller for handling Mandelbrot-related actions.
   */
  public ActionController(
      ParameterController parameterController,
      MediatorController mediatorController,
      ChaosGameFractalFactory fractalFactory,
      UiHandler uiHandler,
      MandelbrotController mandelbrotController) {
    this.parameterController = parameterController;
    this.mediatorController = mediatorController;
    this.fractalFactory = fractalFactory;
    this.uiHandler = uiHandler;
    this.mandelbrotController = mandelbrotController;
  }

  /**
   * <p>Setter for the {@link ChaosGameFileHandler} attribute.</p>
   *
   * @param chaosGameFileHandler The chaos game file handler to set.
   */
  public void setChaosGameFileHandler(ChaosGameFileHandler chaosGameFileHandler) {
    this.chaosGameFileHandler = chaosGameFileHandler;
  }

  /**
   *
   * <p>This method will initialize the {@link edu.ntnu.stud.model.chaosgame.ChaosGameCanvas} and the {@link edu.ntnu.stud.model.chaosgame.ChaosGame} and set them as attributes.
   * The method is called in the {@link #confirmButton(ComboBox, File, TextArea, Button...)} and {@link #displayFractalsCombo} methods,
   * to make sure the canvas and game has been initialized before they generate the fractals.</p>
   */

  private void initializeCanvasAndGame() {
    this.canvas =
        new ChaosGameCanvas(
            WIDTH, HEIGHT, this.description.getMinCoords(), this.description.getMaxCoords());
    this.game = new ChaosGame(description, canvas);
  }

  /**
   * <p>This method will handle the confirm button action. It will read the file, validate it and load it.
   * It will also set the description and initialize the canvas and game. It will update the UI and show the feedback.
   * The action of the button is set to be linked to this function by {@link edu.ntnu.stud.view.display.ChaosGameDisplay}.</p>
   *
   * @param fileComboBox The file combo box to get the selected file from.
   * @param folder The folder where the file is stored.
   * @param feedbackTextArea The text area to show the feedback.
   * @param controlButtons The control buttons to update the UI.
   */

  public void confirmButton(
      ComboBox<String> fileComboBox,
      File folder,
      TextArea feedbackTextArea,
      Button... controlButtons) {
    try {
      String selectedFileName = fileComboBox.getValue();
      File file = new File(folder.getAbsolutePath(), selectedFileName);
      try {
        this.description = chaosGameFileHandler.validateAndLoadFile(file, feedbackTextArea);
        parameterController.setDescription(this.description, file.getAbsolutePath());
        initializeCanvasAndGame();
        uiHandler.updateUi(feedbackTextArea, controlButtons);
      } catch (FileNotFoundException ex) {
        FileExceptionValidator.handleFileNotFoundException(ex, feedbackTextArea, file);
      }
    }
    catch (Exception e) {
        feedbackTextArea.appendText("A file must be read first \n");
    }
  }

  /**
   * <p>This method will handle the confirm name button action. It will validate the inputs, create a new fractal file
   * and write it to the text files directory. It will also update the file combo box in the UI and show the feedback.
   * The action of the button is set to be linked to this function by {@link edu.ntnu.stud.view.display.ChaosGameDisplay}.</p>
   *
   * @param fileComboBox The file combo box to update.
   * @param feedbackTextArea The text area to show the feedback.
   * @param createEmptyFractalCombo The combo box to get the selected fractal type from.
   * @param emptyFractalName The text field to get the new fractal name from.
   * @param numTransformationsField The text field to get the number of transformations from.
   */

  public void confirmNameButton(
      ComboBox<String> fileComboBox,
      TextArea feedbackTextArea,
      ComboBox<String> createEmptyFractalCombo,
      TextField emptyFractalName,
      TextField numTransformationsField) {
    String selectedFractalType = createEmptyFractalCombo.getValue();
    String newFileName = emptyFractalName.getText();

    try {
      InputValidator.validateFractalInputs(
          selectedFractalType, newFileName, numTransformationsField);
      ChaosGameDescription description =
          fractalFactory.createDescription(selectedFractalType, numTransformationsField);
      if (description != null) {
        String testFilesDir = "src/main/resources/textFiles/";
        String newFilePath = testFilesDir + newFileName + ".txt";
        chaosGameFileHandler.writeToFile(description, newFilePath);
        feedbackTextArea.appendText(
            "New fractal file created successfully: " + newFileName + ".txt\n");
        mediatorController.updateFileComboBox(fileComboBox);
      }
    } catch (InvalidFormatException e) {
      feedbackTextArea.appendText("Invalid input: " + e.getMessage() + "\n");
    } catch (FileOperationException e) {
      feedbackTextArea.appendText("File operation error: " + e.getMessage() + "\n");
    }
  }

  /**
   * <p>This method will handle the display fractals combo action. It will extract the selected fractal from the combo box,
   * create the fractal description, initialize the canvas and game, generate the fractal image and display it.
   * It will also show the feedback. The action of the combo box is set to be linked to this function by
   * {@link edu.ntnu.stud.view.display.ChaosGameDisplay}.</p>
   *
   * @param feedbackTextArea The text area to show the feedback.
   * @param imageView The image view to display the fractal image.
   * @param displayFractalsCombo The combo box to get the selected fractal from.
   */

  public void displayFractalsCombo(
      TextArea feedbackTextArea, ImageView imageView, ComboBox<String> displayFractalsCombo) {
    String selectedFractal = displayFractalsCombo.getValue();
    try {
      this.description = ChaosGameDescriptionFactory.createFractal(selectedFractal);
      initializeCanvasAndGame();
      Image fractalImage = generateFractalImage(selectedFractal);
      imageView.setImage(fractalImage);
      feedbackTextArea.appendText("Fractal displayed: " + selectedFractal + "\n");
    } catch (FileNotFoundException e) {
      FileExceptionValidator.handleFileNotFoundException(
          e, feedbackTextArea, new File(selectedFractal));
    }
  }

  /**
   * <p>This method will increase the number of iterations for the {@link MandelbrotController}.
   * The number of iterations is as an argument in {@link MandelbrotSet#generateMandelbrotSet()}.
   * The method is therefore called in {@link edu.ntnu.stud.view.display.ChaosGameDisplay} when
   * the key P is pressed.</p>
   */
  public void increaseMandelbrotIterations() {
    mandelbrotController.increaseIterations();
  }

  /**
   * <p>This method will decrease the number of iterations for the {@link MandelbrotController}.
   * The number of iterations is as an argument in {@link MandelbrotSet#generateMandelbrotSet()}.
   * The method is therefore called in {@link edu.ntnu.stud.view.display.ChaosGameDisplay} when
   * the key M is pressed.</p>
   */
  public void decreaseMandelbrotIterations() {
    mandelbrotController.decreaseIterations();
  }

  /**
   * <p>This method will generate the fractal {@link Image} based on the selected fractal that is passed as argument.
   * If the selected fractal is Mandelbrot, it will generate the Mandelbrot set.
   * If not, it will play the Chaos Game normally. The method is called in {@link #displayFractalsCombo(TextArea, ImageView, ComboBox)}
   * and {@link #printAsciiButton(TextArea, ImageView)}.</p>
   *
   * <p>If the Mandelbrot set is selected the method will not execute the Chaos Game,
   * because the Mandelbrot set is generated in a different way than the rest of the fractals.
   * The Mandelbrot Set is generated by checking for divergence at a point, and not by drawing each
   * iteration to the canvas. Therefore, two different methods were required.</p>
   *
   * @param selectedFractal The selected fractal to generate the image for.
   * @return {@link Image} The generated fractal image.
   */
  
  public Image generateFractalImage(String selectedFractal) {
    if (selectedFractal.equals("Mandelbrot")) {
      mandelbrotController.setActive(true);
      int mandelbrotIterations = mandelbrotController.getMandelbrotIterations();
      MandelbrotSet mandelbrotSet = new MandelbrotSet(WIDTH, HEIGHT, mandelbrotIterations);
      return mandelbrotSet.generateMandelbrotSet();
    } else {
      mandelbrotController.setActive(false);
      int maxIterations = 1000000;
      game.runSteps(maxIterations);
      return canvas.generateFractalImage();
    }
  }
  
  /**
   * <p>This method will handle the print ASCII button action. It will generate the fractal image and display it.
   * It will also show the feedback. The action of the button linked to this function by
   * {@link edu.ntnu.stud.view.display.ChaosGameDisplay}.</p>
   *
   * <p>The method will generate the fractal image with the {@link #generateFractalImage(String)} method, and set
   * it to the {@link ImageView}. It will also append the feedback to the {@link TextArea}.</p>
   *
   * @param feedbackTextArea The text area to show the feedback.
   * @param imageView The image view to display the fractal image.
   */
  public void printAsciiButton(TextArea feedbackTextArea, ImageView imageView) {
    if (this.canvas != null) {
      Image fractalImage = this.canvas.generateFractalImage();
      imageView.setImage(fractalImage);
      feedbackTextArea.appendText("ASCII fractal printed.\n");
    } else {
      feedbackTextArea.appendText("No fractal to display. Please run iterations first.\n");
    }
  }

  /**
   * <p>This method will handle the cycle canvas color action. It will cycle the canvas color
   * and display the new fractal image. The method is called by {@link edu.ntnu.stud.view.display.ChaosGameDisplay}
   * when the key, X, is pressed.</p>
   *
   * @param imageView The image view to display the fractal image.
   */
  public void cycleCanvasColor(ImageView imageView) {
    if (this.canvas != null) {
      ChaosGameCanvas.colorCounter++;
      imageView.setImage(canvas.generateFractalImage());
    }
  }

  /**
   * <p>This method will handle the confirm iterations button action. It will validate the input,
   * run the iterations and show the feedback. The action of the button is set to be linked to this function by
   * {@link edu.ntnu.stud.view.display.ChaosGameDisplay}.</p>
   *
   * @param feedbackTextArea The text area to show the feedback.
   * @param iterationsField The text field to get the number of iterations from.
   */
  public void confirmIterationsButton(TextArea feedbackTextArea, TextField iterationsField) {
    try {
      InputValidator.validateNumberInput(
          iterationsField.getText(),
          "Invalid number of iterations entered. Must be an integer in the range [1, 10000000].");
      runIterations(feedbackTextArea, iterationsField);
    } catch (InvalidFormatException e) {
      feedbackTextArea.appendText(e.getMessage() + "\n");
    }
  }

  /**
   * <p>This method will run the iterations for the chaos game. It will get the number of iterations from the text field,
   * parse it to an integer and run the iterations. It will also give feedback in the {@code feedbackTextArea}. The method is called in {@link #confirmIterationsButton(TextArea, TextField)} ()}.</p>
   *
   * @param feedbackTextArea The text area to show the feedback.
   * @param iterationsField The text field to get the number of iterations from.
   */
  private void runIterations(TextArea feedbackTextArea, TextField iterationsField) {
    String iterations = iterationsField.getText();
    if (!iterations.isEmpty() && this.description != null) {
      int iterationsInt = Integer.parseInt(iterations);
      try {
        game.runSteps(iterationsInt);
        feedbackTextArea.appendText("Iterations completed successfully.\n");
      } catch (Exception e) {
        feedbackTextArea.appendText("TransformationsList is empty. Please read txt file again.\n");
      }
    } else {
      feedbackTextArea.appendText(
          "No iterations entered or no fractal data available. Make sure to read file first.\n");
    }
  }

  /**
   * <p>This method will reset the color of the canvas to the original color and display the new fractal image.
   * The method is called by {@link edu.ntnu.stud.view.display.ChaosGameDisplay} when the key, Y, is pressed.</p>
   *
   * @param imageView The image view to display the fractal image.
   */
  public void resetColorOfCanvas(ImageView imageView) {
    if (this.canvas != null) {
      ChaosGameCanvas.colorCounter = 0;
      imageView.setImage(canvas.generateFractalImage());
    }
  }
}
