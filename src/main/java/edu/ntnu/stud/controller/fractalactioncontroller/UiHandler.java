package edu.ntnu.stud.controller.fractalactioncontroller;

import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * <h3>Ui Handler</h3>
 * <p>{@code UiHandler} is a class for handling the file-related UI of the application.</p>
 *
 * <p>Responsibility: Update the UI of the application.</p>
 *
 * @see TextArea
 * @see Button
 * @see ActionController
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class UiHandler {
  /**
   * <p>Method for updating the file-related UI of the application. The method is called in {@link edu.ntnu.stud.controller.fractalactioncontroller.ActionController}.
   * The method sets visibility of the right side of the control panel with {@code setControlVisibility}
   * as a submethod. Then it will append text to the {@code TextArea}</p>
   *
   * @param feedbackTextArea the text area to append text to.
   * @param controls the buttons to set the visibility of.
   */

  public void updateUi(TextArea feedbackTextArea, Button... controls) {
    setControlVisibility(controls);
    feedbackTextArea.appendText("File loaded successfully.\n");
  }

  /**
   * <p>Method for setting the visibility of the right side of the controll panel which was
   * created in {@link edu.ntnu.stud.view.panels.PanelCreator}. The method is called in {@code updateUI}</p>
   *
   * @param buttons the buttons to set the visibility of.
   */
  public void setControlVisibility(Button... buttons) {
    for (Button button : buttons) {
      button.setVisible(true);
    }
  }
}
