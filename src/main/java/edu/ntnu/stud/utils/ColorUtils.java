package edu.ntnu.stud.utils;

import edu.ntnu.stud.model.transformation.MandelbrotSet;

/**
 * <h3>Color Utils</h3>
 * <p>The class {@code ColorUtils} is a utility class
 * for converting HSB color values to RGB color values.
 * The class has 1 method. It is used in {@link MandelbrotSet}.</p>

 * Responsibility: Convert HSB color values to RGB color values.
 *
 * @see edu.ntnu.stud.model.transformation.MandelbrotSet
 * @author Shiza Ahmad
 * @version 1.0.0
 * @since 0.1.0
 */

public class ColorUtils {

  /**
   * Method for converting HSB color values to RGB color values.
   *
   * @param hue the hue value
   * @param saturation the saturation value
   * @param brightness the brightness value
   * @return The RGB color value passed as a single {@code int} with 8 bits per value.
   */

  public static int hsbToRgb(float hue, float saturation, float brightness) {
    int r;
    int g;
    int b;

    if (saturation == 0) {
      r = g = b = (int) (brightness * 255.0f + 0.5f);
    } else {
      float h = (hue - (float) Math.floor(hue)) * 6.0f;
      float f = h - (float) Math.floor(h);
      float p = brightness * (1.0f - saturation);
      float q = brightness * (1.0f - saturation * f);
      float t = brightness * (1.0f - saturation * (1.0f - f));
      switch ((int) h) {
        case 0:
          r = (int) (brightness * 255.0f + 0.5f);
          g = (int) (t * 255.0f + 0.5f);
          b = (int) (p * 255.0f + 0.5f);
          break;
        case 1:
          r = (int) (q * 255.0f + 0.5f);
          g = (int) (brightness * 255.0f + 0.5f);
          b = (int) (p * 255.0f + 0.5f);
          break;
        case 2:
          r = (int) (p * 255.0f + 0.5f);
          g = (int) (brightness * 255.0f + 0.5f);
          b = (int) (t * 255.0f + 0.5f);
          break;
        case 3:
          r = (int) (p * 255.0f + 0.5f);
          g = (int) (q * 255.0f + 0.5f);
          b = (int) (brightness * 255.0f + 0.5f);
          break;
        case 4:
          r = (int) (t * 255.0f + 0.5f);
          g = (int) (p * 255.0f + 0.5f);
          b = (int) (brightness * 255.0f + 0.5f);
          break;
        case 5:
        default:
          r = (int) (brightness * 255.0f + 0.5f);
          g = (int) (p * 255.0f + 0.5f);
          b = (int) (q * 255.0f + 0.5f);
          break;
      }
    }
    return (r << 16) | (g << 8) | b;
  }
}
