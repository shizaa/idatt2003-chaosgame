package edu.ntnu.stud.utils;

import edu.ntnu.stud.controller.KeyboardController;
import edu.ntnu.stud.controller.MediatorController;
import edu.ntnu.stud.controller.fractalactioncontroller.ActionController;
import edu.ntnu.stud.controller.fractalactioncontroller.MandelbrotController;
import edu.ntnu.stud.controller.fractalactioncontroller.UiHandler;
import edu.ntnu.stud.controller.parameteractioncontroller.FileOperations;
import edu.ntnu.stud.controller.parameteractioncontroller.ParameterController;
import edu.ntnu.stud.model.chaosgame.ChaosGameCanvas;
import edu.ntnu.stud.model.chaosgame.ChaosGameDescription;
import edu.ntnu.stud.model.chaosgame.ChaosGameFileHandler;
import edu.ntnu.stud.model.chaosgame.ChaosGameFractalFactory;
import edu.ntnu.stud.model.controls.KeyboardsControls;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.view.display.ChaosGameDisplay;
import edu.ntnu.stud.view.panels.ParameterElements;
import edu.ntnu.stud.view.panels.PanelCreator;

import java.util.ArrayList;

/**
 * <h3> DependencyManager</h3>
 * <p>DependencyManager class that creates instances of the classes in the project.
 * This class manages the creation and injection of dependencies for various components
 * used throughout the application.</p>
 *
 * Responsibility: Create instances of the classes in the project.
 *
 * @author shizaahmad
 * @version 1.0.0
 * @since 1.0.0
 */

public class DependencyManager {

  /**
   * Creates an instance of MediatorController.
   *
   * @return a new instance of MediatorController.
   */
  public MediatorController createMediatorController() {
    return new MediatorController("src/main/resources/textFiles/");
  }

    /**
     * Creates an instance of ChaosGameFileHandler.
     *
     * @return a new instance of ChaosGameFileHandler.
     */
  public ChaosGameFileHandler createChaosGameFileHandler() {
    return new ChaosGameFileHandler();
  }

    /**
     * Creates an instance of FileOperations.
     *
     * @param chaosGameFileHandler the ChaosGameFileHandler instance to be used.
     * @return a new instance of FileOperations.
     */
  public FileOperations createFileOperations(ChaosGameFileHandler chaosGameFileHandler) {
    return new FileOperations(chaosGameFileHandler);
  }

    /**
     * Creates an instance of ChaosGameDescription.
     *
     * @return a new instance of ChaosGameDescription.
     */
  public ChaosGameDescription createChaosGameDescription() {
    return new ChaosGameDescription(new Vector2D(-1, -1), new Vector2D(1, 1), new ArrayList<>());
  }

  /**
   * Creates an instance of ParameterChangeHandler.
    * @param parameterElements the ParameterElements instance to be used.
   * @param mediatorController  the MediatorController instance to be used.
   * @param fileOperations the FileOperations instance to be used.
   * @param panelCreator the PanelCreator instance to be used.
   * @return a new instance of ParameterChangeHandler.
   */
  public ParameterController createParameterController(ParameterElements parameterElements, MediatorController mediatorController, FileOperations fileOperations, PanelCreator panelCreator) {
    return new ParameterController(parameterElements, mediatorController, fileOperations, panelCreator);
  }

    /**
     * Creates an instance of ChaosGameFractalFactory.
     *
     * @return a new instance of ChaosGameFractalFactory.
     */
  public ChaosGameFractalFactory createChaosGameFractalFactory() {
    return new ChaosGameFractalFactory();
  }

    /**
     * Creates an instance of UiHandler.
     *
     * @return a new instance of UiHandler.
     */

  public UiHandler createUIHandler() {
    return new UiHandler();
  }

    /**
     * Creates an instance of MandelbrotController.
     *
     * @return a new instance of MandelbrotController.
     */
  public MandelbrotController createMandelbrotController() {
    return new MandelbrotController();
  }

    /**
     * Creates an instance of ActionController.
     *
     * @param parameterController the ParameterController instance to be used.
     * @param mediatorController the MediatorController instance to be used.
     * @param chaosGameFractalFactory the ChaosGameFractalFactory instance to be used.
     * @param uiHandler the UiHandler instance to be used.
     * @param mandelbrotController the MandelbrotController instance to be used.
     * @return a new instance of ActionController.
     */

  public ActionController createActionController(ParameterController parameterController, MediatorController mediatorController, ChaosGameFractalFactory chaosGameFractalFactory, UiHandler uiHandler, MandelbrotController mandelbrotController) {
    return new ActionController(parameterController, mediatorController, chaosGameFractalFactory, uiHandler, mandelbrotController);
  }

    /**
     * Creates an instance of ChaosGameCanvas.
     *
     * @return a new instance of ChaosGameCanvas.
     */
  public ChaosGameCanvas createChaosGameCanvas() {
    return new ChaosGameCanvas(ChaosGameDisplay.WIDTH, ChaosGameDisplay.HEIGHT, new Vector2D(-1, -1), new Vector2D(1, 1));
  }

    /**
     * Creates an instance of PanelCreator.
     *
     * @param chaosGameFractalFactory the ChaosGameFractalFactory instance to be used.
     * @return a new instance of PanelCreator.
     */
  public PanelCreator createPanelCreator(ChaosGameFractalFactory chaosGameFractalFactory) {
    return new PanelCreator(chaosGameFractalFactory);
  }

    /**
     * Creates an instance of ChaosGameDisplay.
     *
     * @param actionController the ActionController instance to be used.
     * @param parameterController the ParameterController instance to be used.
     * @param chaosGameFileHandler the ChaosGameFileHandler instance to be used.
     * @param chaosGameDescription the ChaosGameDescription instance to be used.
     * @param chaosGameCanvas the ChaosGameCanvas instance to be used.
     * @param panelCreator the PanelCreator instance to be used.
      * @return a new instance of ChaosGameDisplay.
     */
  public ChaosGameDisplay createChaosGameDisplay(ActionController actionController, ParameterController parameterController, ChaosGameFileHandler chaosGameFileHandler, ChaosGameDescription chaosGameDescription, ChaosGameCanvas chaosGameCanvas, PanelCreator panelCreator) {
    return new ChaosGameDisplay(actionController, parameterController, chaosGameFileHandler, chaosGameDescription, chaosGameCanvas, panelCreator);
  }

    /**
     * Creates an instance of KeyboardsControls.
     *
     * @return a new instance of KeyboardsControls.
     */
  public KeyboardsControls createKeyboardsControls() {
    return new KeyboardsControls();
  }

    /**
     * Creates an instance of KeyboardController.
     *
     * @param display           the ChaosGameDisplay instance to be used.
     * @param keyboardsControls the KeyboardsControls instance to be used.
     */

  public void createKeyboardController(ChaosGameDisplay display, KeyboardsControls keyboardsControls) {
    KeyboardController keyboardController = new KeyboardController(display);
    keyboardsControls.addObserver(keyboardController);
  }
}
